from tasks.helpers.clients.api.neo4j.writers.nosql import NoSQLWriter
import json

if __name__ == '__main__':
	writer = NoSQLWriter()
	data = {
		"uri": "MongoDB",
		"dbpedia_uri": "MongoDB",
		"name": "mongodb",
		"summary": "Document database with replication and sharding, BSON store (binary format JSON).",
		"REST_API": "Yes[14]",
		"latest_release": "",
		"datamodel": [
			{
				"name": "document",
				"dbpedia_uri": "Document-oriented_database"
			}
		],
		"developers": {
			"resources": [
				{
					"dbpedia_uri": "MongoDB_Inc",
					"name": "MongoDB, Inc"
				}
			],
			"text": "MongoDB, Inc"
		},
		"licenses": [
			{
				"movements": [
					{
						"name": "Open Source",
						"dbpedia_uri": "Open-source_license"
					},
					{
						"name": "Free",
						"dbpedia_uri": "Free_software"
					}
				],
				"license": {
					"name": "Apache License",
					"dbpedia_uri": "Apache_License"
				}
			},
			{
				"movements": [
					{
						"name": "Open Source",
						"dbpedia_uri": "Open-source_license"
					}
				],
				"license": {
					"name": "Affero General Public License",
					"dbpedia_uri": "Affero_General_Public_License"
				}
			}
		],
		"drivers": {
"text": "C, C++, C#, Java, Perl, PHP, Python, Node.js, Ruby, Scala [13]",
"resources": [
{
"influenced_by": [
"FORTRAN",
"Assembly_language",
"PL/I",
"ALGOL_68",
"B_(programming_language)",
"CPL_(programming_language)"
],
"dbpedia_uri": "C_(programming_language)",
"paradigms": [
"Imperative , structured"
],
"name": "C",
"summary": "C (/ˈsiː/, as in the letter c) is a general-purpose, imperative computer programming language, supporting structured programming, lexical variable scope and recursion, while a static type system prevents many unintended operations. By design, C provides constructs that map efficiently to typical machine instructions, and therefore it has found lasting use in applications that had formerly been coded in assembly language, including operating systems, as well as various application software for computers ranging from supercomputers to embedded systems."
},
{
"influenced_by": [
"ML_(programming_language)",
"Ada_(programming_language)",
"ALGOL_68",
"CLU_(programming_language)",
"Simula",
"C_(programming_language)"
],
"dbpedia_uri": "C%2B%2B",
"paradigms": [
"Multi-paradigm_programming_language",
"Functional_programming",
"Procedural_programming",
"Object-oriented_programming",
"Generic_programming"
],
"name": "C++",
"summary": "C++ (pronounced cee plus plus, /ˈsiː plʌs plʌs/) is a general-purpose programming language. It has imperative, object-oriented and generic programming features, while also providing facilities for low-level memory manipulation. Many other programming languages have been influenced by C++, including C#, D, Java, and newer versions of C (after 1998)."
},
{
"influenced_by": [
"ML_(programming_language)",
"C++",
"Java_(programming_language)",
"Modula-3",
"F_Sharp_(programming_language)",
"Cω",
"Icon_(programming_language)",
"Object_Pascal",
"Rust_(programming_language)",
"Haskell_(programming_language)",
"Eiffel_(programming_language)"
],
"dbpedia_uri": "C_Sharp_(programming_language)",
"paradigms": [
"Functional_programming",
"Structured_programming",
"Concurrent_computing",
"Reflective_programming",
"Multi-paradigm_programming_language",
"Imperative_programming",
"The_Task-based_Asynchronous_Pattern",
"Event-driven_programming",
"Object-oriented_programming",
"Generic_programming"
],
"name": "C#",
"summary": "() C# (pronounced as see sharp) is a multi-paradigm programming language encompassing strong typing, imperative, declarative, functional, generic, object-oriented (class-based), and component-oriented programming disciplines. It was developed by Microsoft within its .NET initiative and later approved as a standard by Ecma (ECMA-334) and ISO (ISO/IEC 23270:2006). C# is one of the programming languages designed for the Common Language Infrastructure."
},
{
"influenced_by": [
"C++",
"Ada_(programming_language)",
"Objective-C",
"Modula-3",
"C_Sharp_(programming_language)",
"UCSD_Pascal",
"Generic_Java",
"Mesa_(programming_language)",
"Object_Pascal",
"Eiffel_(programming_language)",
"Oberon_(programming_language)"
],
"dbpedia_uri": "Java_(programming_language)",
"paradigms": [
"Multi-paradigm: Object-oriented , structured, imperative, generic, reflective, concurrent"
],
"name": "Java",
"summary": "Java is a general-purpose computer programming language that is concurrent, class-based, object-oriented, and specifically designed to have as few implementation dependencies as possible. It is intended to let application developers \"write once, run anywhere\" (WORA), meaning that compiled Java code can run on all platforms that support Java without the need for recompilation. Java applications are typically compiled to bytecode that can run on any Java virtual machine (JVM) regardless of computer architecture. As of 2016, Java is one of the most popular programming languages in use, particularly for client-server web applications, with a reported 9 million developers. Java was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle Corporation) an"
},
{
"influenced_by": [
"Lisp_(programming_language)",
"C++",
"Smalltalk_80",
"Sed",
"Unix_shell",
"AWK_(programming_language)",
"Pascal_(programming_language)",
"C_(programming_language)"
],
"dbpedia_uri": "Perl",
"paradigms": [
"multi-paradigm: functional, imperative, object-oriented , reflective, procedural, event-driven, generic"
],
"name": "Perl",
"summary": "Perl is a family of high-level, general-purpose, interpreted, dynamic programming languages. The languages in this family include Perl 5 and Perl 6. Though Perl is not officially an acronym, there are various backronyms in use, the best-known being \"Practical Extraction and Reporting Language\". Perl was originally developed by Larry Wall in 1987 as a general-purpose Unix scripting language to make report processing easier. Since then, it has undergone many changes and revisions. Perl 6, which began as a redesign of Perl 5 in 2000, eventually evolved into a separate language. Both languages continue to be developed independently by different development teams and liberally borrow ideas from one another."
},
{
"influenced_by": [],
"dbpedia_uri": "PHP",
"paradigms": [],
"name": "PHP",
"summary": ""
},
{
"influenced_by": [
"Lisp_(programming_language)",
"C++",
"Java_(programming_language)",
"Dylan_(programming_language)",
"Modula-3",
"ALGOL_68",
"Perl",
"Icon_(programming_language)",
"Haskell_(programming_language)",
"ABC_(programming_language)",
"C_(programming_language)"
],
"dbpedia_uri": "Python_(programming_language)",
"paradigms": [
"Functional_programming",
"Reflective_programming",
"Multi-paradigm_programming_language",
"Imperative_programming",
"Procedural_programming",
"Object-oriented_programming"
],
"name": "Python",
"summary": "Python is a widely used high-level, general-purpose, interpreted, dynamic programming language. Its design philosophy emphasizes code readability, and its syntax allows programmers to express concepts in fewer lines of code than possible in languages such as C++ or Java. The language provides constructs intended to enable writing clear programs on both a small and large scale."
},
{
"dbpedia_uri": "Node.js",
"name": "Node.js"
},
{
"influenced_by": [
"Lisp_(programming_language)",
"Ada_(programming_language)",
"Smalltalk",
"C++",
"Dylan_(programming_language)",
"Perl",
"Python_(programming_language)",
"CLU_(programming_language)",
"Eiffel_(programming_language)",
"Lua_(programming_language)"
],
"dbpedia_uri": "Ruby_(programming_language)",
"paradigms": [
"Multi-paradigm_programming_language",
"Functional_programming",
"Imperative_programming",
"Object-oriented_programming",
"Reflective_programming"
],
"name": "Ruby",
"summary": "Ruby is a dynamic, reflective, object-oriented, general-purpose programming language. It was designed and developed in the mid-1990s by Yukihiro \"Matz\" Matsumoto in Japan. According to its creator, Ruby was influenced by Perl, Smalltalk, Eiffel, Ada, and Lisp. It supports multiple programming paradigms, including functional, object-oriented, and imperative. It also has a dynamic type system and automatic memory management."
},
{
"influenced_by": [
"Lisp_(programming_language)",
"Smalltalk",
"Java_(programming_language)",
"OCaml",
"Scheme_(programming_language)",
"Erlang_(programming_language)",
"Standard_ML",
"Haskell_(programming_language)",
"Eiffel_(programming_language)",
"Oz_(programming_language)",
"Pizza_(programming_language)"
],
"dbpedia_uri": "Scala_(programming_language)",
"paradigms": [
"Multi-paradigm_programming_language",
"Functional_programming",
"Imperative_programming",
"Object-oriented_programming",
"Concurrent_programming"
],
"name": "Scala",
"summary": "Scala (/ˈskɑːlɑː/ SKAH-lah) is a general-purpose programming language. Scala has full support for functional programming and a very strong static type system. Designed to be concise, many of Scala's design decisions were inspired by criticism of Java's shortcomings. The name Scala is a portmanteau of scalable and language, signifying that it is designed to grow with the demands of its users."
}
]
},
	"developed_languages": {"resources": [], "text": "C++"},
	"keywords": [
{
"dbpedia_uri": "BigCouch",
"name": "BigCouch"
},
{
"dbpedia_uri": "Open_source",
"name": "open source"
},
{
"dbpedia_uri": "Apache_Software_Foundation",
"name": "Apache"
},
{
"dbpedia_uri": "CouchDB",
"name": "CouchDB"
}
],
"transactions": {
"text": "Full ACID",
"resources": []
},
"API": {"text": "PHP", "resources": []},
"datatypes": [
"Java and .NET data types"
],
"alternativeto_post": {
"uri": "mongodb",
"description": "MongoDB (from \"humongous\") is a scalable, high-performance, open source NoSQL database. The database is document-oriented so it manages collections of JSON-like documents. Many applications can thus model data in a more natural way, as data can be nested in complex hierarchies and still be query-able and indexable.",
"alternatives": [
{
"uri": "postgressql",
"thumbnail": "https://d2.alternativeto.net/dist/icons/postgressql_12513.png?width=64&height=64&mode=crop&upscale=false",
"likes": 193,
"name": "PostgreSQL"
},
{
"uri": "sqlite",
"thumbnail": "https://d2.alternativeto.net/dist/icons/sqlite_52754.png?width=64&height=64&mode=crop&upscale=false",
"likes": 222,
"name": "SQLite"
},
{
"uri": "mysql-community-edition",
"thumbnail": "https://d2.alternativeto.net/dist/icons/mysql-community-edition_52755.png?width=64&height=64&mode=crop&upscale=false",
"likes": 243,
"name": "MySQL Community Edition"
},
{
"uri": "mariadb",
"thumbnail": "https://d2.alternativeto.net/dist/icons/mariadb_102820.png?width=64&height=64&mode=crop&upscale=false",
"likes": 87,
"name": "MariaDB"
},
{
"uri": "redis",
"thumbnail": "https://d2.alternativeto.net/dist/icons/redis_100785.png?width=64&height=64&mode=crop&upscale=false",
"likes": 73,
"name": "Redis"
},
{
"uri": "microsoft-sql-server",
"thumbnail": "https://d2.alternativeto.net/dist/icons/microsoft-sql-server_52756.png?width=64&height=64&mode=crop&upscale=false",
"likes": 62,
"name": "Microsoft SQL Server"
},
{
"uri": "couchdb",
"thumbnail": "https://d2.alternativeto.net/dist/icons/couchdb_114231.png?width=64&height=64&mode=crop&upscale=false",
"likes": 32,
"name": "CouchDB"
},
{
"uri": "rethinkdb",
"thumbnail": "https://d2.alternativeto.net/dist/icons/rethinkdb_49031.png?width=64&height=64&mode=crop&upscale=false",
"likes": 15,
"name": "RethinkDB"
},
{
"uri": "cassandra-database",
"thumbnail": "https://d2.alternativeto.net/dist/icons/cassandra-database_42470.png?width=64&height=64&mode=crop&upscale=false",
"likes": 12,
"name": "Apache Cassandra"
},
{
"uri": "couchbase",
"thumbnail": "https://d2.alternativeto.net/dist/icons/couchbase_21396.png?width=64&height=64&mode=crop&upscale=false",
"likes": 13,
"name": "CouchBase"
},
{
"uri": "cockroachdb",
"thumbnail": "https://d2.alternativeto.net/dist/icons/cockroachdb_108973.png?width=64&height=64&mode=crop&upscale=false",
"likes": 9,
"name": "CockroachDB"
},
{
"uri": "oracle-database",
"thumbnail": "https://d2.alternativeto.net/dist/icons/oracle-database_75791.png?width=64&height=64&mode=crop&upscale=false",
"likes": 19,
"name": "Oracle Database"
},
{
"uri": "arangodb",
"thumbnail": "https://d2.alternativeto.net/dist/icons/arangodb_36826.png?width=64&height=64&mode=crop&upscale=false",
"likes": 26,
"name": "ArangoDB"
},
{
"uri": "firebird",
"thumbnail": "https://d2.alternativeto.net/dist/icons/firebird_808.png?width=64&height=64&mode=crop&upscale=false",
"likes": 14,
"name": "Firebird"
},
{
"uri": "h2-database-engine",
"thumbnail": "https://d2.alternativeto.net/dist/icons/h2-database-engine_12357.png?width=64&height=64&mode=crop&upscale=false",
"likes": 10,
"name": "H2 Database Engine"
}
],
"summary": "MongoDB (from \"humongous\") is a scalable, high-performance, open source NoSQL database.\n\nCreated by MongoDB, Inc.",
"repository": "",
"labels": [
{
"name": "Free",
"dbpedia_uri": "Free_license"
},
{
"name": "Open Source",
"dbpedia_uri": "Open-source_license"
},
{
"name": "Mac OS X",
"dbpedia_uri": "MacOS"
},
{
"name": "Windows",
"dbpedia_uri": "Microsoft_Windows"
},
{
"name": "Linux",
"dbpedia_uri": "Linux"
},
{
"name": "Online",
"dbpedia_uri": "Cloud_database"
},
{
"name": "BSD",
"dbpedia_uri": "BSD_licenses"
}
],
"likes": 96,
"thumbnail": "https://d2.alternativeto.net/dist/icons/mongodb_12399.png?width=200&height=200&mode=crop&upscale=false",
"name": "MongoDB",
"homepage": "http://www.mongodb.org/"
},

"stackshare_post": {
"uri": "mongodb",
"description": "MongoDB stores data in JSON-like documents that can vary in structure, offering a dynamic, flexible schema. MongoDB was also designed for high availability and scalability, with built-in replication and auto-sharding.",
"summary": "The database for giant ideas",
"repository": "https://github.com/mongodb/mongo",
"votes": 3780,
"clients": [
{
"uri": "codecombat",
"thumbnail": "https://img.stackshare.io/stack/344/35606c16e07a383a6e4cd2147c49da00.png",
"name": "CodeCombat"
},
{
"uri": "bugsnag",
"thumbnail": "https://img.stackshare.io/stack/339/DtgzNvem.png",
"name": "Bugsnag"
},
{
"uri": "codecademy",
"thumbnail": "https://img.stackshare.io/stack/4/ih2vOpDt.png",
"name": "Codecademy"
},
{
"uri": "uber",
"thumbnail": "https://img.stackshare.io/stack/5/p9jxYqy5.png",
"name": "Uber"
},
{
"uri": "foursquare",
"thumbnail": "https://img.stackshare.io/stack/18/lybUngI1.png",
"name": "Foursquare"
},
{
"uri": "hootsuite",
"thumbnail": "https://img.stackshare.io/stack/25/_FvB01Ox.jpg",
"name": "Hootsuite"
},
{
"uri": "billguard",
"thumbnail": "https://img.stackshare.io/company/37/vUkDvcm7.png",
"name": "BillGuard"
},
{
"uri": "buffer",
"thumbnail": "https://img.stackshare.io/stack/67/hnc3q-7x.jpg",
"name": "Buffer"
},
{
"uri": "circleci",
"thumbnail": "https://img.stackshare.io/stack/76/circleci-logo-stacked-fb.png",
"name": "CircleCI"
}
],
"homepage": "http://www.mongodb.com/",
"name": "MongoDB",
"forks": 3350,
"repository_stars": 12500,
"comments": [
{
"text": "Document-oriented storage",
"likes": 776
},
{
"text": "No sql",
"likes": 552
},
{
"text": "Ease of use",
"likes": 516
},
{
"text": "Fast",
"likes": 444
},
{
"text": "High performance",
"likes": 388
},
{
"text": "Free",
"likes": 231
},
{
"text": "Open source",
"likes": 200
},
{
"text": "Flexible",
"likes": 168
},
{
"text": "Replication & high availability",
"likes": 128
},
{
"text": "Easy to maintain",
"likes": 99
},
{
"text": "Querying",
"likes": 36
},
{
"text": "High availability",
"likes": 30
},
{
"text": "Easy scalability",
"likes": 29
},
{
"text": "Auto-sharding",
"likes": 27
},
{
"text": "Map/reduce",
"likes": 24
},
{
"text": "Document database",
"likes": 24
},
{
"text": "Easy setup",
"likes": 22
},
{
"text": "Full index support",
"likes": 21
},
{
"text": "Reliable",
"likes": 13
},
{
"text": "Fast in-place updates",
"likes": 12
},
{
"text": "Agile programming, flexible, fast",
"likes": 10
},
{
"text": "No database migrations",
"likes": 9
},
{
"text": "Enterprise",
"likes": 6
},
{
"text": "Easy integration with Node.Js",
"likes": 5
},
{
"text": "Enterprise Support",
"likes": 4
},
{
"text": "Great NoSQL DB",
"likes": 3
},
{
"text": "Aggregation Framework",
"likes": 2
},
{
"text": "Drivers support is good",
"likes": 2
},
{
"text": "Fast",
"likes": 1
},
{
"text": "Support for many languages through different drivers",
"likes": 1
}
],
"favorites": 360,
"thumbnail": "https://img.stackshare.io/service/1030/leaf-360x360.png",
"tools": [
{
"uri": "meteor",
"thumbnail": "https://img.stackshare.io/service/1162/uJPMDhZq.png",
"name": "Meteor"
},
{
"uri": "buddy",
"thumbnail": "https://img.stackshare.io/service/4263/logo-sygnet_big.png",
"name": "Buddy"
},
{
"uri": "presto",
"thumbnail": "https://img.stackshare.io/service/2606/zfcO78oI.png",
"name": "Presto"
},
{
"uri": "metabase",
"thumbnail": "https://img.stackshare.io/service/3860/dfiGLc6p.png",
"name": "Metabase"
},
{
"uri": "mongodb-atlas",
"thumbnail": "https://img.stackshare.io/service/5739/atlas-360x360.png",
"name": "MongoDB Atlas"
},
{
"uri": "stitch",
"thumbnail": "https://img.stackshare.io/service/7199/26872-e7a29977a9b25ab4f8d0ca4e586ebeb9-medium_jpg.jpg",
"name": "Stitch"
},
{
"uri": "re-dash",
"thumbnail": "https://img.stackshare.io/service/3033/favicon-96x96.png",
"name": "re:dash"
},
{
"uri": "backand",
"thumbnail": "https://img.stackshare.io/service/3928/1j_MwuN2.jpeg",
"name": "Backand"
},
{
"uri": "keystonejs",
"thumbnail": "https://img.stackshare.io/service/1230/d5575455853a3eb2ec1dafb5522ffc9a.png",
"name": "KeystoneJS"
}
],
"fans": 3270
},
"wikipedia_post": {
"uri": "https://en.wikipedia.org/wiki/MongoDB",
"latest_release": "3.4.9 / 11 September 2017; 53 days ago (2017-09-11)",
"developed_languages": {
"text": "",
"resources": [
{
"summary": "C++ (pronounced cee plus plus, /ˈsiː plʌs plʌs/) is a general-purpose programming language. It has imperative, object-oriented and generic programming features, while also providing facilities for low-level memory manipulation. Many other programming languages have been influenced by C++, including C#, D, Java, and newer versions of C (after 1998).",
"paradigms": [
"Generic_programming",
"Object-oriented_programming",
"Multi-paradigm_programming_language",
"Functional_programming",
"Procedural_programming"
],
"influenced_by": [
"C_(programming_language)",
"CLU_(programming_language)",
"ALGOL_68",
"Simula",
"Ada_(programming_language)",
"ML_(programming_language)"
],
"name": "C++",
"dbpedia_uri": "C%2B%2B"
},
{
"summary": "C (/ˈsiː/, as in the letter c) is a general-purpose, imperative computer programming language, supporting structured programming, lexical variable scope and recursion, while a static type system prevents many unintended operations. By design, C provides constructs that map efficiently to typical machine instructions, and therefore it has found lasting use in applications that had formerly been coded in assembly language, including operating systems, as well as various application software for computers ranging from supercomputers to embedded systems.",
"paradigms": [
"Imperative , structured"
],
"influenced_by": [
"B_(programming_language)",
"Assembly_language",
"PL/I",
"ALGOL_68",
"FORTRAN",
"CPL_(programming_language)"
],
"name": "C",
"dbpedia_uri": "C_(programming_language)"
},
{
"summary": "JavaScript (/ˈdʒævəˌskrɪpt/) is a high-level, dynamic, untyped, and interpreted programming language. It has been standardized in the ECMAScript language specification. Alongside HTML and CSS, it is one of the three core technologies of World Wide Web content production; the majority of websites employ it and it is supported by all modern Web browsers without plug-ins. JavaScript is prototype-based with first-class functions, making it a multi-paradigm language, supporting object-oriented, imperative, and functional programming styles. It has an API for working with text, arrays, dates and regular expressions, but does not include any I/O, such as networking, storage, or graphics facilities, relying for these upon the host environment in which it is embedded.",
"paradigms": [
"Multi-paradigm: scripting, object-oriented , imperative, functional, event-driven"
],
"influenced_by": [
"AWK",
"C_(programming_language)",
"Perl",
"Python_(programming_language)",
"Lua_(programming_language)",
"Java_(programming_language)",
"Self_(programming_language)",
"HyperTalk",
"Scheme_(programming_language)"
],
"name": "JavaScript",
"dbpedia_uri": "JavaScript"
}
]
},
"repository": "https://github.com/mongodb/mongo",
"developer": {
"text": "MongoDB Inc.",
"resources": [
{
"name": "MongoDB Inc.",
"dbpedia_uri": "MongoDB_Inc."
}
]
},
"licenses": {
"text": "Various; see § Licensing",
"resources": []
},
"operating_systems": {
"text": "",
"resources": [
{
"name": "Windows Vista",
"dbpedia_uri": "Windows_Vista"
},
{
"name": "Linux",
"dbpedia_uri": "Linux"
},
{
"name": "OS X",
"dbpedia_uri": "OS_X"
},
{
"name": "Solaris",
"dbpedia_uri": "Solaris_(operating_system)"
},
{
"name": "FreeBSD",
"dbpedia_uri": "FreeBSD"
}
]
},
"status": "Active",
"thumbnail": "https://upload.wikimedia.org/wikipedia/en/thumb/4/45/MongoDB-Logo.svg/250px-MongoDB-Logo.svg.png",
"initial_release": "February 2009, 11 (11-02-2009)",
"homepage": "https://www.mongodb.com/"
},
"dbengine_post": {
"homepage": "https://www.mongodb.com/",
"summary": "One of the most popular document stores",
"supports_durability": "yes",
"supports_concurrency": "yes",
"data_schemes": [
"schema-free "
],
"operating_systems": {
"text": [],
"resources": [
{
"name": "Linux",
"dbpedia_uri": "Linux"
},
{
"name": "OS X",
"dbpedia_uri": "MacOS"
},
{
"name": "Solaris",
"dbpedia_uri": "Solaris_(operating_system)"
},
{
"name": "Windows",
"dbpedia_uri": "Microsoft_Windows"
}
]
},
"datatypes": [
"string",
"integer",
"double",
"boolean",
"date",
"object_id"
],
"use_cases": [
"Internet of Things",
"Mobile",
"Single View",
"Real Time Analytics",
"Personalization",
"Catalogs",
"Content Management"
],
"developer": "MongoDB, Inc",
"partitioning_methods": [
"Sharding"
],
"transaction_methods": [
"Atomic single-row operations"
],
"supports_xml": "",
"name": "MongoDB",
"is_cloud_based": "no",
"latest_release": "3.4.9, September 2017",
"documentation": "https://docs.mongodb.com/manual/",
"licenses": {
	"text": [],
	"resources": [
					{
						"license": "",
						"movements": [
							{
								"dbpedia_uri": "Open-source_license",
								"name": "Open Source"
							}
						]
					}
				]
},
"developed_languages": {
"text": [],
"resources": [
{
"name": "C++",
"dbpedia_uri": "C%2B%2B"
}
]
},
"score": 330.47,
"drivers": {
"text": [],
"resources": [
{
"name": "Actionscript",
"status": "inofficial",
"dbpedia_uri": "ActionScript"
},
{
"name": "C",
"status": "unknown",
"dbpedia_uri": "C_(programming_language)"
},
{
"name": "C#",
"status": "unknown",
"dbpedia_uri": "C_Sharp_(programming_language)"
},
{
"name": "C++",
"status": "unknown",
"dbpedia_uri": "C%2B%2B"
},
{
"name": "Clojure",
"status": "inofficial",
"dbpedia_uri": "Clojure"
},
{
"name": "ColdFusion",
"status": "inofficial",
"dbpedia_uri": "ColdFusion_Markup_Language"
},
{
"name": "D",
"status": "inofficial",
"dbpedia_uri": "D_(programming_language)"
},
{
"name": "Dart",
"status": "inofficial",
"dbpedia_uri": "Dart_(programming_language)"
},
{
"name": "Delphi",
"status": "inofficial",
"dbpedia_uri": "Delphi_(programming_language)"
},
{
"name": "Erlang",
"status": "unknown",
"dbpedia_uri": "Erlang_(programming_language)"
},
{
"name": "Go",
"status": "inofficial",
"dbpedia_uri": "Go_(programming_language)"
},
{
"name": "Groovy",
"status": "inofficial",
"dbpedia_uri": "Groovy_(programming_language)"
},
{
"name": "Haskell",
"status": "unknown",
"dbpedia_uri": "Haskell_(programming_language)"
},
{
"name": "Java",
"status": "unknown",
"dbpedia_uri": "Java_(programming_language)"
},
{
"name": "JavaScript",
"status": "unknown",
"dbpedia_uri": "JavaScript"
},
{
"name": "Lisp",
"status": "inofficial",
"dbpedia_uri": "Lisp_(programming_language)"
},
{
"name": "Lua",
"status": "inofficial",
"dbpedia_uri": "Lua_(programming_language)"
},
{
"name": "MatLab",
"status": "inofficial",
"dbpedia_uri": "MATLAB"
},
{
"name": "Perl",
"status": "unknown",
"dbpedia_uri": "Perl"
},
{
"name": "PHP",
"status": "unknown",
"dbpedia_uri": "PHP"
},
{
"name": "PowerShell",
"status": "inofficial",
"dbpedia_uri": "PowerShell"
},
{
"name": "Prolog",
"status": "inofficial",
"dbpedia_uri": "Prolog"
},
{
"name": "Python",
"status": "unknown",
"dbpedia_uri": "Python_(programming_language)"
},
{
"name": "R",
"status": "inofficial",
"dbpedia_uri": "R_(programming_language)"
},
{
"name": "Ruby",
"status": "unknown",
"dbpedia_uri": "Ruby_(programming_language)"
},
{
"name": "Scala",
"status": "unknown",
"dbpedia_uri": "Scala_(programming_language)"
},
{
"name": "Smalltalk",
"status": "inofficial",
"dbpedia_uri": "Smalltalk"
}
]
},
"uri": "https://db-engines.com/en/system/MongoDB",
"replication_methods": [
"Master-slave replication"
],
"supports_sql": "no",
"consistency_methods": [
"Eventual Consistency",
"Immediate Consistency "
],
"initial_release": "2009",
"access_methods": [
"proprietary protocol using JSON"
]
},
"dbpedia_post": {
"homepage": "https://cloud.google.com/bigtable/",
"summary": "Bigtable is a compressed, high performance, and proprietary data storage system built on Google File System, Chubby Lock Service, SSTable (log-structured storage like LevelDB) and a few other Google technologies. On May 6, 2015, a public version of Bigtable was made available as a service. Bigtable also underlies Google Cloud Datastore, which is available as a part of the Google Cloud Platform.",
"frequently_updated": "",
"operating_systems": [],
"latest_release": "",
"latest_release_date": "",
"developed_languages": [
{
"dbpedia_uri": "Go_(programming_language)",
"name": "Go"
},
{
"dbpedia_uri": "Ruby_(programming_language)",
"name": "Ruby"
},
{
"dbpedia_uri": "Python_(programming_language)",
"name": "Python"
},
{
"dbpedia_uri": "Java_(programming_language)",
"name": "Java"
}
],
"external_links": [
"http://research.google.com/archive/bigtable-osdi06.pdf",
"http://www.cs.washington.edu/htbin-post/mvis/mvis?ID=437",
"http://readwrite.com/2009/02/12/is-the-relational-database-doomed",
"https://cloud.google.com/bigtable/",
"http://video.google.com/videoplay?docid=7278544055668715642",
"https://github.com/bluefish/kdi",
"http://www.uwtv.org/programs/displayevent.asp?rid=2787",
"http://www.baselinemag.com/article2/0,1540,1985050,00.asp",
"http://andrewhitchcock.org/?post=214",
"http://research.google.com/archive/bigtable.html"
],
"licenses": [
{
"license": "",
"movements": [
{
"dbpedia_uri": "Proprietary_software",
"name": "Proprietary"
}
]
}
],
"status": "",
"thumbnail": "",
"developers": [
{
"dbpedia_uri": "Google",
"name": "Google Inc."
}
],
"name": "Bigtable"
},

"wikidata_post": {
"homepage": "http://www.hypertable.org/",
"operating_systems": [
{
"dbpedia_uri": "Cross-platform",
"name": ""
}
],
"twitter_username": "",
"latest_release": "",
"developed_languages": [
{
"dbpedia_uri": "Java_(programming_language)",
"name": "Java"
}
],
"facebook_username": "",
"thumbnail": "",
"repository": "",
"licenses": [
{
"license": {
"dbpedia_uri": "GNU_General_Public_License",
"name": "GNU General Public License"
},
"movements": [
{
"dbpedia_uri": "Open-source_license",
"name": "Open Source"
},
{
"dbpedia_uri": "Free_software",
"name": "Free"
}
]
}
],
"stack_exchange_tag": "",
"quora_topic": "HyperTable",
"developers": [
{
"dbpedia_uri": "Apache_Software_Foundation",
"name": "Apache Software Foundation"
}
]
},
"stackoverflow_tag": {
"uri": "stardog",
"unanswered_questions": 0,
"active": "",
"summary": "",
"created_at": "",
"related_tags": [
{
"uri": "sparql",
"summary": "SPARQL (pronounced 'sparkle', a recursive acronym for SPARQL Protocol and RDF Query Language) is an RDF query language, that is, a semantic query language for databases, able to retrieve and manipulate data stored in Resource Description Framework (RDF) format. It was made a standard by the RDF Data Access Working Group (DAWG) of the World Wide Web Consortium, and is recognized as one of the key technologies of the semantic web. On 15 January 2008, SPARQL 1.0 became an official W3C Recommendation, and SPARQL 1.1 in March, 2013.",
"weight": 0,
"paradigms": [
"Query_language"
],
"influenced_by": [ ],
"name": "SPARQL",
"dbpedia_uri": "SPARQL"
},
{
"uri": "rdf",
"weight": 0,
"name": "Rdf"
}],
"links": ["http://test/link"],
"number_questions": 0,
"name": "Stardog",
"views": 0
},
"comments": {
	"comments": [
			{
			"text": "Do the bigtable performance characteristics look kind of like cache line ping ponging? My intuition for scenario 3 outperforming scenario 2 (100% remote vs 50% local + 50% remote) is that there are more mutations of data and therefore more interconnect traffic is required to maintain coherency across sockets.",
			"url": "http://highscalability.com/blog/2013/5/30/google-finds-numa-up-to-20-slower-for-gmail-and-websearch.html",
			"stars": 3.2,
			"sentiment": 0.28
			},
			{
			"text": "A reasonable thought, but wrong. The existing datastore has basically the 99.9% of the benefits you describe. Unfortunately, 99.9% availability (especially when a single datastore request may touch 10 things and thus be at 99% availability) isn't good enough.One of the major problems with having a single bigtable is partial outages. These can either be the major events like there were today, with 10% or more of the cell having issues, or it can be just one server in the bigtable having performance issues. The key to having higher availability with the High Replication datastore is that if certain entities are slow or unavailable in one datacenter, we can rely on the performance of other datacenters to limit the latency.",
			"url": "http://googleappengine.blogspot.com/2011/01/announcing-high-replication-datastore.html",
			"stars": 2.36,
			"sentiment": -0.055
			},
			{
			"text": "Some 6000 changes in the database are performed every second.Things have changed a lot in 15 years!https:cloud.google.combigtabledocsperformance 10k QPS per node...https:cloud.google.combigtablepdfFISConsolidatedAuditTr... 2.7 Million FIX messages processed and inserted per secondDisclosure: I work at Google Cloud.",
			"url": "https://home.cern/about/computing",
			"stars": 2.5,
			"sentiment": 0
			}
],
"total": 16,
"total_rating": 2.62,
"avg_sentiment": 0.12743749999999998
},

"drivers_repositories": [
	{
		"total": 7,
		"popular_repositories": [ ],
		"language": {
			"name": "Ruby",
			"dbpedia_uri": "Ruby_(programming_language)"
		}
	},
	{
		"total": 19,
		"popular_repositories": [
  		{
  			"summary": "MySQL client like cassanda command line interface based on CQL and pycassa driver",
  			"owner": "rembish",
  			"updated_at": "2014-04-02T09:21:50",
  			"language": "Python",
  			"name": "cassandra-cli",
  			"created_at": "2012-01-05T22:05:23",
  			"has_downloads": True,
  			"stars": 1,
  			"url": "https://api.github.com/repos/rembish/cassandra-cli",
  			"has_wiki": True,
  			"homepage": ""
  		},
  		{
  			"summary": "Interactive with Kafka and Zookeeper servers, catch stock price from google finance and write to Kafka, and interactive with Cassandra servers and save data to Cassandra based on Cassandra driver. Finally show real-time stock price.",
  			"owner": "XingchenYu",
  			"updated_at": "2017-07-18T17:51:02",
  			"language": "Python",
  			"name": "bigdata_stockprice",
  			"created_at": "2017-07-08T05:47:24",
  			"has_downloads": True,
  			"stars": 1,
  			"url": "https://api.github.com/repos/XingchenYu/bigdata_stockprice",
  			"has_wiki": True,
  			"homepage": ""
  		}
    ],
	"language": {
		"name": "Python",
		"dbpedia_uri": "Python_(programming_language)"
	}
	}
],
"technologies_stats": {
"total_by_language": [
{
"total": 38,
"name": "Java"
},
{
"total": 18,
"name": "JavaScript"
},
{
"total": 9,
"name": "Go"
},
{
"total": 7,
"name": "Python"
},
{
"total": 6,
"name": "C++"
},
{
"total": 3,
"name": "Rust"
},
{
"total": 3,
"name": "Shell"
},
{
"total": 2,
"name": "C#"
},
{
"total": 2,
"name": "Kotlin"
},
{
"total": 2,
"name": "Makefile"
}
],
"total": 123
}
}
	

	writer.write_nosql(data)
    
