from py2neo import Graph, Node, Relationship

def filter():
	graph = Graph('bolt://neo4j:techsearch@localhost:7687')
	movements = ['Open-source_license', 'Proprietary_software']
	datamodels = ['Graph_database', 'Document-oriented_database']
	languages = ['Java_(programming_language)', 'Perl', 'Haskell_(programming_language)']

	datamodels_uris = filter_datamodels(graph, datamodels)
	languages_uris = filter_languages(graph, languages)
	movements_uris = filter_movements(graph, movements)
	result_uris = intersect(datamodels_uris, languages_uris, movements_uris)
	print(inflate_values(graph, result_uris))

def inflate_values(graph, uris):
	query = """MATCH (n:NoSQL) 
	WHERE n.uri IN {}
	OPTIONAL MATCH (n)-[:HAS]-(re:Review)
	OPTIONAL MATCH (n)-[:SAME_AS]-(same) WITH n, COUNT(DISTINCT same) as counter,
	COLLECT(same.summary) as summaries, re 
	RETURN DISTINCT n.uri, n.name, n.thumbnail, n.summary,
	FILTER(i in summaries WHERE size(i) > 1), re.total_rating, counter ORDER BY counter DESC""".format(uris)
	cursor = graph.run(query)
	return cursor

def intersect(a, b, c):
	return list(set(a) & set(b) & set(c))

def filter_datamodels(graph, datamodels):
	query = """MATCH (n:NoSQL)-[:TYPE]->(d:DataModel) 
	WHERE d.dbpedia_uri in {} RETURN n""".format(datamodels)
	cursor = graph.run(query)
	uris = [d['n']['uri'] for d in cursor]
	return uris

def filter_languages(graph, languages):
	query = """MATCH (n:NoSQL)-[:SUPPORTS]-(p:ProgrammingLanguage) 
	WHERE p.dbpedia_uri IN {0} RETURN n
	UNION
	MATCH (n:NoSQL)-[:SAME_AS]-(same:StackoverflowTag)-[r:SUPPORTS]-(p:ProgrammingLanguage) 
	WHERE p.dbpedia_uri IN {0} RETURN n
	UNION
	MATCH (n:NoSQL)-[:HAS]-(dr:DriverReposCluster)-[:FOR]-(p:ProgrammingLanguage) 
	WHERE dr.count > 0 AND p.dbpedia_uri IN {0} RETURN n""".format(languages)
	cursor = graph.run(query)
	uris = [n['n']['uri'] for n in cursor]
	return uris

def filter_movements(graph, movements):
	query = """MATCH (n:NoSQL)-[*1..2]->(m:Movement) 
	WHERE m.dbpedia_uri IN {0} RETURN n
	UNION
	MATCH (n:NoSQL)-[r:SAME_AS]->(same)-[:APPROVED_BY*1..2]->(m:Movement) 
	WHERE m.dbpedia_uri IN {0} RETURN n""".format(movements)
	cursor = graph.run(query)
	uris = [n['n']['uri'] for n in cursor]
	return uris

if __name__ == '__main__':
	filter()

	

