# -*- coding: utf-8 -*-
import scrapy
from scrapy.http import Request
from scrapy.loader import ItemLoader
from ..items import GenericItem
from ..items_cleaners import text_cleaner as tc
import json


class TechnologySpider(scrapy.Spider):
    name = "technologyTestSpider"

    def __init__(self, source_file, *args, **kwargs):
        super(TechnologySpider, self).__init__(*args, **kwargs)
        self.source_file = source_file
        with open(self.source_file, "r") as f:
            source = json.load(f)
            start_url = source['url']
            self.start_urls = [start_url]
            self.source = source

    def parse(self, response):
        tech_selectors = self._get_technologies_selectors(response)
        for s in tech_selectors:
            il = self._create_item_loader(s)
            yield il.load_item()

    def _get_technologies_selectors(self, response):
        xpath = self.source['root_xpath']
        technology_selectors = self._get_selectors(response, xpath)
        return technology_selectors

    def _create_item_loader(self, selector):
        item = GenericItem()
        il = ItemLoader(item=item, selector=selector)
        il = self._add_all_simple_fields(il)
        il = self._add_all_resource_group_fields(il)
        il = self._add_all_value_fields(il)
        il = self._add_label(il)
        return il

    def _get_selectors(self, selector, xpath):
        inner_selector = selector.xpath(xpath)
        return inner_selector

    def _add_all_simple_fields(self, item_loader):
        s_fields = self._get_fields_by_type('simple')
        for f in s_fields:
            item_loader = self._add_simple_field(item_loader, f)
        return item_loader

    def _add_all_resource_group_fields(self, item_loader):
        groups_fields = self._get_fields_by_type('resources_group')
        for group in groups_fields:
            item_loader = self._add_group_fields(item_loader, group)
        return item_loader

    def _add_all_value_fields(self, item_loader):
        value_fields = self._get_fields_by_type('value')
        for f in value_fields:
            resource = {'name': f.get('name_resource', ''), 'dbpedia_uri': f.get('dbpedia_uri')}
            f_name = f['name']
            item_loader.add_value(f_name, resource)
        return item_loader

    def _add_label(self, item_loader):
        label = self.source['label']
        item_loader.add_value('label', label)
        return item_loader

    def _add_simple_field(self, item_loader, field):
        name = field['name']
        xpath = field['xpath']
        item_loader.add_xpath(name, xpath)
        item_loader = self._add_not_founded_fields(item_loader, name)
        return item_loader

    def _add_group_fields(self, item_loader, group):
        group_name = group['name']
        group_xpath = group['xpath']
        group_resources = self._get_resources(item_loader, group_xpath)
        item_loader.add_value(group_name, group_resources)
        return item_loader

    def _get_resources(self, item_loader, group_xpath):
        group_selector = self._get_selectors_from_item_loader(item_loader, group_xpath)
        resources = self._get_resources_with_dbpedia_uri(group_selector)
        text = self._get_text(group_selector)
        group_resources = {}
        if text or resources:
            group_resources = {'text': text, 'resources': resources}
        return group_resources

    def _get_resources_with_dbpedia_uri(self, group_selector):
        links = group_selector.xpath(".//a[not(contains(@href, '#cite_note')) and not(contains(@href, 'action=edit'))]")
        resources = []
        for link in links:
            resource = self._create_resource_by_link(link)
            if resource:
                resources.append(resource)
        return resources

    def _get_text(self, group_selector):
        text = group_selector.xpath('.//text()').extract()
        text = ''.join(text)
        return text

    def _get_selectors_from_item_loader(self, item_loader, xpath):
        item_loader_selector = item_loader.selector
        selectors = item_loader_selector.xpath(xpath)
        return selectors

    def _create_resource_by_link(self, link):
        resource = {}
        name = link.xpath('.//text()').extract()
        dbpedia_uri = link.xpath('./@href').extract()
        if name and dbpedia_uri:
            resource = {'dbpedia_uri': dbpedia_uri[0], 'name': name[0]}
        return resource

    def _get_fields_by_type(self, type):
        type_fields = [f for f in self.source['fields'] if f['type'] == type]
        return type_fields

    def _add_not_founded_fields(self, il, name):
        value = il.get_collected_values(name)
        if not value:
            il.add_value(name, [])
        return il
