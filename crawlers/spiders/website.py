# -*- coding: utf-8 -*-
import scrapy
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule
from scrapy.http import Request
from scrapy.loader import ItemLoader
from ..items import GenericItem
import json


class WebsiteSpider(CrawlSpider):
    name = 'website'

    def __init__(self, source_file, *args, **kwargs):
        super(WebsiteSpider, self).__init__(*args, **kwargs)
        self._source_file = source_file

        # For some reason response.meta doesn't saved the source, so we added as a
        # instance variable
        with open(self._source_file, "r") as f:
            source = json.load(f)
            self._source = source

        # Add rules based on the source file
        rules = [Rule(LinkExtractor(restrict_xpaths=self._source['links_xpath']),
                      callback='parse_item')]
        if self._source.get('next_page_xpath', ''):
            rules.append(Rule(LinkExtractor(restrict_xpaths=self._source['next_page_xpath'])))

        # As we added the rules dynamically we need to compile it
        WebsiteSpider.rules = rules
        super(WebsiteSpider, self)._compile_rules()

    def start_requests(self):
        start_url = self._source['url']
        yield Request(start_url)

    def parse_item(self, response):
        label = self._source['label']
        fields = self._source['fields']

        item = GenericItem()
        il = ItemLoader(item=item, response=response)
        il = self._add_simple_fields(il, fields)
        il = self._add_resource_fields(il, fields)
        il = self._add_label(il, label)
        il.add_value('uri', response.url)
        return il.load_item()

    def _add_simple_fields(self, il, fields):
        simple_fields = self._get_fields_by_type('simple', fields)
        for f in simple_fields:
            name = f['name']
            xpath = f['xpath']
            il.add_xpath(name, xpath)
        return il

    def _add_resource_fields(self, il, fields):
        r_fields = self._get_fields_by_type('resource', fields)
        for r in r_fields:
            name = r['name']
            xpath = r['xpath']
            resources = self._get_resources(il, r, xpath)
            il.add_value(name, resources)
        return il

    def _add_label(self, il, label):
        il.add_value('label', label)
        return il

    def _get_resources(self, il, r, xpath):
        r_selectors = self._get_selectors(il.selector, xpath)
        fields = r['fields']
        values = []
        for s in r_selectors:
            values.append(self._get_inner_resource(s, fields))
        return values

    def _get_inner_resource(self, s, fields):
        resource = {}
        for f in fields:
            name = f['name']
            xpath = f['xpath']
            value = self._get_value(s, xpath)
            resource[name] = value
        return resource

    def _get_value(self, selector, xpath):
        value = selector.xpath(xpath).extract()
        return value

    def _get_selectors(self, response, xpath):
        selectors = response.xpath(xpath)
        return selectors

    def _get_fields_by_type(self, type, fields):
        type_fields = [f for f in fields if f['type'] == type]
        return type_fields
