from . import text_cleaner as cleaner
from urllib.parse import urljoin


class StacksharePostCleaner:

    def clean_item(self, item):
        item = self._clean_uri(item)
        item = self._clean_name(item)
        item = self._clean_label(item)
        item = self._clean_description(item)
        item = self._clean_repository(item)
        item = self._clean_forks(item)
        item = self._clean_image(item)
        item = self._clean_homepage(item)
        item = self._clean_repositories_stars(item)
        item = self._clean_fans(item)
        item = self._clean_votes(item)
        item = self._clean_favorites(item)
        item = self._clean_summary(item)
        item = self._clean_all_tools(item)
        item = self._clean_all_clients(item)
        item = self._clean_all_comments(item)
        return item

    def _clean_uri(self, item):
        uri = self._clean_simple_str_field(item, 'uri')
        uri = uri.replace('https://stackshare.io/', '')
        item['uri'] = uri
        return item

    def _clean_name(self, item):
        item['name'] = self._clean_simple_str_field(item, 'name')
        return item

    def _clean_label(self, item):
        item['label'] = self._clean_simple_str_field(item, 'label')
        return item

    def _clean_description(self, item):
        item['description'] = self._clean_simple_str_field(item, 'description')
        return item

    def _clean_repository(self, item):
        item['repository'] = self._clean_simple_str_field(item, 'repository')
        return item

    def _clean_forks(self, item):
        forks = self._clean_simple_str_field(item, 'forks')
        forks = self._convert_value_to_int(forks)
        item['forks'] = forks
        return item

    def _clean_image(self, item):
        item['image'] = self._clean_simple_str_field(item, 'image')
        return item

    def _clean_homepage(self, item):
        item['homepage'] = self._clean_simple_str_field(item, 'homepage')
        return item

    def _clean_repositories_stars(self, item):
        stars_list = item.get('repository_stars', [])
        if stars_list:
            stars = self._convert_value_to_int(stars_list[0])
        else:
            stars = 0
        item['repository_stars'] = stars
        return item

    def _clean_fans(self, item):
        fans = self._clean_list_str_field(item, 'fans')
        fans = self._convert_value_to_int(fans)
        item['fans'] = fans
        return item

    def _clean_votes(self, item):
        fans = self._clean_list_str_field(item, 'votes')
        fans = self._convert_value_to_int(fans)
        item['votes'] = fans
        return item

    def _clean_favorites(self, item):
        fans = self._clean_list_str_field(item, 'favorites')
        fans = self._convert_value_to_int(fans)
        item['favorites'] = fans
        return item

    def _clean_summary(self, item):
        summary = self._clean_list_str_field(item, 'summary')
        item['summary'] = summary
        return item

    def _clean_all_tools(self, item):
        tools = item.get('tools', [])
        clean_tools = [self._clean_resource(t) for t in tools]
        item['tools'] = clean_tools
        return item

    def _clean_all_clients(self, item):
        clients = item.get('clients', [])
        clean_clients = [self._clean_resource(c) for c in clients]
        item['clients'] = clean_clients
        return item

    def _clean_all_comments(self, item):
        comments = item.get('comments', [])
        clean_comments = [self._clean_comment(c) for c in comments]
        item['comments'] = clean_comments
        return item

    def _clean_resource(self, resource):
        relative_uri = self._clean_simple_str_field(resource, 'uri')
        uri = urljoin('https://stackshare.io', relative_uri)
        name = self._clean_simple_str_field(resource, 'thumbnail')
        image = self._clean_simple_str_field(resource, 'image')
        return {'uri': uri, 'name': name, 'thumbnail': image}

    def _clean_comment(self, comment):
        likes = cleaner.get_first_from_word_list(comment['likes'])
        likes = self._convert_value_to_int(likes)
        text = self._clean_list_str_field(comment, 'text')
        return {'likes': likes, 'text': text}

    def _clean_simple_str_field(self, item, field):
        value = item.get(field, [])
        value = cleaner.get_first_from_word_list(value)
        value = value.strip()
        return value

    def _clean_list_str_field(self, item, field):
        value = item.get(field, [])
        value = cleaner.get_word_list_as_sentence(value)
        value = value.strip()
        return value

    def _convert_value_to_int(self, value):
        if 'k' in value.lower():
            value = self._convert_to_thousands(value)
        elif not value:
            value = 0
        else:
            value = int(value)
        return value

    def _convert_to_thousands(self, value):
        value = value.lower()
        value = value.replace('k', '')
        value = float(value)
        value *= 1000
        value = int(value)
        return value