from . import text_cleaner as tc


class NoSQLCleaner:

    def __init__(self):
        self.nosql_stop_words = ['database', 'server']

    def clean_item(self, item):
        item = self._clean_label(item)
        item = self._clean_dbpedia_uri(item)
        item = self._clean_name(item)
        item = self._clean_summary(item)
        item = self._clean_latest_release(item)
        item = self._clean_rest_api(item)
        item = self._clean_sql_support(item)
        item = self._clean_resource_group(item, 'developers')
        item = self._clean_resource_group(item, 'drivers')
        item = self._clean_resource_group(item, 'licenses')
        item = self._clean_resource_group(item, 'developed_languages')
        item = self._clean_resource_group(item, 'transactions')
        item = self._clean_resource_group(item, 'API')
        item = self._clean_resource_group(item, 'native_storage')
        item = self._clean_keywords(item)
        return item

    def _clean_label(self, item):
        label = self._get_str(item, 'label')
        item['label'] = label
        return item

    def _clean_dbpedia_uri(self, item):
        dbpedia_uri = self._get_str(item, 'dbpedia_uri')
        if not self._check_if_is_external_link(dbpedia_uri):
            dbpedia_uri = dbpedia_uri.replace('/wiki/', '')
        else:
            dbpedia_uri = ''
        item['dbpedia_uri'] = dbpedia_uri
        return item

    def _check_if_is_external_link(self, dbpedia_uri):
        return 'http' in dbpedia_uri

    def _clean_name(self, item):
        name = self._get_str(item, 'name')
        name = self._replace_common_nosql_words(name)
        name = tc.remove_parenthesis(name)
        name = tc.remove_square_brackets(name)
        name = tc.remove_punctuations_chars_at_beginning(name)
        name = name.strip()
        item['name'] = name
        return item

    def _clean_summary(self, item):
        summary = self._get_str(item, 'summary')
        item['summary'] = summary
        return item

    def _clean_latest_release(self, item):
        version = item.get('latest_release', [])
        version = ''.join(version)
        item['latest_release'] = version
        return item

    def _clean_rest_api(self, item):
        text = self._get_str(item, 'REST_API')
        item['REST_API'] = text
        return item

    def _clean_sql_support(self, item):
        text = self._get_str(item, 'sql_support')
        item['sql_support'] = text
        return item

    def _clean_keywords(self, item):
        keywords = item.get('keywords')
        clean_keywords = []
        if keywords:
            keywords_field = self._get_first(keywords)
            resources = keywords_field.get('resources', [])
            resources = self._clean_dbpedia_resources(resources)
            clean_keywords = resources
        item['keywords'] = clean_keywords
        return item

    def _clean_resource_group(self, item, field_name):
        field_list = item.get(field_name, [])
        if field_list:
            field = self._get_first(field_list)
            field = self._clean_resource_group_text(field)
            field = self._clean_resource_group_resources(field)
            item[field_name] = field
        return item

    def _clean_resource_group_text(self, field):
        text = field.get('text', '')
        text = self._clean_text(text)
        field['text'] = text
        return field

    def _clean_resource_group_resources(self, field):
        resources = field.get('resources', [])
        clean_resources = self._clean_dbpedia_resources(resources)
        field['resources'] = clean_resources
        return field

    def _clean_text(self, text):
        if not tc.has_letters(text):
            return ''
        return text

    def _clean_dbpedia_resources(self, resources):
        clean_resources = []
        for r in resources:
            item = {}
            dbpedia_uri = r['dbpedia_uri'].replace('/wiki/', '')
            name = r['name']
            if self._is_external(dbpedia_uri):
                external_uri = dbpedia_uri
                dbpedia_uri = ''
                item['external_uri'] = external_uri
            item['dbpedia_uri'] = dbpedia_uri
            item['name'] = name
            clean_resources.append(item)
        return clean_resources

    def _is_external(self, dbpedia_uri):
        return dbpedia_uri.startswith('http')

    def _get_str(self, item, field):
        values = item.get(field, [])
        if len(values) > 1:
            return tc.get_word_list_as_sentence(values)
        elif len(values) == 1:
            return tc.get_first_from_word_list(values)
        return ''

    def _replace_common_nosql_words(self, name):
        for sw in self.nosql_stop_words:
            name = tc.remove_word_at_end_or_start(sw, name)
        return name

    def _get_first(self, rs):
        if rs:
            return rs[0]
        return []
