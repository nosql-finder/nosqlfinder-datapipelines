import re
import nltk
from nltk.corpus import stopwords


def remove_square_brackets(value):
    value = re.sub(r'\[.*?\]', '', value)
    value = value.strip()
    return value


def remove_parenthesis(s):
    value = re.sub(r'\(.*\)', '', s)
    return value


def remove_punctuations_chars_at_beginning(s):
    s = re.sub(r'^[^0-9a-zA-Z]', '', s)
    s = s.strip()
    return s


def remove_punctuations_chars_at_end(s):
    s = re.sub(r'[^0-9a-zA-Z]$', '', s)
    s = s.strip()
    return s


def get_text_between_tags(html):
    return re.sub(r'<.*?>', '', html)


def has_word(word, text):
    regex = r'\b{word}\b'.format(word=word)
    search_obj = re.search(regex, text)
    return bool(search_obj)


def has_characters(text):
    search_obj = re.search(r'[a-zA-Z]+', text)
    return bool(search_obj)


def not_has_unclosed_parenthesis(str):
    count = 0
    for i in str:
        if i == "(":
            count += 1
        elif i == ")":
            count -= 1
        if count < 0:
            return False
    return count == 0


def remove_word_at_end_or_start(word, sentence):
    sentence = sentence.lower()
    if sentence.startswith(word) or sentence.endswith(word):
        sentence = sentence.replace(word, '')
    return sentence


def has_letters(s):
    search_obj = re.search(r'[a-zA-Z]', s)
    return bool(search_obj)


def has_letters_or_numbers(s):
    search_obj = re.search(r'[0-9a-zA-Z]', s)
    return bool(search_obj)


def remove_conjunctions_at_end(s):
    words = nltk.word_tokenize(s)
    tagged_words = nltk.pos_tag(words)
    if tagged_words:
        last_tagged_word = tagged_words[-1]
        if last_tagged_word[1] == 'CC':
            s = s[:-2]
    return s


def split_list_with_all_conjunctions(value):
    value = tag_conjunctions(value)
    values = value.split('_CONJ')
    values = [v.strip() for v in values]
    values = filter_etc(values)
    return values


def tag_conjunctions(value):
    value = value.replace(',', '_CONJ')
    conjunctions = get_conjunctions(value)
    for c in conjunctions:
        regex = '[ ]{}[ ]'.format(c)
        value = re.sub(regex, '_CONJ', value)
    return value


def filter_etc(values):
    values = [v for v in values if not v.startswith('etc')]
    return values


def has_conjunctions_or_commas(value):
    has_commas = ',' in value
    has_conjunctions = len(get_conjunctions(value)) > 0
    return has_commas or has_conjunctions


def get_conjunctions(value):
    words = nltk.word_tokenize(value)
    tagged_words = nltk.pos_tag(words)
    conjunctions = set()
    for t in tagged_words:
        if t[1].startswith('CC'):
            conjunctions.add(t[0])
    return conjunctions


def ends_or_starts_with_stop_words(s):
    starts = starts_with_stop_word(s)
    ends = ends_with_stop_word(s)
    return starts or ends


def starts_with_stop_word(s):
    s = s.strip()
    words = nltk.word_tokenize(s)
    found = False
    if words:
        first_word = words[0]
        found = is_stop_word(first_word)
    return found


def ends_with_stop_word(s):
    s = s.strip()
    words = nltk.word_tokenize(s)
    found = False
    if words:
        last_word = words[-1]
        found = is_stop_word(last_word)
    return found


def is_stop_word(word):
    for w in stopwords.words('english'):
        if word in w:
            return True
    return False


def has_preposition(s):
    words = nltk.word_tokenize(s)
    tagged_words = nltk.pos_tag(words)
    tags = [t[1] for t in tagged_words]
    return 'IN' in tags


# Functions when working with scrapy


def get_first_from_word_list(values):
    clean_val = ''
    if values:
        clean_val = values[0]
    return clean_val


def get_word_list_as_sentence(words):
    sentence = ''.join(words)
    return sentence


def remove_cite(value):
    clean_v = re.sub(r'\[\d+\]', '', value)
    return clean_v
