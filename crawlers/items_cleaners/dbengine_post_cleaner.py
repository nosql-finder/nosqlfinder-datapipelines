from . import text_cleaner as cleaner


class DBEnginePostCleaner:

    def clean_item(self, item):
        item = self._clean_label(item)
        item = self._clean_uri(item)
        item = self._clean_name(item)
        item = self._clean_summary(item)
        item = self._clean_score(item)
        item = self._clean_global_rank(item)
        item = self._clean_category_rank(item)
        item = self._add_simple_field(item, 'homepage')
        item = self._add_simple_field(item, 'documentation')
        item = self._add_simple_field(item, 'developer')
        item = self._add_simple_field(item, 'initial_release')
        item = self._add_simple_field(item, 'current_release')
        item = self._add_simple_field(item, 'license')
        item = self._add_support(item, 'sql_support')
        item = self._add_support(item, 'cloud_based')
        item = self._clean_inner_fields(item, 'developed_languages')
        item = self._add_support(item, 'concurrency')
        item = self._add_support(item, 'data_scheme')
        item = self._clean_data_types(item)
        item = self._add_support(item, 'xml_support')
        item = self.add_drivers_with_status(item)
        item = self._add_support(item, 'durability')
        item = self._clean_uses(item)
        return item

    def _clean_label(self, item):
        label = item.get('label', [])
        label = cleaner.get_first_from_word_list(label)
        item['label'] = label
        return item

    def _clean_uri(self, item):
        uri = item.get('uri', [])
        uri = cleaner.get_first_from_word_list(uri)
        uri = uri.replace('https://db-engines.com/en/system/', '')
        item['uri'] = uri
        return item

    def _clean_name(self, item):
        name = item.get('name', [])
        name = cleaner.get_first_from_word_list(name)
        item['name'] = name
        return item

    def _clean_summary(self, item):
        summary = item.get('summary', [])
        summary = cleaner.get_word_list_as_sentence(summary)
        item['summary'] = summary
        return item

    def _clean_score(self, item):
        score = item.get('score', [])
        score = cleaner.get_first_from_word_list(score)
        score = self._to_float(score)
        item['score'] = score
        return item

    def _clean_global_rank(self, item):
        rank = item.get('global_rank', [])
        rank = self._clean_rank(rank)
        item['global_rank'] = rank
        return item

    def _clean_category_rank(self, item):
        rank = item.get('category_rank', [])
        rank = self._clean_rank(rank)
        item['category_rank'] = rank
        return item

    def _add_simple_field(self, item, field_name):
        field = item.get(field_name, [])
        field = cleaner.get_first_from_word_list(field)
        item[field_name] = field
        return item

    def _clean_data_types(self, item):
        fields = item.get('data_types', [])
        clean_ds = []
        for f in fields:
            clean_ds += self._split_field(f)
        clean_ds = self._add_basic_data_types(clean_ds)
        clean_ds = self._remove_no_types(clean_ds)
        clean_ds = self._filter_datatypes(clean_ds)
        item['data_types'] = clean_ds
        return item

    def _add_basic_data_types(self, datatypes):
        has_one_type = len(datatypes) == 1
        basic_types = ['Numbers', 'Strings', 'Booleans', 'Null', 'Lists']
        yes_types = [d for d in datatypes if 'yes' in d.lower()]
        if yes_types and has_one_type:
            return basic_types
        return datatypes

    def _remove_no_types(self, datatypes):
        has_one_type = len(datatypes) == 1
        no_types = [d for d in datatypes if 'no' in d.lower()]
        if no_types and has_one_type:
            return []
        return datatypes

    def _filter_datatypes(self, datatypes):
        ds = [d for d in datatypes if 'yes' not in d.lower()]
        ds = [d for d in ds if 'no' not in d.lower()]
        return ds

    def _clean_inner_fields(self, item, field_name):
        fields = item.get(field_name, [])
        clean_fields = []
        for f in fields:
            clean_fields += self._split_field(f)
        item[field_name] = clean_fields
        return item

    def _split_field(self, field):
        has_conjunctions = cleaner.has_conjunctions_or_commas(field)
        if has_conjunctions:
            field = cleaner.split_list_with_all_conjunctions(field)
        else:
            field = [field]
        return field

    def _add_support(self, item, field_name):
        support = item.get(field_name, [])
        support = cleaner.get_first_from_word_list(support)
        is_supported = self._is_supported(support)
        item[field_name] = is_supported
        return item

    def add_drivers_with_status(self, item):
        drivers = item.get('drivers')
        clean_languages = []
        if drivers:
            drivers_status = self.get_drivers_with_status(drivers[0])
            clean_languages = self.process_drivers(drivers_status)
        item['drivers'] = clean_languages
        return item

    def get_drivers_with_status(self, html):
        langs_html = html.split('<br>')
        languages = [cleaner.get_text_between_tags(l) for l in langs_html]
        return languages

    def process_drivers(self, languages):
        clean_langs = []
        for lang in languages:
            lang_status = lang.split(' ')
            if lang_status:
                lang_data = self.get_status_and_name(lang_status)
                clean_langs.append(lang_data)
        return clean_langs

    def get_status_and_name(self, language_status):
        name = language_status[0]
        try:
            if language_status[1].startswith('('):
                name += language_status[1]
                status = language_status[2]
            else:
                status = language_status[1]
        except:
            status = 'unknown'
        data = {'name': name, 'status': status}
        return data

    def _clean_uses(self, item):
        scenarios = item.get('uses', [])
        clean_scenarios = []
        if scenarios:
            clean_scenarios = [cleaner.remove_parenthesis(c) for c in scenarios]
            clean_scenarios = [c.strip() for c in clean_scenarios if c.strip()]
        item['uses'] = clean_scenarios
        return item

    def _clean_rank(self, rank):
        rank = cleaner.get_first_from_word_list(rank)
        rank = rank.replace('#', '')
        rank = self._to_int(rank)
        return rank

    def _is_supported(self, support):
        if 'yes' in support:
            support = True
        elif 'no' in support:
            support = False
        return support

    def _to_float(self, score):
        try:
            return float(score)
        except:
            return 0.0

    def _to_int(self, rank):
        try:
            return int(rank)
        except:
            return 0
