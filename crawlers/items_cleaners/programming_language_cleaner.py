from . import text_cleaner as cleaner


class ProgrammingLanguageCleaner:

    def clean_item(self, item):
        item = self._clean_label(item)
        item = self._clean_dbpedia_uri(item)
        item = self._clean_name(item)
        item = self._clean_uses(item)
        return item

    def _clean_label(self, item):
        label = item.get('label', [])
        label = cleaner.get_first_from_word_list(label)
        item['label'] = label
        return item

    def _clean_dbpedia_uri(self, item):
        uris = item.get('dbpedia_uri', [])
        uri = cleaner.get_first_from_word_list(uris)
        uri = uri.replace('/wiki/', '')
        item['dbpedia_uri'] = uri
        return item

    def _clean_name(self, item):
        names = item.get('name', [])
        name = cleaner.get_word_list_as_sentence(names)
        name = cleaner.remove_parenthesis(name)
        name = cleaner.remove_punctuations_chars_at_beginning(name)
        name = cleaner.remove_punctuations_chars_at_end(name)
        item['name'] = name
        return item

    def _clean_uses(self, item):
        uses = item.get('uses', [])
        clean_us = []
        for u in uses:
            clean_us += self._split_use(u)
        item['uses'] = clean_us
        return item

    def _split_use(self, use):
        has_conjunctions = cleaner.has_conjunctions_or_commas(use)
        if has_conjunctions:
            use = cleaner.split_list_with_all_conjunctions(use)
        else:
            use = [use]
        return use