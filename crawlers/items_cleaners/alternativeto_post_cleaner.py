from . import text_cleaner as cleaner
from urllib.parse import urljoin


class AlternativeToPostCleaner:

    def clean_item(self, item):
        item = self._clean_uri(item)
        item = self._clean_name(item)
        item = self._clean_label(item)
        item = self._clean_homepage(item)
        item = self._clean_likes(item)
        item = self._clean_full_description(item)
        item = self._clean_labels(item)
        item = self._clean_summary(item)
        item = self._clean_alternatives(item)
        item = self._clean_thumbnail(item)
        return item

    def _clean_uri(self, item):
        uri = self._clean_simple_str_field(item, 'uri')
        uri = uri.replace('https://alternativeto.net/software/', '')
        item['uri'] = uri
        return item

    def _clean_name(self, item):
        item['name'] = self._clean_simple_str_field(item, 'name')
        return item

    def _clean_label(self, item):
        item['label'] = self._clean_simple_str_field(item, 'label')
        return item

    def _clean_homepage(self, item):
        item['homepage'] = self._clean_simple_str_field(item, 'homepage')
        return item

    def _clean_likes(self, item):
        likes = self._clean_simple_str_field(item, 'likes')
        item['likes'] = self._to_int(likes)
        return item

    def _clean_full_description(self, item):
        item['full_description'] = self._clean_list_str_field(item, 'full_description')
        return item

    def _clean_labels(self, item):
        labels = item.get('labels', [])
        labels = [l for l in labels if l.strip()]
        item['labels'] = labels
        return item

    def _clean_summary(self, item):
        item['summary'] = self._clean_list_str_field(item, 'summary')
        return item

    def _clean_thumbnail(self, item):
        item['thumbnail'] = self._clean_simple_str_field(item, 'thumbnail')
        return item

    def _clean_alternatives(self, item):
        alternatives = item.get('alternatives', [])
        clean_alternatives = []
        for al in alternatives:
            likes = self._clean_simple_str_field(al, 'likes')
            likes = self._to_int(likes)
            uri = self._clean_simple_str_field(al, 'uri')
            uri = urljoin('https://alternativeto.net', uri)
            name = self._clean_simple_str_field(al, 'name')
            thumbnail = self._clean_simple_str_field(al, 'thumbnail')
            thumbnail = urljoin('https://alternativeto.net', thumbnail)
            alternative = {'likes': likes, 'uri': uri, 'name': name, 'thumbnail': thumbnail}
            clean_alternatives.append(alternative)
        item['alternatives'] = clean_alternatives
        return item

    def _clean_simple_str_field(self, item, field):
        value = item.get(field, [])
        value = cleaner.get_first_from_word_list(value)
        value = value.strip()
        return value

    def _clean_list_str_field(self, item, field):
        value = item.get(field, [])
        value = cleaner.get_word_list_as_sentence(value)
        value = value.strip()
        return value

    def _to_int(self, value):
        if value:
            return int(value)
        return 0