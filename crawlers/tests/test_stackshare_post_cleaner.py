import unittest
from ..items_cleaners.stackshare_post_cleaner import StacksharePostCleaner


class StacksharePostCleanerTest(unittest.TestCase):

    def setUp(self):
        self.stackshare_cleaner = StacksharePostCleaner()

    def test_clean_repositories_stars_converts_str_to_number(self):
        item = {'repository_stars': ['\n5\n']}
        item = self.stackshare_cleaner._clean_repositories_stars(item)
        self.assertEqual(5, item['repository_stars'])

    def test_clean_repositories_stars_if_empty_should_return_zero(self):
        item = {'repository_stars': ['']}
        item = self.stackshare_cleaner._clean_repositories_stars(item)
        self.assertEqual(0, item['repository_stars'])

    def test_clean_repositories_stars_if_has_K_should_return_thousands(self):
        item = {'repository_stars': ["\n3.16K\n"]}
        item = self.stackshare_cleaner._clean_repositories_stars(item)
        self.assertEqual(3160, item['repository_stars'])

    def test_clean_fans_converts_str_to_number(self):
        item = {'fans': ["\n", "\n3\n"]}
        item = self.stackshare_cleaner._clean_fans(item)
        self.assertEqual(3, item['fans'])

    def test_clean_fans_converts_if_empty_should_return_zero(self):
        item = {'fans': ['']}
        item = self.stackshare_cleaner._clean_fans(item)
        self.assertEqual(0, item['fans'])

    def test_clean_fans_if_has_K_should_return_thousands(self):
        item = {'fans': ["\n", "\n6k\n"]}
        item = self.stackshare_cleaner._clean_fans(item)
        self.assertEqual(6000, item['fans'])

    def test_clean_votes_converts_str_to_number(self):
        item = {'votes': ["\n", "\n3\n"]}
        item = self.stackshare_cleaner._clean_votes(item)
        self.assertEqual(3, item['votes'])

    def test_clean_votes_converts_if_empty_should_return_zero(self):
        item = {'votes': ['']}
        item = self.stackshare_cleaner._clean_votes(item)
        self.assertEqual(0, item['votes'])

    def test_clean_votes_if_has_K_should_return_thousands(self):
        item = {'votes': ["\n", "\n6k\n"]}
        item = self.stackshare_cleaner._clean_votes(item)
        self.assertEqual(6000, item['votes'])

    def test_clean_favorites_converts_str_to_number(self):
        item = {'favorites': ['23']}
        item = self.stackshare_cleaner._clean_favorites(item)
        self.assertEqual(23, item['favorites'])

    def test_clean_favorites_converts_if_empty_should_return_zero(self):
        item = {'favorites': ['']}
        item = self.stackshare_cleaner._clean_favorites(item)
        self.assertEqual(0, item['favorites'])

    def test_clean_favorites_if_has_K_should_return_thousands(self):
        item = {'favorites': ['2k']}
        item = self.stackshare_cleaner._clean_favorites(item)
        self.assertEqual(2000, item['favorites'])

    def test_clean_forks_converts_str_to_number(self):
        item = {'forks': ["\n96\n"]}
        item = self.stackshare_cleaner._clean_forks(item)
        self.assertEqual(96, item['forks'])

    def test_clean_homepage_list_should_return_the_first(self):
        item = {'homepage': ['http://www.clustrix.com/']}
        item = self.stackshare_cleaner._clean_homepage(item)
        self.assertEqual('http://www.clustrix.com/', item['homepage'])

    def test_clean_homepage_list_if_empty_should_return_empty_str(self):
        item = {'homepage': []}
        item = self.stackshare_cleaner._clean_homepage(item)
        self.assertEqual('', item['homepage'])

    def test_clean_summary_list_should_return_the_first(self):
        item = {'summary': ['This is the summary']}
        item = self.stackshare_cleaner._clean_summary(item)
        self.assertEqual('This is the summary', item['summary'])

    def test_clean_summary_list_if_empty_should_return_empty_str(self):
        item = {'summary': []}
        item = self.stackshare_cleaner._clean_summary(item)
        self.assertEqual('', item['summary'])

    def test_clean_name_should_return_the_first(self):
        item = {'name': ['Allegrograph']}
        item = self.stackshare_cleaner._clean_name(item)
        self.assertEqual('Allegrograph', item['name'])

    def test_clean_label_should_return_the_first(self):
        item = {'label': ['StacksharePost']}
        item = self.stackshare_cleaner._clean_label(item)
        self.assertEqual('StacksharePost', item['label'])

    def test_clean_description_should_return_the_first(self):
        item = {'description': ['This is the description']}
        item = self.stackshare_cleaner._clean_description(item)
        self.assertEqual('This is the description', item['description'])

    def test_clean_repository_should_return_the_first(self):
        item = {'repository': ['This is the repository']}
        item = self.stackshare_cleaner._clean_repository(item)
        self.assertEqual('This is the repository', item['repository'])

    def test_clean_image_should_return_the_first(self):
        item = {'image': ['test.png']}
        item = self.stackshare_cleaner._clean_image(item)
        self.assertEqual('test.png', item['image'])

    def test_clean_tools_should_return_first_attrs_from_every_resource(self):
        item = {"tools": [{"uri": ["/postgresql"], "name": ["PostgreSQL"], "image": ["test.png"]}]}
        item = self.stackshare_cleaner._clean_all_tools(item)
        clean_item = {'tools': [{'uri': 'postgresql', 'name': 'PostgreSQL', 'image': 'test.png'}]}
        self.assertEqual(clean_item, item)

    def test_clean_comments_should_return_first_attrs_for_every_comment(self):
        item = {"comments": [{"count": ["1"], "text": ["\n", "ClustrixDB is scale-out", "\n"]},
                            {"count": ["1"], "text": ["\n", "Very High Connection Count", "\n"]}]}
        item = self.stackshare_cleaner._clean_comments(item)
        clean_item = {'comments': [{'likes': 1, 'text': 'ClustrixDB is scale-out'},
                                   {'likes': 1, 'text': 'Very High Connection Count'}]}
        self.assertEqual(clean_item, item)

    def test_clean_clients_should_return_first_attrs_for_every_client(self):
        item = {'clients': [{"uri": ["/nuxeo/nuxeo"], "name": ["Nuxeo"], "image": ["nuxeo-logo.png"]}]}
        item = self.stackshare_cleaner._clean_all_clients(item)
        clean_item = {'clients': [{'uri': 'nuxeo/nuxeo', 'name': 'Nuxeo', 'image': 'nuxeo-logo.png'}]}
        self.assertEqual(clean_item, item)
