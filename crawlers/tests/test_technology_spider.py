import unittest
from ..spiders.technology import TechnologySpider
from .responses import fake_response_from_file
import json

class TechnologySpiderTest(unittest.TestCase):

    def setUp(self):
        self.source_file = './technologies/tests/responses/test_sources.json'
        self.spider = TechnologySpider(source_file=self.source_file)
        self.items = self.spider.parse(fake_response_from_file('table_test.html'))
        with open(self.source_file) as f:
            source = json.load(f)
        self.fields = source['fields']

    def test_items_count(self):
        self.assertEqual(9, len(list(self.items)))

    def test_items_should_have_just_one_label(self):
        for item in self.items:
            length_label = len(item['label'])
            self.assertEqual(length_label, 1)

    def test_items_names_should_be_at_least_one_str(self):
        for item in self.items:
            length_name = len(item['name'])
            self.assertTrue(length_name > 0)

    def test_items_dbpedia_uri_should_not_be_external(self):
        for item in self.items:
            dbpedia_uri = item.get('dbpedia_uri', [])
            if dbpedia_uri:
                self.assertIn('/wiki/', dbpedia_uri[0])

    def test_items_dbpedia_uri_should_not_be_red_link(self):
        for item in self.items:
            dbpedia_uri = item.get('dbpedia_uri', [])
            if dbpedia_uri:
                self.assertNotIn('action=edit&redlink', dbpedia_uri[0])

    def test_items_dbpedia_uri_should_not_be_a_cite_note(self):
        for item in self.items:
            dbpedia_uri = item.get('dbpedia_uri', [])
            if dbpedia_uri:
                starts_with_cite = dbpedia_uri[0].startswith('#cite_note')
                self.assertFalse(starts_with_cite)

    def test_that_items_should_have_all_simple_fields(self):
        simple_fields_names = self._get_fields_by_type('simple')
        for item in self.items:
            for name in simple_fields_names:
                self.assertIsInstance(item[name], list)

    def test_that_items_should_have_all_resource_groups_fields(self):
        resource_groups_fields = self._get_resources_group_fields()
        for item in self.items:
            for name in resource_groups_fields:
                self.assertIsInstance(item[name], list)

    def test_items_resource_groups_have_a_dict(self):
        resource_groups_fields = self._get_resources_group_fields()
        for item in self.items:
            for name in resource_groups_fields:
                self.assertIsInstance(item[name][0], dict)

    def test_items_resource_groups_have_no_both_text_and_resources_empty(self):
        resource_groups_fields = self._get_resources_group_fields()
        for item in self.items:
            for name in resource_groups_fields:
                resource_group = item[name][0]
                if resource_group:
                    has_resources = bool(resource_group['resources'])
                    has_text = bool(resource_group['text'])
                    self.assertTrue(has_resources or has_text, 'The error: {}'.format(item))

    def test_that_items_should_have_valid_resource_groups_fields(self):
        resources_group_fields = self._get_resources_group_fields()
        for item in self.items:
            self._test_resource_groups_fields_have_valid_resources(item, resources_group_fields)

    def _test_resource_groups_fields_have_valid_resources(self, item, resources_group_fields):
        for group in resources_group_fields:
            self._test_resources_have_dbpedia_uri_and_name(item[group][0])
            self._test_resources_should_not_have_name_with_unclosed_parenthesis(item[group][0])
            self._test_resources_dbpedia_uri_should_not_be_a_cite_note(item[group][0])

    def _test_resources_have_dbpedia_uri_and_name(self, group):
        resources = group.get('resources', [])
        for r in resources:
            fields = r.keys()
            self.assertIn('dbpedia_uri', fields)
            self.assertIn('name', fields)

    def _test_resources_should_not_have_name_with_unclosed_parenthesis(self, group):
        resources = group.get('resources', [])
        for r in resources:
            self._test_resource_should_not_have_name_without_unclosed_parenthesis(r)

    def _test_resource_should_not_have_name_without_unclosed_parenthesis(self, field):
        if '(' in field['name']:
            self.assertIn(')', field['name'])

    def _test_resources_dbpedia_uri_should_not_be_a_cite_note(self, group):
        resources = group.get('resources', [])
        for r in resources:
            starts_with_cite = r['dbpedia_uri'].startswith('#cite_note')
            self.assertFalse(starts_with_cite, '{} is a cite note'.format(r))

    def _get_resources_group_fields(self):
        return self._get_fields_by_type('resources_group')

    def _get_fields_by_type(self, type):
        return [f['name'] for f in self.fields if f['type'] == type]
    #
    # def test_all_items(self):
    #     for i in self.items:
    #         print(i)
    #         print("*" * 30)
