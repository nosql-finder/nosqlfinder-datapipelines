import unittest
from ..items_cleaners.nosql_cleaner import NoSQLCleaner


class NoSQLCleanerTest(unittest.TestCase):

    def setUp(self):
        self.cleaner = NoSQLCleaner()

    def test_label_should_be_string(self):
        item = {'label': ['NoSQL']}
        item = self.cleaner._clean_label(item)
        self.assertEqual('NoSQL', item['label'])

    def test_dbpedia_uri_should_not_have_wiki_prefix(self):
        item = {'dbpedia_uri': ['/wiki/Basex']}
        item = self.cleaner._clean_dbpedia_uri(item)
        self.assertEqual('Basex', item['dbpedia_uri'])

    def test_name_should_be_a_lower_string(self):
        item = {'name': ['BaseX']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('basex', item['name'])

    def test_name_should_not_have_parenthesis(self):
        item = {'name': ['Cassandra (Database)']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('cassandra', item['name'])

    def test_name_should_not_end_with_word_database(self):
        item = {'name': ['Couchdb Database']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('couchdb', item['name'])

    def test_name_shoud_not_end_with_word_server(self):
        item = {'name': ['Couchbase server']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('couchbase', item['name'])

    def test_name_should_not_remove_middle_database_word(self):
        item = {'name': ['ObjectDatabase++']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('objectdatabase++', item['name'])

    def test_name_should_not_end_or_start_with_space(self):
        item = {'name': [' Basex ']}
        item = self.cleaner._clean_name(item)
        self.assertEqual('basex', item['name'])

    def test_summary_shoud_be_a_string(self):
        item = {"summary": ["ACID", " transaction supported, ", "JSON", " only"]}
        item = self.cleaner._clean_summary(item)
        self.assertEqual('ACID transaction supported, JSON only', item['summary'])

    def test_latest_release_version_should_be_a_string(self):
        item = {"latest_release_version": ["5.1 (May 2015)"]}
        item = self.cleaner._clean_latest_release_version(item)
        self.assertEqual('5.1 (May 2015)', item['latest_release_version'])

    def test_keywords_dbpedia_uri_should_not_have_wiki_prefix(self):
        item = {'keywords': [{'dbpedia_uri': ['/wiki/Middleware'], 'name': ['Middleware']}]}
        item = self.cleaner._clean_keywords(item)
        clean_item = {"keywords": [{'dbpedia_uri': 'Middleware', 'name': 'Middleware'}]}
        self.assertEqual(clean_item, item)

    def test_licenses_dbpedia_uri_should_not_have_wiki_prefix(self):
        item = {"licenses": [{"dbpedia_uri": ["/wiki/Proprietary_software"], "name": ["Proprietary"]}]}
        item = self.cleaner._clean_licenses(item)
        clean_item = {"licenses": [{"dbpedia_uri": "Proprietary_software", "name": "Proprietary"}]}
        self.assertEqual(clean_item, item)

    def test_drivers_dbpedia_uri_should_not_have_wiki_prefix(self):
        item = {"drivers": [{"name": ["C"], "dbpedia_uri": ["/wiki/C_(programming_language)"]}]}
        item = self.cleaner._clean_drivers(item)
        clean_item = {"drivers": [{"name": "C", "dbpedia_uri": "C_(programming_language)"}]}
        self.assertEqual(clean_item, item)

    def test_developers_dbepedia_uri_should_not_have_wiki_prefix(self):
        item = {"developers": [{"name": ["Qualcomm"], "dbpedia_uri": ["/wiki/Qualcomm"]}]}
        item = self.cleaner._clean_developers(item)
        clean_item = {"developers": [{"name": "Qualcomm", "dbpedia_uri": "Qualcomm"}]}
        self.assertEqual(clean_item, item)

    def test_sql_support_should_be_a_string(self):
        item = {"sql_support": ["SQL subset. Object notation allowed. Supports embedded SQL, dynamic SQL and xDBC access."]}
        item = self.cleaner._clean_sql_support(item)
        clean_item = {"sql_support": "SQL subset. Object notation allowed. Supports embedded SQL, dynamic SQL and xDBC access."}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_true_if_yes(self):
        item = {'REST_API': ['Yes']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': True}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_true_if_yes_and_more_text(self):
        item = {'REST_API': ['Yes', '[7]']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': True}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_false_if_no(self):
        item = {'REST_API': ['No']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': False}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_false_if_no_and_more_text(self):
        item = {'REST_API': ['No', '[7]']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': False}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_text(self):
        item = {'REST_API': ['Only in beta']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': 'Only in beta'}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_text_if_letters_yes_in_text(self):
        item = {'REST_API': ['yesterday was removed']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': 'yesterday was removed'}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_text_if_letters_no_in_text(self):
        item = {'REST_API': ['nonetheless is good']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': 'nonetheless is good'}
        self.assertEqual(clean_item, item)

    def test_rest_api_should_be_empty_string_if_question_mark(self):
        item = {'REST_API': ['?']}
        item = self.cleaner._clean_rest_api(item)
        clean_item = {'REST_API': ''}
        self.assertEqual(clean_item, item)

    def test_clean_item_should_return_all_fields(self):
        item = {"datamodel": [{"name": "document", "dbpedia_uri": "Document-oriented_database"}],
                "REST_API": ["Yes"],
                "summary": ["Support for XML"],
                "name": ["BaseX"],
                "dbpedia_uri": ["/wiki/BaseX"],
                "licenses": [{"name": ["BSD License"], "dbpedia_uri": ["/wiki/BSD_License"]}],
                "drivers": [{"name": ["Java"], "dbpedia_uri": ["/wiki/Java_(programming_language)"]},
                            {"name": ["XQuery"], "dbpedia_uri": ["/wiki/XQuery"]}], "label": ["NoSQL"]
                }
        item = self.cleaner.clean_item(item)
        clean_item_keys = ['name', 'dbpedia_uri', 'datamodel', 'licenses', 'developers', 'summary',
                           'label', 'drivers', 'keywords', 'REST_API', 'latest_release_version', 'sql_support']
        self.assertCountEqual(clean_item_keys, item.keys())






