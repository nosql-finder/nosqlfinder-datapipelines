# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

from .items_cleaners.nosql_cleaner import NoSQLCleaner
from .items_cleaners.programming_language_cleaner import ProgrammingLanguageCleaner
from .items_cleaners.dbengine_post_cleaner import DBEnginePostCleaner
from .items_cleaners.stackshare_post_cleaner import StacksharePostCleaner
from .items_cleaners.alternativeto_post_cleaner import AlternativeToPostCleaner
from .items_cleaners import text_cleaner


class TechnologiesPipeline(object):

    def process_item(self, item, spider):
        label = item.get('label', [])
        if label:
            cleaner = self.get_cleaner(label)
            item = self._clean_item(cleaner, item)
        return item

    def _clean_item(self, cleaner, item):
        if cleaner:
            item = cleaner.clean_item(item)
        return item

    def get_cleaner(self, label):
        cleaner = None
        if label[0] == 'NoSQL':
            cleaner = NoSQLCleaner()
        elif label[0] == 'ProgrammingLanguage':
            cleaner = ProgrammingLanguageCleaner()
        elif label[0] == 'DBEnginePost':
            cleaner = DBEnginePostCleaner()
        elif label[0] == 'StacksharePost':
            cleaner = StacksharePostCleaner()
        elif label[0] == 'AlternativeToPost':
            cleaner = AlternativeToPostCleaner()
        return cleaner


class LicenseMinnerPipeline(object):

    def __init__(self):
        self.free_license = {'dbpedia_uri': 'Free_software',
                             'name': 'Free'}
        self.proprietary_license = {'dbpedia_uri': 'Proprietary_software',
                                   'name': 'Proprietary'}
        self.commercial_license = {'dbpedia_uri': 'Commercial_software',
                                   'name': 'Commercial'}
        self.gpl_license = { 'dbpedia_uri': 'GNU_General_Public_License',
                             'name': 'GNU General Public License'}
        self.bsd_license = {'dbpedia_uri': 'BSD_licenses',
                            'name': 'BSD licenses'}
        self.apache_license = {'dbpedia_uri': 'Apache_License',
                               'name': 'Apache License'}
        self.lgpl_license = {'dbpedia_uri': 'GNU_Lesser_General_Public_License',
                             'name': 'GNU Lesser General Public License'}
        self.agpl_license = {'dbpedia_uri': 'Affero_General_Public_License',
                             'name': 'Affero General Public License'}


    def process_item(self, item, spider):
        label = item.get('label', '')
        if label == 'NoSQL':
            item = self._clean_licenses(item)
        return item

    def _clean_licenses(self, item):
        item = self._create_licenses_field_if_empty(item)
        licenses = item.get('licenses')
        resources = licenses.get('resources', [])
        text = licenses.get('text', '')
        licenses_in_text = self._find_licenses_in_text(text)
        item['licenses']['resources'] = licenses_in_text
        return item

    def _create_licenses_field_if_empty(self, item):
        licenses = item.get('licenses', {})
        if not licenses:
            item['licenses'] = licenses
        return item


    def _find_licenses_in_text(self, text):
        resources = []
        if self._is_in_text(text, 'commercial'):
            resources.append(self.commercial_license)
        if self._is_in_text(text, 'proprietary'):
            resources.append(self.proprietary_license)
        if self._is_in_text(text, 'free'):
            resources.append(self.free_license)
        if self._is_in_text_abreviation_of_license(text, 'gpl'):
            resources.append(self.gpl_license)
        if self._is_in_text_abreviation_of_license(text, 'bsd'):
            resources.append(self.bsd_license)
        if self._is_in_text_abreviation_of_license(text, 'apache'):
            resources.append(self.apache_license)
        if self._is_in_text_abreviation_of_license(text, 'lgpl'):
            resources.append(self.gpl_license)
        if self._is_in_text_abreviation_of_license(text, 'agpl'):
            resources.append(self.agpl_license)
        return resources

    def _is_in_text(self, text, word):
        text = text.lower()
        word = word.lower()
        return word in text

    def _is_in_text_abreviation_of_license(self, text, license):
        text = text.lower()
        license = license.lower()
        text = text.split()
        for w in text:
            if w.startswith(license):
                return True
        return False
