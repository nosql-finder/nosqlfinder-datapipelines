# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy
from scrapy.item import BaseItem


class TechnologiesItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    pass


class GenericItem(dict, BaseItem):
    """
    A generic item that allow us to define an item with a dynamic schema
    """
    pass
