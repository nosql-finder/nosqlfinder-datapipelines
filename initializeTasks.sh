#!/bin/bash
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
MM=$(date +%m)
YY=$(date +%Y)
d=$YY-$MM
source `which virtualenvwrapper.sh`
workon nosql-finder
PYTHONPATH='.' luigi --module tasks.helpers.tasks.writer WriteNeo4j --date-interval $d
