import unittest
from py2neo import Graph, Node, Relationship
import json

from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks import settings


class TestLanguageWriter(unittest.TestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.writer = LanguageWriter(self.graph)
        self.base_node = Node(settings.PROGRAMMING_LANGUAGE_LABEL, dbpedia_uri='Java')

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_all_the_node_attributes(self):
        language = {'name': 'Lang1', 'summary': 'summary', 'dbpedia_uri': 'Lang1'}
        self.writer.write_language(language)
        query = """MATCH (p:{}) RETURN p""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(language['name'].lower(), result['name'])
        self.assertEqual(language['dbpedia_uri'], result['dbpedia_uri'])
        self.assertEqual(language['summary'], result['summary'])

    def test_path_INFLUENCED_BY_programming_language_exists(self):
        """
        Test that the path (ProgramingLanguage)-[INFLUENCED_BY]->(ProgramingLanguage)
        it's created
        """
        language = {'name': 'Lang1', 'influenced_by': ['Lang1', 'Lang2']}
        self.writer._write_influenced_languages_nodes(language, self.base_node)
        query = """MATCH (p:{0})-[r:INFLUENCED_BY]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_paradigms_exists(self):
        """
        Test that the path (ProgrammingLanguage)-[SUPPORTS]->(ProgrammingParadigm)
        it's created
        """
        language = {'name': 'Lang1', 'paradigms': ['Par1', 'Par2']}
        self.writer._write_paradigms_nodes(language, self.base_node)
        query = """MATCH (p:{0})-[r:SUPPORTS]->(p1:{1})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL,
                                  settings.PROGRAMMING_PARADIGM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_language_node_doesnt_write_twice(self):
        language = {'name': 'Lang1', 'dbpedia_uri': 'Lang1'}
        self.writer.write_language(language)
        self.writer.write_language(language)
        query = """MATCH (p:{0}) WHERE p.name='{1}'
        RETURN COUNT(p)""".format(settings.PROGRAMMING_LANGUAGE_LABEL,
                                  language['name'].lower())
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_writes_languages_nodes_from_resources(self):
        language = {
            'resources': [
                {
                    'name': 'Lang1',
                    'dbpedia_uri': 'Lang1'
                },
                {
                    'name': 'Lang2',
                    'dbpedia_uri': 'Lang2'
                }
            ]
        }
        self.writer._write_language_resources(language['resources'], self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_language_node_from_text(self):
        language = {
            'resources': [
            ],
            'text': 'OtherLanguage'
        }
        self.writer._write_languages_text(language['text'], self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        count_path = self.graph.run(query).evaluate()
        self.assertEqual(1, count_path)
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN p1.name""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        name = self.graph.run(query).evaluate()
        self.assertEqual(name, language['text'])

    def test_write_language_from_text_when_text_is_list_of_names(self):
        language = {
            'resources': [
            ],
            'text': 'lang1, lang2 and lang3'
        }
        self.writer._write_languages_text(language['text'], self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        count_path = self.graph.run(query).evaluate()
        self.assertEqual(3, count_path)

    def test_write_only_one_node_when_resources_and_text_have_same_name(self):
        language = {
            'resources': [
                {
                    'name': 'Lang1',
                    'dbpedia_uri': 'Lang1'
                }
            ],
            'text': 'lang1, lang2 and lang3'
        }
        self.writer.write_languages_attribute(language, self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(3, result)

    def test_writes_text_node_if_repites(self):
        language = {
            'resources': [

            ],
            'text': 'lang1, lang1'
        }
        self.writer.write_languages_attribute(language, self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_writes_repeated_resources(self):
        language = {
            'resources': [
                {
                    'name': 'Lang1',
                    'dbpedia_uri': 'Lang1'
                },
                {
                    'name': 'Lang1',
                    'dbpedia_uri': 'Lang1'
                }
            ]
        }
        self.writer.write_languages_attribute(language, self.base_node, 'TEST_RELATION')
        query = """MATCH (p:{0})-[r:TEST_RELATION]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
