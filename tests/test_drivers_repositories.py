from .graph import GraphTestCase
from py2neo import Graph, Node

from tasks.helpers.clients.api.neo4j.writers.repository import DriverWriter
from tasks import settings


class TestDriverWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode', uri='base_node')
        self.writer = DriverWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_popular_repository_node_with_all_simple_attributes(self):
        repository = {
            'popular_repositories': [
                {
                    'uri': 'http://github/test',
                    'name': 'Test',
                    'has_downloads': True,
                    'has_wiki': True,
                    'stars': 0,
                    'updated_at': '2016-06-10',
                    'created_at': '2016-06-10',
                    'homepage': 'http://index',
                    'summary': 'This is the summary',
                    'owner': 'owner'
                }
            ]
        }
        self.writer._bind_popular_repositories(repository, self.base_node)
        query = """MATCH (n)-[:HAS]->(repo:{})
        RETURN repo""".format(settings.REPOSITORY_LABEL)
        result = self.graph.run(query).evaluate()
        self.assert_shares_all_attributes(repository['popular_repositories'][0], result)

    def test_write_drivers_cluster_binding_with_language_node(self):
        repository = {
            'language': {
                'dbpedia_uri': 'Lang1',
                'name': 'Lang1'
            },
            'count': 34
        }
        cluster_node = self.writer._write_driver_cluster_node(repository)
        self.graph.merge(cluster_node)
        query = """MATCH (c:{})-[r:FOR]->(p:{})
        RETURN COUNT(r)""".format(settings.DRIVER_REPOS_CLUSTER_LABEL,
                                  settings.PROGRAMMING_LANGUAGE_LABEL)
        count_path = self.graph.run(query).evaluate()
        self.assertEqual(34, cluster_node['count'])
        self.assertEqual(1, count_path)

    def test_writes_drivers_cluster_node(self):
        repository = [
                {
                    'language': {
                        'dbpedia_uri': 'Lang1',
                        'name': 'Lang1'
                    },
                    'count': 34,
                    'popular_repositories': [
                        {
                            'uri': 'http://github/test',
                            'name': 'Test',
                            'has_downloads': True,
                            'has_wiki': True,
                            'stars': 0,
                            'updated_at': '2016-06-10',
                            'created_at': '2016-06-10',
                            'homepage': 'http://index',
                            'summary': 'This is the summary',
                            'owner': 'owner'
                        }
                    ]
                }
        ]
        self.writer.write_driver_repository(repository, self.base_node)
        query = """MATCH (n:BaseNode)-[r:HAS]->(dr:{})-[:FOR]->(p:{})
        RETURN COUNT(r)""".format(settings.DRIVER_REPOS_CLUSTER_LABEL,
                                  settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_writes_drivers_cluster_with_diferent_base_node(self):
        repository = [
                {
                    'language': {
                        'dbpedia_uri': 'Lang1',
                        'name': 'Lang1'
                    },
                    'count': 34,
                    'popular_repositories': [
                        {
                            'uri': 'http://github/test',
                            'name': 'Test',
                            'has_downloads': True,
                            'has_wiki': True,
                            'stars': 0,
                            'updated_at': '2016-06-10',
                            'created_at': '2016-06-10',
                            'homepage': 'http://index',
                            'summary': 'This is the summary',
                            'owner': 'owner'
                        }
                    ]
                }
        ]
        base_node2 = Node('BaseNode', uri='BaseNode2')
        self.graph.merge(base_node2)
        self.writer.write_driver_repository(repository, self.base_node)
        self.writer.write_driver_repository(repository, base_node2)
        query = """MATCH (n:BaseNode {{uri: 'BaseNode2'}})-[r:HAS]->(dr:{})-[:FOR]->(p:{})
        RETURN COUNT(r)""".format(settings.DRIVER_REPOS_CLUSTER_LABEL,
                                  settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
