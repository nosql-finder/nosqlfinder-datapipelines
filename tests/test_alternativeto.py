from .graph import GraphTestCase
from py2neo import Graph, Node

from tasks.helpers.clients.api.neo4j.writers.alternativeto import AlternativetoWriter
from tasks import settings


class TestAlternativetoWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode')
        self.writer = AlternativetoWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_node_with_all_simple_attributes(self):
        alternativeto = {
            'uri': 'http://alternativeto/test',
            'name': 'Test',
            'thumbnail': 'http://logo.png',
            'repository': 'http://github/test',
            'homepage': 'http://homepage',
            'likes': 4,
            'summary': 'This is the summary',
            'description': 'This is all the description'
        }
        node = self.writer._write_simple_attributes(alternativeto)
        self.assert_shares_all_attributes(alternativeto, node)

    def test_writes_alternatives_nodes(self):
        alternativeto = {
            'alternatives': [
                {
                    'uri': 'http://alternativeto/test',
                    'name': 'Test',
                    'thumbnail': 'http://logo.png',
                    'likes': 10
                },
                {
                    'uri': 'http://alternativeto/test2',
                    'name': 'Test2',
                    'thumbnail': 'http://logo.png',
                    'likes': 10
                }
            ]
        }
        self.writer._bind_alternatives(alternativeto, self.base_node)
        query = """MATCH (n:BaseNode)-[r:RELATED_TO]->(al:{})
        RETURN COUNT(r)""".format(settings.ALTERNATIVETO_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_writes_labels_nodes_when_is_movement(self):
        alternativeto = {
            'labels': [
                {
                    'name': 'Commercial',
                    'dbpedia_uri': 'Commercial_software'
                }
            ]
        }
        movement_node = Node(settings.MOVEMENT_LABEL, dbpedia_uri='Commercial_software', name='Commercial')
        movement_node2 = Node(settings.MOVEMENT_LABEL, dbpedia_uri='Free', name='Free')
        self.graph.merge(movement_node)
        self.graph.merge(movement_node2)
        self.writer._bind_labels_nodes(alternativeto, self.base_node)
        query = """MATCH (n:BaseNode)-[r:APPROVED_BY]-(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_writes_labels_nodes_when_is_operating_system(self):
        alternativeto = {
            'labels': [
                {
                    'name': 'Linux',
                    'dbpedia_uri': 'Linux'
                }
            ]
        }
        op_node = Node(settings.OPERATING_SYSTEM_LABEL, dbpedia_uri='Linux', name='Linux')
        op_node2 = Node(settings.OPERATING_SYSTEM_LABEL, dbpedia_uri='Windows', name='Windows')
        self.graph.merge(op_node)
        self.graph.merge(op_node2)
        self.writer._bind_labels_nodes(alternativeto, self.base_node)
        query = """MATCH (n:BaseNode)-[r:SUPPORTS]-(op:{})
        RETURN COUNT(r)""".format(settings.OPERATING_SYSTEM_LABEL)
        count_path = self.graph.run(query).evaluate()
        query = """MATCH (n:BaseNode)-[r:SUPPORTS]-(op:{})
        RETURN COUNT(op)""".format(settings.OPERATING_SYSTEM_LABEL)
        count_node = self.graph.run(query).evaluate()
        self.assertEqual(1, count_path)
        self.assertEqual(1, count_node)
