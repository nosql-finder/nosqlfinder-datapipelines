from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship
import json

from tasks.helpers.clients.api.neo4j.writers.wikipedia import WikipediaWriter
from tasks import settings


class TestWikipediaPost(GraphTestCase):

    def setUp(self):
        graph = Graph("bolt://neo4j:techsearch@localhost:7687")
        self.graph = graph
        self.writer = WikipediaWriter(self.graph)
        self.base_node = Node('BaseNode')
        with open('tests/DataWikipedia.json') as file:
            self.wikipedia_post = json.load(file)

    def tearDown(self):
        self.graph.delete_all()

    def test_write_node_with_simple_attributes(self):
        simple_attributes = self.wikipedia_post['simple_attributes']
        self.writer._write_simple_attrs(simple_attributes)
        query = """MATCH (w) WHERE w.uri='{}' RETURN w""".format(simple_attributes['uri'])
        result = self.graph.run(query).evaluate()
        self.assert_shares_all_attributes(simple_attributes, result)

    def test_path_WRITTEN_IN_in_developed_languages_nodes_exists(self):
        """
        Test that the relation (WikipediaPost)-[WRITTEN_IN]-(ProgrammingLanguage)
        it's created
        """
        self.writer._write_developed_languages_nodes(self.wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:WRITTEN_IN]->(p {{dbpedia_uri: '{}'}})
        RETURN count(r) as countPath""".format('Ruby_(programming_language)')
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_INFLUENCED_BY_in_developed_languages_nodes_exists(self):
        """
        Test that the relation
        (WikipediaPost)-[WRITTEN_IN]-(ProgrammingLanguage)-[INFLUENCED_BY]->(ProgrammingLanguage)
        it's created
        """
        self.writer._write_developed_languages_nodes(self.wikipedia_post, self.base_node)
        query = """MATCH (w)-[:WRITTEN_IN]->(p {{dbpedia_uri: '{}'}})-[r:INFLUENCED_BY]->(op:{})
        RETURN count(r)""".format('Ruby_(programming_language)', settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_paradigms_in_developed_languages_nodes_exists(self):
        """
        Test that the relation
        (WikipediaPost)-[WRITTEN_IN]-(ProgrammingLanguage)-[INFLUENCED_BY]->(ProgrammingLanguage)
        it's created
        """
        self.writer._write_developed_languages_nodes(self.wikipedia_post, self.base_node)
        query = """MATCH (w)-[:WRITTEN_IN]->(p {{dbpedia_uri: '{}'}})-[r:SUPPORTS]->(par:{})
        RETURN count(r)""".format('Ruby_(programming_language)', settings.PROGRAMMING_PARADIGM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_CREATED_BY_in_developer_nodes_exists(self):
        """
        Test that the relation
        (WikipediaPost)-[CREATED_BY]->(Developer)
        its created
        """
        wikipedia_post = {
            'developer': {
                'resources': [
                    {
                        'name': 'Company',
                        'dbpedia_uri': 'Company'
                    }
                ],
                'text': ''
            }
        }
        self.writer._write_developer_node(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:CREATED_BY]->(d {{dbpedia_uri: '{}'}})
        RETURN count(r)""".format('Company')
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_developer_node_with_attributes(self):
        """
        Test that the relation
        (WikipediaPost)-[CREATED_BY]->(Developer)
        its created
        """
        wikipedia_post = {
            'developer': {
                'resources': [
                    {
                        'name': 'Company',
                        'dbpedia_uri': 'Company'
                    }
                ],
                'text': ''
            }
        }
        self.writer._write_developer_node(wikipedia_post, self.base_node)
        query = """MATCH (w)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Company', result['dbpedia_uri'])
        self.assertEqual('company', result['name'])

    def test_write_developer_node_with_only_text_attribute_is_written(self):
        wikipedia_post = {
            'developer': {
                'resources': [
                ],
                'text': 'Company'
            }
        }
        self.writer._write_developer_node(wikipedia_post, self.base_node)
        query = """MATCH (w)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('company', result['name'])

    def test_if_before_was_developer_resource_text_with_same_name_is_not_written(self):
        wikipedia_post = {
            'developer': {
                'resources': [
                    {
                        'name': 'Company',
                        'dbpedia_uri': 'Company'
                    }
                ],
                'text': 'company'
            }
        }
        self.writer._write_developer_node(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:CREATED_BY]->(d:{})
        RETURN COUNT(r)""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_operating_systems_resources_nodes(self):
        wikipedia_post = {'operating_systems': {'resources': [{'name': 'Cross-platform', 'dbpedia_uri': 'Cross-platform'}]}}
        self.writer._write_operating_systems_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:SUPPORTS]->(op) WHERE op.dbpedia_uri='{}'
        RETURN COUNT(r) AS countPath""".format('Cross-platform')
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_operating_systems_text_node(self):
        wikipedia_post = {'operating_systems': {'resources': [], 'text': 'Linux'}}
        self.writer._write_operating_systems_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:SUPPORTS]->(op) WHERE op.name='{}'
        RETURN COUNT(r) AS countPath""".format('linux')
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_operating_systems_with_text_and_same_resources_doesnt_duplicate(self):
        wikipedia_post = {
            'operating_systems': {
                'resources': [
                    {
                        'name': 'Linux',
                        'dbpedia_uri': 'Linux'
                    }
                ],
                'text': 'linux'
            }
        }
        self.writer._write_operating_systems_nodes(wikipedia_post, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(op:{})
        RETURN COUNT(r)""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_operating_systems_with_text_and_same_resources_have_attributes(self):
        wikipedia_post = {
            'operating_systems': {
                'resources': [
                    {
                        'name': 'Linux',
                        'dbpedia_uri': 'Linux'
                    }
                ],
                'text': 'linux'
            }
        }
        self.writer._write_operating_systems_nodes(wikipedia_post, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(op:{})
        RETURN op""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Linux', result['dbpedia_uri'])
        self.assertEqual('linux', result['name'])

    def test_writes_licenses_resources_nodes(self):
        wikipedia_post = {
            'licenses': {
                'resources': [
                    {
                        'license': {
                            'name': 'Apache License',
                            'dbpedia_uri': 'Apache_License'
                        }
                     }
                ]
            }
        }
        self.writer._write_licenses_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:LICENSED_UNDER]->(lic) WHERE lic.dbpedia_uri='{}'
        RETURN COUNT(r)""".format('Apache_License')
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_APPROVED_BY_exists_with_licenses_resources_nodes(self):
        wikipedia_post = {
            'licenses': {
                'resources': [
                    {
                        'license': {
                            'name': 'Apache License',
                            'dbpedia_uri': 'Apache_License'
                        },
                        'movements': [
                            {
                                'dbpedia_uri': 'Open-source_license',
                                'name': 'Open Source'
                            },
                            {
                                'dbpedia_uri': 'Free',
                                'name': 'Free'
                            }
                        ]
                    }
                ]
            }
        }
        self.writer._write_licenses_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[:LICENSED_UNDER]->(lic {{dbpedia_uri: '{}'}})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format('Apache_License', settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_APPROVED_BY_exists_if_no_license(self):
        wikipedia_post = {
            'licenses': {
                'resources': [
                        {
                            'license': {},
                            'movements': [
                                {
                                    'name': 'Open Source',
                                    'dbpedia_uri': 'Open_Source'
                                }
                            ]
                        }
                ]
            }
        }
        self.writer._write_licenses_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:APPROVED_BY]-(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_licenses_text_node(self):
        wikipedia_post = {
            'licenses': {
                'resources': [],
                'text': 'Same as Perl'
            }
        }
        self.writer._write_licenses_nodes(wikipedia_post, self.base_node)
        query = """MATCH (w)-[r:LICENSED_UNDER]->(lic:{})
        RETURN lic""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('same as perl', result['name'])

    def test_path_SAME_AS_exists(self):
        """
        Test that the relation (NoSQL)-[:SAME_AS]-(WikipediaPost)
        it's created
        """
        nosql_node = Node('NoSQL', dbpedia_uri='MongoDB')
        self.graph.merge(nosql_node)
        wikipedia_post = {'wikipedia_post': self.wikipedia_post['simple_attributes']}
        self.writer.write_wikipedia_post(wikipedia_post, nosql_node)
        query = """MATCH (n:NoSQL)-[r:SAME_AS]-(w:WikipediaPost) WHERE w.uri='{}'
        RETURN COUNT(r) as countPath""".format(self.wikipedia_post['simple_attributes']['uri'])
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_doesnt_write_duplicated_wikipedias_nodes(self):
        simple_attributes = self.wikipedia_post['simple_attributes']
        self.writer._write_simple_attrs(simple_attributes)
        self.writer._write_simple_attrs(simple_attributes)
        query = """MATCH (w) WHERE w.uri='{}'
        RETURN COUNT(w)""".format(simple_attributes['uri'])
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
