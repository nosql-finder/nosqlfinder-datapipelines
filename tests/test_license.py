import unittest
from py2neo import Graph, Node, Relationship
import json

from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter
from tasks import settings


class TestLicenseWriter(unittest.TestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.writer = LicenseWriter(self.graph)
        self.base_node = Node('BaseNode')

    def tearDown(self):
        self.graph.delete_all()

    def test_path_APPROVED_BY_exists(self):
        """
        Test that the path (BaseNode)-[:APPROVED_BY]-(Movement)
        it's created
        """
        license = {
            'movements': [
                {
                    'name': 'open source',
                    'dbpedia_uri': 'Open_Source'
                }
            ]
        }
        self.writer._write_movements_nodes(license, self.base_node)
        query = """MATCH (n)-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r) as countPath, m as movement""".format(settings.MOVEMENT_LABEL)
        results = self.graph.data(query)[0]
        count_path = results['countPath']
        node = results['movement']
        self.assertEqual(1, count_path)
        self.assertEqual(node['name'], 'open source')
        self.assertEqual(node['dbpedia_uri'], 'Open_Source')

    def test_write_all_the_licenses_attributes(self):
        license = {
            'license': {
                'name': 'License1',
                'dbpedia_uri': 'License1'
            }
        }
        self.writer._write_license_node(license)
        query = """MATCH (n:{}) RETURN n""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(result['name'], license['license']['name'].lower())
        self.assertEqual(result['dbpedia_uri'], license['license']['dbpedia_uri'])

    def test_write_license_node_with_movements(self):
        """
        Test (License)-[APPROVED_BY]-(Movements) its created
        """
        license = {
            'license': {
                'name': 'License1',
                'dbpedia_uri': 'License1'
            },
            'movements': [
                {
                    'name': 'Open Source',
                    'dbpedia_uri': 'Open_Source'
                },
                {
                    'name': 'Free',
                    'dbpedia_uri': 'Free'
                }
            ]
        }
        self.writer._write_license_with_movements(license)
        query = """MATCH (lic:{})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL, settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_license_node_its_created_from_text(self):
        license = 'apache, gpl and gplv2'
        self.writer._write_licenses_text(license, self.base_node)
        query = """MATCH (n)-[r:LICENSED_UNDER]-(lic:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(3, result)

    def test_license_nodes_are_created_and_if_is_movement_these_are_created(self):
        movement_node = Node('Movement', name='open source')
        self.graph.merge(movement_node)
        movement_node2 = Node('Movement', name='free')
        self.graph.merge(movement_node2)
        license = 'open source, free, apache'
        self.writer._write_licenses_text(license, self.base_node)
        query = """MATCH (n)-[r:LICENSED_UNDER]-(lic:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL)
        count_path = self.graph.run(query).evaluate()
        query = """MATCH (n)-[r:APPROVED_BY]-(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        number_movements = self.graph.run(query).evaluate()
        self.assertEqual(1, count_path)
        self.assertEqual(2, number_movements)

    def test_write_license_nodes_from_resources(self):
        license = {
            'resources': [
                {
                    'license': {
                        'name': 'Lic1',
                        'dbpedia_uri': 'Lic1'
                    },
                    'movements': [
                        {
                            'name': 'Free',
                            'dbpedia_uri': 'Free'
                        },
                        {
                            'name': 'Open Source',
                            'dbpedia_uri': 'Open_Source'
                        }
                    ]
                }
            ],
            'text': ''
        }
        self.writer._write_license_resources(license['resources'], self.base_node)

        query = """MATCH (n)-[r:LICENSED_UNDER]->(lic:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

        query = """MATCH (n)-[:LICENSED_UNDER]->(lic:{})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL, settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_license_nodes_are_written_from_attributes(self):
        license = {
            'resources': [
                {
                    'license': {
                        'name': 'Lic1',
                        'dbpedia_uri': 'Lic1'
                    },
                    'movements': [
                        {
                            'name': 'Free',
                            'dbpedia_uri': 'Free'
                        },
                        {
                            'name': 'Open Source',
                            'dbpedia_uri': 'Open_Source'
                        }
                    ]
                },
                {
                    'license': {
                        'name': 'Lic2',
                        'dbpedia_uri': 'Lic2'
                    },
                    'movements': [
                        {
                            'name': 'Free',
                            'dbpedia_uri': 'Free'
                        }
                    ]
                }
            ],
            'text': 'free, open source'
        }
        self.writer.write_licenses_attribute(license, self.base_node)
        query = """MATCH (n:BaseNode)-[r:LICENSED_UNDER]->(lic:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)
        query = """MATCH (n:BaseNode)-[:LICENSED_UNDER]->(lic{{dbpedia_uri: 'Lic1'}})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)
        query = """MATCH (n:BaseNode)-[:LICENSED_UNDER]->(lic{{dbpedia_uri: 'Lic2'}})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
        query = """MATCH (n:BaseNode)-[r:APPROVED_BY]-(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(0, result)

    def test_write_license_node_text_ignoring_the_version(self):
        license = {
            'resources': [
                {
                    'license': {
                        'name': 'Apache License',
                        'dbpedia_uri': 'Apache License'
                    }
                }
            ],
            'text': 'Apache License 2.0'
        }
        self.writer.write_licenses_attribute(license, self.base_node)
        query = """MATCH (n)-[r:LICENSED_UNDER]->(lic:{})
        RETURN COUNT(lic)""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
