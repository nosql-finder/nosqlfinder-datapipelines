import unittest
from py2neo import Graph, Node, Relationship


class GraphTestCase(unittest.TestCase):

    def assert_shares_all_attributes(self, desired, actual):
        for key, value in desired.items():
            self.assertEqual(value, actual[key])
