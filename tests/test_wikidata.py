from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.wikidata import WikidataWriter
from tasks import settings


class TestWikidataWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode')
        self.writer = WikidataWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_node_with_all_simple_attributes(self):
        wikidata_post = {
            'repository': 'TestRepo',
            'stack_exchange_tag': 'Tag',
            'twitter_username': 'username',
            'facebook_username': 'username',
            'quora_topic': 'username',
            'thumbnail': 'http://logo.png',
            'latest_release': '2 years ago',
            'homepage': 'http://index',
        }
        node = self.writer._write_simple_attributes(wikidata_post)
        self.assert_shares_all_attributes(wikidata_post, node)

    def test_writes_developed_languages_nodes(self):
        wikidata_post = {
            'developed_languages': [
                {
                    'name': 'lang1',
                    'dbpedia_uri': 'Lang1',
                    'summary': 'This is the summary',
                    'paradigms': [
                        'Paradigm1'
                    ],
                    'influenced_by': [
                        'Lang2',
                        'Lang3'
                    ]
                }
            ]
        }
        self.writer._write_developed_languages(wikidata_post, self.base_node)
        query = """MATCH (n:BaseNode)-[:WRITTEN_IN]->(p:{})
        RETURN p""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Lang1', result['dbpedia_uri'])
        self.assertEqual('lang1', result['name'])
        self.assertEqual('This is the summary', result['summary'])

    def test_writes_developed_languages_paradigms_nodes(self):
        wikidata_post = {
            'developed_languages': [
                {
                    'name': 'lang1',
                    'dbpedia_uri': 'Lang1',
                    'summary': 'This is the summary',
                    'paradigms': [
                        'Paradigm1'
                    ],
                    'influenced_by': [
                        'Lang2',
                        'Lang3'
                    ]
                }
            ]
        }
        self.writer._write_developed_languages(wikidata_post, self.base_node)
        query = """MATCH (n:BaseNode)-[:WRITTEN_IN]->(p:{})-[r:SUPPORTS]->(par:{})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL,
                                  settings.PROGRAMMING_PARADIGM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_writes_developed_languages_paradigms_nodes(self):
        wikidata_post = {
            'developed_languages': [
                {
                    'name': 'lang1',
                    'dbpedia_uri': 'Lang1',
                    'summary': 'This is the summary',
                    'paradigms': [
                        'Paradigm1'
                    ],
                    'influenced_by': [
                        'Lang2',
                        'Lang3'
                    ]
                }
            ]
        }
        self.writer._write_developed_languages(wikidata_post, self.base_node)
        query = """MATCH (n:BaseNode)-[:WRITTEN_IN]->(p:{0})-[r:INFLUENCED_BY]->(p1:{0})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_licenses_nodes(self):
        wikidata_post = {
            'licenses': [
                {
                    'license': {
                        'dbpedia_uri': 'License1',
                        'name': 'License1'
                    },
                    'movements': [
                        {
                            'name': 'Open Source',
                            'dbpedia_uri': 'Open_source'
                        }
                    ]
                }
            ]
        }
        self.writer._write_licenses(wikidata_post, self.base_node)
        query = """MATCH (n:BaseNode)-[:LICENSED_UNDER]->(lic:{})
        RETURN lic""".format(settings.LICENSE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('License1', result['dbpedia_uri'])
        self.assertEqual('license1', result['name'])

    def test_write_licenses_nodes_movements(self):
        wikidata_post = {
            'licenses': [
                {
                    'license': {
                        'dbpedia_uri': 'License1',
                        'name': 'License1'
                    },
                    'movements': [
                        {
                            'name': 'Open Source',
                            'dbpedia_uri': 'Open_source'
                        }
                    ]
                }
            ]
        }
        self.writer._write_licenses(wikidata_post, self.base_node)
        query = """MATCH (n:BaseNode)-[:LICENSED_UNDER]->(lic:{})-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.LICENSE_LABEL, settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_developers_nodes(self):
        wikidata_post = {
            'developers': [
                {
                    'dbpedia_uri': 'Developer1',
                    'name': 'Developer1'
                }
            ]
        }
        self.writer._write_developer(wikidata_post, self.base_node)
        query = """MATCH (n)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Developer1', result['dbpedia_uri'])
        self.assertEqual('developer1', result['name'])

    def test_write_operating_systems_nodes(self):
        wikidata_post = {
            'operating_systems': [
                {
                    'dbpedia_uri': 'OperatingSystem',
                    'name': 'OperatingSystem'
                }
            ]
        }
        self.writer._write_operating_systems(wikidata_post, self.base_node)
        query = """MATCH (n)-[:SUPPORTS]->(op:{})
        RETURN op""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('OperatingSystem', result['dbpedia_uri'])
        self.assertEqual('operatingsystem', result['name'])
