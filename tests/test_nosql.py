from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.nosql import NoSQLWriter
from tasks import settings


class TestNoSQLWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode', uri='BaseNode')
        self.writer = NoSQLWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_node_with_all_simple_attributes(self):
        nosql = {
            'uri': 'nosql1',
            'name': 'nosql1',
            'summary': 'This is the summary',
            'REST_API': 'Rest API',
            'latest_release': 'version 1',
            'supports_sql': 'Yes',
            'homepage': 'http://index'
        }
        node = self.writer._write_simple_attributes(nosql)
        self.assert_shares_all_attributes(nosql, node)

    def test_writes_datamodel_node(self):
        nosql = {
            'datamodel': [
                {
                    'dbpedia_uri': 'Datamodel1',
                    'name': 'datamodel1'
                }
            ]
        }
        self.writer._write_datamodel(nosql, self.base_node)
        query = """MATCH (n)-[:TYPE]->(d:{})
        RETURN d""".format(settings.DATAMODEL_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Datamodel1', result['dbpedia_uri'])
        self.assertEqual('datamodel1', result['name'])

    def test_writes_developers_resources_nodes(self):
        nosql = {
            'developers': {
                'resources': [
                    {
                        'dbpedia_uri': 'Developer1',
                        'name': 'Developer1'
                    }
                ]
            }
        }
        self.writer._write_developers_resources(nosql['developers']['resources'], self.base_node)
        query = """MATCH (n)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Developer1', result['dbpedia_uri'])
        self.assertEqual('developer1', result['name'])

    def test_writes_developers_text_node(self):
        nosql = {
            'developers': {
                'resources': [

                ],
                'text': 'Developer1'
            }
        }
        self.writer._write_developers_text(nosql['developers']['text'], self.base_node)
        query = """MATCH (n)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Developer1', result['name'])

    def test_writes_developers_node(self):
        nosql = {
            'developers': {
                'resources': [
                    {
                        'dbpedia_uri': 'Developer1',
                        'name': 'Developer1'
                    }
                ],
                'text': 'Developer1'
            }
        }
        self.writer._write_developers(nosql, self.base_node)
        query = """MATCH (n)-[r:CREATED_BY]->(d:{})
        RETURN COUNT(r)""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_licenses_node(self):
        nosql = {
            'licenses': [
                {
                    'license': {
                        'dbpedia_uri': 'Lic1',
                        'name': 'lic1'
                    },
                    'movements': [
                        {
                            'dbpedia_uri': 'Movement1',
                            'name': 'movement1'
                        },
                        {
                            'dbpedia_uri': 'Movement2',
                            'name': 'movement2'
                        }
                    ]
                }
            ]
        }
        self.writer._write_licenses(nosql, self.base_node)
        query = """MATCH (n)-[:LICENSED_UNDER]->(lic:{})-[:APPROVED_BY]->(m:{})
        RETURN lic""".format(settings.LICENSE_LABEL, settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Lic1', result['dbpedia_uri'])
        self.assertEqual('lic1', result['name'])

    def test_write_licenses_node(self):
        nosql = {
            'licenses': [
                {
                    'license': {
                    },
                    'movements': [
                        {
                            'dbpedia_uri': 'Movement1',
                            'name': 'movement1'
                        },
                        {
                            'dbpedia_uri': 'Movement2',
                            'name': 'movement2'
                        }
                    ]
                }
            ]
        }
        self.writer._write_licenses(nosql, self.base_node)
        query = """MATCH (n)-[r:APPROVED_BY]->(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_drivers_node(self):
        nosql = {
            'drivers': [
                {
                    'dbpedia_uri': 'Lang1',
                    'name': 'lang1',
                    'paradigms': [
                        'Paradigm1'
                    ],
                    'summary': 'This is the summary',
                    'influenced_by': [
                        'Lang2',
                        'Lang3'
                    ]
                },
                {
                    'dbpedia_uri': 'Lang4',
                    'name': 'lang4'
                }
            ]
        }
        self.writer._write_drivers(nosql, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(p:{})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_keywords(self):
        nosql = {
            'keywords': [
                {
                    'dbpedia_uri': 'Keyword1',
                    'name': 'Keyword1'
                },
                {
                    'dbpedia_uri': 'Keyword2',
                    'name': 'Keyword2'
                }
            ]
        }
        self.writer._write_keywords(nosql, self.base_node)
        query = """MATCH (n)-[r:HAS]->(k:{})
        RETURN COUNT(r)""".format(settings.KEYWORD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_transaction_resources_nodes(self):
        nosql = {
            'transactions': {
                'resources': [
                    {
                        'dbpedia_uri': 'Transaction1',
                        'name': 'transaction1'
                    },
                    {
                        'dbpedia_uri': 'Transaction2',
                        'name': 'transaction2'
                    }
                ],
                'text': ''
            }
        }
        self.writer._write_transaction_resources(nosql['transactions']['resources'], self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.TRANSACTION_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_transaction_resources_nodes(self):
        nosql = {
            'transactions': {
                'resources': [
                    ],
                'text': 'Transaction1, Transaction2'
            }
        }
        self.writer._write_transaction_text(nosql['transactions']['text'], self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.TRANSACTION_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_transaction_nodes(self):
        nosql = {
            'transactions': {
                'resources': [
                    {
                        'dbpedia_uri': 'Transaction1',
                        'name': 'transaction1'
                    },
                    {
                        'dbpedia_uri': 'Transaction2',
                        'name': 'transaction2'
                    }
                ],
                'text': 'Transaction1, Transaction2'
            }
        }
        self.writer._write_transactions(nosql, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.TRANSACTION_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_writes_datatypes_nodes(self):
        nosql = {
            'datatypes': [
                'DataType1',
                'DataType2'
            ]
        }
        self.writer._write_datatypes(nosql, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(d:{})
        RETURN COUNT(r)""".format(settings.DATATYPE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_writes_developed_languages(self):
        nosql = {
            'developed_languages': [
                {
                    'dbpedia_uri': 'Lang1',
                    'name': 'lang1',
                    'paradigms': [
                        'Paradigm1'
                    ],
                    'summary': 'This is the summary',
                    'influenced_by': [
                        'Lang2',
                        'Lang3'
                    ]
                },
                {
                    'dbpedia_uri': 'Lang4',
                    'name': 'lang4'
                }
            ]
        }
        self.writer._write_developed_languages(nosql, self.base_node)
        query = """MATCH (n)-[r:CREATED_BY]->(p:{})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_apis_resources(self):
        nosql = {
            'API': {
                'resources': [
                    {
                        'dbpedia_uri': 'api1',
                        'name': 'api1'
                    }
                ],
                'text': 'api1'
            }
        }
        self.writer._write_apis_resources(nosql['API']['resources'], self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(a:{})
        RETURN COUNT(r)""".format(settings.API_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_apis_text(self):
        nosql = {
            'API': {
                'resources': [
                    {
                        'dbpedia_uri': 'api1',
                        'name': 'api1'
                    }
                ],
                'text': 'api1'
            }
        }
        self.writer._write_apis_text(nosql['API']['text'], self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(a:{})
        RETURN COUNT(r)""".format(settings.API_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_apis(self):
        nosql = {
            'API': {
                'resources': [
                    {
                        'dbpedia_uri': 'api1',
                        'name': 'api1'
                    }
                ],
                'text': 'api1'
            }
        }
        self.writer._write_apis(nosql, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(a:{})
        RETURN COUNT(r)""".format(settings.API_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
