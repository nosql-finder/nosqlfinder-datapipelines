import unittest
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from tasks.helpers.clients.scrapper.dbengine import DBEngineScrapper


class TestDBpediaPostSchema(unittest.TestCase):

    def test_dbengine_post_follow_the_json_scheme(self):
        uri = 'MongoDB'
        scrapper = DBEngineScrapper(uri)
        dbengine_post = scrapper.get_dbengine_post()
        schema = {
            "type": "object",
            "properties": {
                "uri": {"type": "string"},
                "name": {"type": "string"},
                "initial_release": {"type": "string"},
                "latest_release": {"type": "string"},
                "documentation": {"type": "string"},
                "is_cloud_based": {"type": "string"},
                "score": {"type": "number"},
                "homepage": {"type": "string"},
                "summary": {"type": "string"},
                "supports_xml": {"type": "string"},
                "supports_sql": {"type": "string"},
                "supports_concurrency": {"type": "string"},
                "supports_durability": {"type": "string"},
                "use_cases": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "data_schemes": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "access_methods": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "transaction_methods": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "consistency_methods": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "replication_methods": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "partitioning_methods": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "data_types": {
                    "type": "array",
                    "items": {"type": "string"}
                },
                "operating_systems": {
                    "type": "array"
                },
                "developer": {"type": "string"},
                "licenses": {
                    "type": "array"
                },
                "developed_languages": {
                    "type": "array"
                },
                "drivers": {
                    "type": "array"
                }
            }
        }
        try:
            validate(dbengine_post, schema)
        except ValidationError:
            self.fail("the data doesn't follow the scheme")
