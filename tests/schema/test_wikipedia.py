import unittest
from jsonschema import validate
from jsonschema.exceptions import ValidationError

from tasks.helpers.clients.scrapper.wikipedia import WikipediaScrapper


class TestWikipediaPostSchema(unittest.TestCase):

    def setUp(self):
        self.scrapper = WikipediaScrapper()

    def test_wikipedia_post_follow_the_json_scheme(self):
        dbpedia_uri = 'Apache_Cassandra'
        wikipedia_post = self.scrapper.get_wikipedia_post(dbpedia_uri)
        schema = {
            "type": "object",
            "properties": {
                "uri": {"type": "string"},
                "thumbnail": {"type": "string"},
                "repository": {"type": "string"},
                "homepage": {"type": "string"},
                "status": {"type": "string"},
                "latest_release": {"type": "string"},
                "initial_release": {"type": "string"},
                "operating_systems": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "dbpedia_uri": {"type": "string"}
                                }
                            }
                        },
                        "text": {"type": "string"}
                    }
                },
                "licenses": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "license": {
                                        "type": "object",
                                        "properties": {
                                            "name": {"type": "string"},
                                            "dbpedia_uri": {"type": "string"}
                                        }
                                    },
                                    "movements": {"type": "array"}
                                }
                            }
                        },
                        "text": {"type": "string"}
                    }
                },
                "developer": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "dbpedia_uri": {"type": "string"}
                                }
                            }
                        },
                        "text": {"type": "string"}
                    }
                },
                "developed_languages": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "items": {
                                "type": "object",
                                "properties": {
                                    "name": {"type": "string"},
                                    "dbpedia_uri":  {"type": "string"}
                                }
                            }
                        },
                        "text": {"type": "string"}
                    }
                }

            }
        }
        try:
            validate(wikipedia_post, schema)
        except ValidationError:
            self.fail("the data doesn't follow the scheme")
