from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.stackoverflow import StackoverflowWriter
from tasks import settings


class TestStackoverflowWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode', uri='BaseNode')
        self.writer = StackoverflowWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_path_SAME_AS_exists(self):
        stackoverflow_tag = {
            'stackoverflow_tag': {
                'uri': 'http://stackoverflow/test/info',
                'name': 'test',
                'summary': 'test summary',
                'created_at': '9 years',
                'views': 234,
                'number_questions': 23,
                'unanswered_questions': 12,
                'active': '9 months ago',
                'related_tags': [
                    {
                        'uri': 'http://stackoverlow/test-tag/info',
                        'name': 'test-tag',
                        'weight': 0.6
                    }
                ]
            }
        }
        self.writer.write_stackoverflow_tag(stackoverflow_tag, self.base_node)
        query = """MATCH (n)-[r:SAME_AS]-(st:{0})-[:RELATED_TO]-(st2:{0})
        RETURN COUNT(r)""".format(settings.STACKOVERFLOW_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_stackoverflow_node_with_simple_attributes(self):
        stackoverflow_tag = {
            'uri': 'http://stackoverflow/test/info',
            'name': 'test',
            'summary': 'test summary',
            'created_at': '9 years',
            'views': 234,
            'number_questions': 23,
            'unanswered_questions': 12,
            'active': '9 months ago'
        }
        node = self.writer._write_simple_attrs(stackoverflow_tag)
        self.assert_shares_all_attributes(stackoverflow_tag, node)

    def test_related_tags_when_normal_text(self):
        stackoverflow_tag = {
            'related_tags': [
                {
                    'uri': 'http://stackoverlow/test-tag/info',
                    'name': 'test-tag',
                    'weight': 0.6
                }
            ]
        }
        self.writer._write_related_tags(stackoverflow_tag, self.base_node)
        query = """MATCH (n)-[r:RELATED_TO {{weight: 0.6}}]-(st)
        RETURN st""".format(settings.STACKOVERFLOW_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('http://stackoverlow/test-tag/info', result['uri'])
        self.assertEqual('test-tag', result['name'])

    def test_related_tags_when_language_resource(self):
        stackoverflow_tag = {
            'related_tags': [
                {
                    'uri': 'http://stackoverflow/python',
                    'dbpedia_uri': 'Python',
                    'name': 'python',
                    'summary': 'This is the description',
                    'weight': 34,
                    'influenced_by': [
                        'Java'
                    ],
                    'paradigms': [
                        'object_oriented',
                        'Procedural_programming'
                    ]
                }
            ]
        }
        self.writer._write_related_tags(stackoverflow_tag, self.base_node)
        query = """MATCH (n)-[:RELATED_TO {{weight: 34}}]->(st:{})-[:SAME_AS]-(p:{})
        RETURN st""".format(settings.STACKOVERFLOW_LABEL, settings.PROGRAMMING_LANGUAGE_LABEL )
        result = self.graph.run(query).evaluate()
        self.assertEqual('http://stackoverflow/python', result['uri'])
