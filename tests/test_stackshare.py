from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.stackshare import StackshareWriter
from tasks import settings


class TestStackshareWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode')
        self.writer = StackshareWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_writes_node_with_all_simple_attributes(self):
        stackshare_post = {
            'uri': 'stack1',
            'name': 'stack1',
            'thumbnail': 'http://logo.png',
            'description': 'this is the description',
            'forks': 10,
            'votes': 10,
            'repository_stars': 10,
            'summary': 'A little summary',
            'favorites': 10,
            'homepage': 'http://index',
            'fans': 10
        }
        node = self.writer._write_simple_attributes(stackshare_post)
        self.assert_shares_all_attributes(stackshare_post, node)

    def test_writes_comments_nodes(self):
        stackshare_post = {
            'comments': [
                {
                    'text': 'This is the text',
                    'likes': 5
                },
                {
                    'text': 'This is the other text',
                    'likes': 8
                }
            ]
        }
        self.writer._bind_comments(stackshare_post, self.base_node)
        query = """MATCH (n)-[r:HAS]-(c:{})
        RETURN COUNT(r)""".format(settings.COMMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_write_clients_nodes(self):
        stackshare_post = {
            'clients': [
                {
                    'uri': 'client1',
                    'name': 'client1',
                    'thumbnail': 'http://logo.png'
                },
                {
                    'uri': 'client2',
                    'name': 'client2',
                    'thumbnail': 'http://logo.png'
                }
            ]
        }
        self.writer._bind_clients(stackshare_post, self.base_node)
        query = """MATCH (n)-[r:USED_BY]->(c:{})
        RETURN COUNT(r)""".format(settings.CLIENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_doesnt_write_repeated_clients_nodes(self):
        stackshare_post = {
            'clients': [
                {
                    'uri': 'client1',
                    'name': 'client1',
                    'thumbnail': 'http://logo.png'
                },
                {
                    'uri': 'client1',
                    'name': 'client1',
                    'thumbnail': 'http://logo.png'
                }
            ]
        }
        self.writer._bind_clients(stackshare_post, self.base_node)
        query = """MATCH (n)-[r:USED_BY]->(c:{})
        RETURN COUNT(r)""".format(settings.CLIENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_tools_nodes(self):
        stackshare_post = {
            'tools': [
                {
                    'uri': 'tool1',
                    'name': 'tool1',
                    'thumbnail': 'http://logo.png'
                },
                {
                    'uri': 'tool2',
                    'name': 'tool2',
                    'thumbnail': 'http://logo.png'
                }
            ]
        }
        self.writer._bind_tools(stackshare_post, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.TOOL_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_doesnt_write_repeated_tools_nodes(self):
        stackshare_post = {
            'tools': [
                {
                    'uri': 'tool1',
                    'name': 'tool1',
                    'thumbnail': 'http://logo.png'
                },
                {
                    'uri': 'tool1',
                    'name': 'tool1',
                    'thumbnail': 'http://logo.png'
                }
            ]
        }
        self.writer._bind_tools(stackshare_post, self.base_node)
        query = """MATCH (n)-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.TOOL_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_base_node(self):
        stackshare_post = {
            'uri': 'stack1',
            'name': 'stack1',
            'thumbnail': 'http://logo.png',
            'description': 'this is the description',
            'forks': 10,
            'votes': 10,
            'repository_stars': 10,
            'summary': 'A little summary',
            'favorites': 10,
            'homepage': 'http://index',
            'fans': 10,
            'comments': [
                {
                    'text': 'This is the text',
                    'likes': 5
                }
            ],
            'clients': [
                {
                    'uri': 'client1',
                    'name': 'client1',
                    'thumbnail': 'http://logo.png'
                }
            ],
            'tools': [
                {
                    'uri': 'tool1',
                    'name': 'tool1',
                    'thumbnail': 'http://logo.png'
                }
            ]
        }
        node = self.writer.create_stackshare_node(stackshare_post)
        self.graph.merge(node)
        query = """MATCH (st:{})-[r:HAS]->(c:{})
        RETURN COUNT(r)""".format(settings.STACKSHARE_LABEL, settings.COMMENT_LABEL)
        count_comments_path = self.graph.run(query).evaluate()

        query = """MATCH (st:{})-[r:USED_BY]->(c:{})
        RETURN COUNT(r)""".format(settings.STACKSHARE_LABEL, settings.CLIENT_LABEL)
        count_clients_path = self.graph.run(query).evaluate()

        query = """MATCH (st:{})-[r:SUPPORTS]->(t:{})
        RETURN COUNT(r)""".format(settings.STACKSHARE_LABEL, settings.TOOL_LABEL)
        count_tools_path = self.graph.run(query).evaluate()

        self.assertEqual(1, count_comments_path)
        self.assertEqual(1, count_clients_path)
        self.assertEqual(1, count_tools_path)
