from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.hackernews import HackernewsWriter
from tasks import settings


class TestNoSQLWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode', uri='BaseNode')
        self.writer = HackernewsWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_write_review_node(self):
        hackernews = {
            'comments': {
                'total_rating': 2.49,
                'comments': [
                ],
                'total': 0,
                'avg_sentiment': 0.188
            }
        }
        node = self.writer.write_review(hackernews['comments'])
        self.assertEqual(2.49, node['total_rating'])
        self.assertEqual(0, node['total'])
        self.assertEqual(0.188, node['avg_sentiment'])

    def test_bind_comments(self):
        hackernews = {
            'comments': {
                'total_rating': 2.49,
                'comments': [
                    {
                        'sentiment': 0.1,
                        'stars': 2.7,
                        'text': 'This is the text',
                        'url': 'http://comment'
                    }
                ],
                'total': 0,
                'avg_sentiment': 0.188
            }
        }
        self.writer._bind_comments(hackernews['comments'], self.base_node)
        query = """MATCH (n)-[r:HAS]->(c:{})
        RETURN COUNT(r)""".format(settings.COMMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
