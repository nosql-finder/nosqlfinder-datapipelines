from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.dbpedia import DBpediaWriter
from tasks import settings


class TestDBpediaWriter(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode')
        self.writer = DBpediaWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_write_node_with_simple_attributes(self):
        dbpedia_post = {
            'name': 'Test',
            'description': 'This is the description',
            'frequently_updated': 'yes',
            'latest_release': '9 of June 2015',
            'homepage': 'http://homepage',
            'status': 'Active',
            'thumbnail': 'http://logo.png',
            'uri': 'http://dbpedia/Test'
        }
        node = self.writer._write_simple_attrs(dbpedia_post)
        self.assert_shares_all_attributes(dbpedia_post, node)

    def test_write_developed_languages_nodes(self):
        dbpedia_post = {
            'developed_languages': [
                {
                    'dbpedia_uri': 'TestLang',
                    'name': 'TestLang',
                    'description': 'This is the description',
                    'influenced_by': [
                        'TestLang1',
                        'TestLang2'
                    ],
                    'paradigms': [
                        'Paradigm1',
                        'Paradigm2'
                    ]
                }
            ]
        }
        self.writer._write_developed_languages_nodes(dbpedia_post, self.base_node)
        query = """MATCH (n)-[r:WRITTEN_IN]->(p:{})
        RETURN p""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('TestLang', result['dbpedia_uri'])
        self.assertEqual('testlang', result['name'])

    def test_developer_node(self):
        dbpedia_post = {
            'developers': [
                {
                    'dbpedia_uri': 'Developer',
                    'name': 'Developer'
                }
            ]
        }
        self.writer._write_developers_nodes(dbpedia_post, self.base_node)
        query = """MATCH (n)-[:CREATED_BY]->(d:{})
        RETURN d""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Developer', result['dbpedia_uri'])
        self.assertEqual('developer', result['name'])

    def test_operating_systems_nodes(self):
        dbpedia_post = {
            'operating_systems': [
                {
                    'dbpedia_uri': 'Cross-platform',
                    'name': ''
                }
            ]
        }
        self.writer._write_operating_systems_nodes(dbpedia_post, self.base_node)
        query = """MATCH (n)-[:SUPPORTS]-(op:{})
        RETURN op""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('Cross-platform', result['dbpedia_uri'])
        self.assertEqual('', result['name'])

    def test_license_nodes(self):
        dbpedia_post = {
            'licenses': [
                {
                    'license': {
                        'dbpedia_uri': 'License',
                        'name': 'License'
                    },
                    'movements': [
                        {
                            'dbpedia_uri': 'Free',
                            'name': 'Free'
                        }
                    ]
                }
            ]
        }
        self.writer._write_license_node(dbpedia_post, self.base_node)
        query = """MATCH (n)-[:LICENSED_UNDER]->(lic:{})-[:APPROVED_BY]-(m:{})
        RETURN lic""".format(settings.LICENSE_LABEL, settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual('License', result['dbpedia_uri'])
        self.assertEqual('license', result['name'])
