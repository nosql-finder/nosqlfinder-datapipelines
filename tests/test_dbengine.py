from .graph import GraphTestCase
from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.dbengine import DBEngineWriter
from tasks import settings


class TestDBEnginePost(GraphTestCase):

    def setUp(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.base_node = Node('BaseNode', uri='BaseNode')
        self.writer = DBEngineWriter(self.graph)

    def tearDown(self):
        self.graph.delete_all()

    def test_path_SAME_AS_exists(self):
        dbengine_post = {
            'dbengine_post': {
            'name': 'Test',
            'initial_release': '4 of June',
            'latest_release': '5 of June 2017',
            'documentation': 'http://documentation.html',
            'is_cloud_based': 'yes',
            'score': 989,
            'homepage': 'http://test',
            'summary': 'this is a summary',
            'supports_xml': 'yes',
            'supports_sql': 'no',
            'supports_concurrency': 'yes',
            'supports_durability': 'no',
            'uri': 'http://dbengine/Test'
            }
        }
        self.writer.write_dbengine_post(dbengine_post, self.base_node)
        query = """MATCH (n)-[r:SAME_AS]-(db) WHERE db.uri='{}'
        RETURN COUNT(r)""".format(dbengine_post['dbengine_post']['uri'])
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_write_node_with_simple_attributes(self):
        dbengine_post = {
            'name': 'Test',
            'initial_release': '4 of June',
            'latest_release': '5 of June 2017',
            'documentation': 'http://documentation.html',
            'is_cloud_based': 'yes',
            'score': 989,
            'homepage': 'http://test',
            'summary': 'this is a summary',
            'supports_xml': 'yes',
            'supports_sql': 'no',
            'supports_concurrency': 'yes',
            'supports_durability': 'no',
            'uri': 'http://dbengine/Test'
        }
        self.writer._write_simple_attrs(dbengine_post)
        query = """MATCH (d) WHERE d.uri='{}'
        RETURN d""".format(dbengine_post['uri'])
        result = self.graph.run(query).evaluate()
        self.assert_shares_all_attributes(dbengine_post, result)

    def test_path_USED_FOR_exists(self):
        dbengine_post = {'use_cases': ['Use1', 'Use2', 'Use3']}
        self.writer._write_use_cases_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:USED_FOR]-(u:{})
        RETURN COUNT(r)""".format(settings.USE_CASE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(3, result)

    def test_use_cases_nodes_dont_repit(self):
        dbengine_post = {'use_cases': ['Use1', 'Use1']}
        self.writer._write_use_cases_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:USED_FOR]-(u:Use) RETURN COUNT(r)"""
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_SUPPORTS_for_schemas_nodes_exists(self):
        dbengine_post = {'data_schemas': ['schema1', 'schema2']}
        self.writer._write_schemas_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:SUPPORTS]-(s:{})
        RETURN COUNT(r)""".format(settings.SCHEMA_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_schemas_nodes_dont_repits(self):
        dbengine_post = {'data_schemas': ['schema1', 'schema1']}
        self.writer._write_schemas_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:SUPPORTS]-(s:{})
        RETURN COUNT(r)""".format(settings.SCHEMA_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_SUPPORTS_for_access_methods_nodes_exists(self):
        dbengine_post = {'access_methods': ['method1', 'method2']}
        self.writer._write_access_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:SUPPORTS]-(m:{})
        RETURN COUNT(r)""".format(settings.ACCESS_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test__access_methods_nodes_dont_repit(self):
        node = Node(settings.ACCESS_METHOD_LABEL, name='method1')
        self.graph.merge(node)
        dbengine_post = {'access_methods': ['method1', 'method1']}
        self.writer._write_access_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (m:{}) RETURN COUNT(m)""".format(settings.ACCESS_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_SUPPORTS_for_transactions_methods_nodes_exists(self):
        dbengine_post = {'transaction_methods': ['method1', 'method2']}
        self.writer._write_transactions_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:SUPPORTS]-(m:{})
        RETURN COUNT(r)""".format(settings.TRANSACTION_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_for_consistency_methods_nodes_exists(self):
        dbengine_post = {'consistency_methods': ['method1', 'method2']}
        self.writer._write_consistency_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:SUPPORTS]-(m:{})
        RETURN COUNT(r)""".format(settings.CONSISTENCY_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_for_replication_methods_nodes_exists(self):
        dbengine_post = {'replication_methods': ['method1', 'method2']}
        self.writer._write_replication_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:SUPPORTS]-(m:{})
        RETURN COUNT(r)""".format(settings.REPLICATION_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_for_partitioning_methods_nodes_exists(self):
        dbengine_post = {'partitioning_methods': ['method1', 'method2']}
        self.writer._write_partitioning_methods_nodes(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:SUPPORTS]-(m:{})
        RETURN COUNT(r)""".format(settings.PARTITIONING_METHOD_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_SUPPORTS_for_datatypes_nodes_exists(self):
        dbengine_post = {'data_types': ['type1', 'type2', 'type3']}
        self.writer._write_datatypes_nodes(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:SUPPORTS]-(d:{})
        RETURN COUNT(r)""".format(settings.DATATYPE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(3, result)

    def test_datatypes_nodes_dont_repit(self):
        node = Node(settings.DATATYPE_LABEL, name='type1')
        self.graph.merge(node)
        dbengine_post = {'data_types': ['type1']}
        self.writer._write_datatypes_nodes(dbengine_post, self.base_node)
        query = """MATCH (d:{})
        RETURN COUNT(d)""".format(settings.DATATYPE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_SUPPORTS_for_operating_systems_nodes_exists(self):
        dbengine_post = {'operating_systems': {
            'resources': [{'dbpedia_uri': 'Unix', 'name': 'Unix'},
                          {'dbpedia_uri': 'Linux', 'name': 'Linux'}],
            'text': 'Windows'}
        }
        self.writer._write_operating_systems_nodes(dbengine_post, self.base_node)
        query = """MATCH (d)-[r:SUPPORTS]-(op:{})
        RETURN COUNT(r)""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(3, result)

    def test_operating_systems_nodes_dont_repit_when_text_has_same_name_other_op(self):
        dbengine_post = {'operating_systems': {
            'resources': [{'dbpedia_uri': 'Unix', 'name': 'Unix'},
                          {'dbpedia_uri': 'Linux', 'name': 'Linux'}],
            'text': 'linux'}
        }
        self.writer._write_operating_systems_nodes(dbengine_post, self.base_node)
        query = """MATCH (op:{})
        RETURN COUNT(op)""".format(settings.OPERATING_SYSTEM_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(2, result)

    def test_path_CREATED_BY_for_developer_nodes_exists(self):
        dbengine_post = {'developer': 'Developer1'}
        self.writer._write_developer_node(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:CREATED_BY]-(d:{})
        RETURN COUNT(r)""".format(settings.DEVELOPER_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_LICENSED_UNDER_exists(self):
        dbengine_post = {
            'licenses': {
                'resources': [
                    {
                        'license': {
                        },
                        'movements': [
                            {
                                'name': 'Free',
                                'dbpedia_uri': 'Free'
                            }
                        ]
                    }
                ]
            }
        }
        self.writer._write_license_node(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:APPROVED_BY]-(m:{})
        RETURN COUNT(r)""".format(settings.MOVEMENT_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_path_WRITTEN_IN_exists(self):
        dbengine_post = {"developed_languages": {
            "resources": [
                {
                    "dbpedia_uri": "Ruby_(programming_language)",
                    "name": "Ruby",
                    "description": "Ruby language",
                    "influenced_by": [
                        "C_(programming_language)",
                        "Python_(programming_language)"
                    ],
                    "paradigms": [
                        "Functional_programming",
                        "Procedural_programming"
                    ]
                }
            ]
        }}
        self.writer._write_developed_languages_nodes(dbengine_post, self.base_node)
        query = """MATCH (w)-[r:WRITTEN_IN]->(p) RETURN count(p) as countPath"""
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)

    def test_drivers_nodes_exists(self):
        dbengine_post = {
            'drivers': {
                'resources': [
                    {
                        'dbpedia_uri': 'Java',
                        'description': 'test',
                        'influenced_by': ['other'],
                        'name': 'java',
                        'paradigms': ['event'],
                        'status': 'unknown'
                    }
                ]
            }
        }
        self.writer._write_drivers_nodes(dbengine_post, self.base_node)
        query = """MATCH (db)-[r:SUPPORTS {{status: 'unknown'}}]-(p:{})
        RETURN COUNT(r)""".format(settings.PROGRAMMING_LANGUAGE_LABEL)
        result = self.graph.run(query).evaluate()
        self.assertEqual(1, result)
