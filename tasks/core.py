import json
import luigi
import os.path

from tasks.helpers.tasks.merge_all_posts import AddGithubTechnologies
from tasks.helpers.clients.api.neo4j.writers.nosql import NoSQLWriter
from tasks.helpers.clients.api.trends import InterestOverTimeWriter
from tasks import settings


class WriteNeo4j(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddGithubTechnologies(self.date_interval)

    def output(self):
        path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                            'writed_technologies_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        writer = NoSQLWriter()

        for t in technologies:
            writer.write_nosql(t)

        with self.output().open('w') as file:
            json.dump(technologies, file)


class WriteInterest(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return WriteNeo4j(self.date_interval)

    def run(self):
        interest = InterestOverTimeWriter()
        interest.write_interest_nodes()

        
