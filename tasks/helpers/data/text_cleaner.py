import re
import nltk
from nltk.corpus import stopwords
from urllib.parse import unquote
import unicodedata


def remove_text_between_parenthesis(value):
    value = re.sub(r'\(.*?\)', '', value)
    value = value.strip()
    return value


def remove_text_between_square_brackets(value):
    value = re.sub(r'\[.*?\]', '', value)
    value = value.strip()
    return value


def remove_cites(value):
    value = re.sub(r'(#.*)', '', value)
    value = value.strip()
    return value


def replace_chars(value):
    value = re.sub('[^0-9a-zA-Z-\+%\(\)_]+', '_', value)
    return value


def list_to_str(values):
    return ''.join(values).strip()


def remove_text_between_backslashs(value):
    return re.sub(r'\/.*\/', '', value)


def remove_backslash(text):
    return text.replace('/', '').strip()


def remove_version(text):
    text = text.strip()
    if text[-1].isdigit():
        text = re.sub(r'([0-9]|[.])', '', text)
        text = text.strip()
    return text
    

def has_conjunctions_or_commas(value):
    has_commas = ',' in value
    has_conjunctions = len(get_conjunctions(value)) > 0
    return has_commas or has_conjunctions


def get_conjunctions(value):
    words = nltk.word_tokenize(value)
    tagged_words = nltk.pos_tag(words)
    conjunctions = set()
    for t in tagged_words:
        if t[1].startswith('CC'):
            conjunctions.add(t[0])
    return conjunctions


def split_list_with_all_conjunctions(value):
    value = tag_conjunctions(value)
    values = value.split('_CONJ')
    values = [v.strip() for v in values]
    values = filter_etc(values)
    return values


def tag_conjunctions(value):
    value = value.replace(',', '_CONJ')
    conjunctions = get_conjunctions(value)
    for c in conjunctions:
        regex = '[ ]{}[ ]'.format(c)
        value = re.sub(regex, '_CONJ', value)
    return value


def filter_etc(values):
    values = [v for v in values if not v.startswith('etc')]
    return values


def is_no_in_list(values):
    no_values = [v for v in values if 'no' in v.lower()]
    return len(no_values) > 0


def is_yes_in_list(values):
    yes_values = [v for v in values if 'yes' in v.lower()]
    return len(yes_values) > 0


def get_text_between_tags(html):
    return re.sub(r'<.*?>', '', html)


def stack_uri_to_name(uri):
    name = uri.replace('-', ' ').title()
    return name


def clean_apostrophe(text):
    return re.sub(r'&#x27;', "'", text)


def clean_html_tags(text):
    return re.sub(r'<.*?>', '', text)


def clean_hex_characters(text):
    return re.sub(r'&.*?;', '', text)


def has_numbers(text):
    return bool(re.search(r'\d', text))


def to_int(value):
    try:
        return int(value)
    except:
        return 0


def to_float(value):
    try:
        return float(value)
    except:
        return 0


def remove_dbpedia_uri_prefix(dbpedia_uri):
    return dbpedia_uri.replace('http://dbpedia.org/resource/', '')


class URICleaner:
    """Allow us to clean the name and dbpedia_uri in order
    to create a new URI
    """

    def clean_dbpedia_uri(self, dbpedia_uri):
        dbpedia_uri = self._replace_parenthesis(dbpedia_uri)
        dbpedia_uri = replace_chars(dbpedia_uri)
        dbpedia_uri = self._replace_percent(dbpedia_uri)
        dbpedia_uri = self._replace_plus(dbpedia_uri)
        return dbpedia_uri

    def clean_name(self, name):
        name = remove_text_between_square_brackets(name)
        name = replace_chars(name)
        name = name.title()
        name = self._format_name_with_spaces(name)
        return name

    def _replace_parenthesis(self, dbpedia_uri):
        dbpedia_uri = remove_text_between_parenthesis(dbpedia_uri)
        dbpedia_uri = replace_chars(dbpedia_uri)
        if dbpedia_uri.endswith('_'):
            dbpedia_uri = dbpedia_uri[:-1]
        return dbpedia_uri

    def _format_name_with_spaces(self, name):
        name = name.split()
        name = '_'.join(name)
        return name

    def _replace_plus(self, dbpedia_uri):
        if '+' in dbpedia_uri:
            dbpedia_uri = dbpedia_uri.split('+')
            dbpedia_uri = '_plus'.join(dbpedia_uri)
        return dbpedia_uri

    def _replace_percent(self, dbpedia_uri):
        if '%' in dbpedia_uri:
            dbpedia_uri = unquote(dbpedia_uri)
            dbpedia_uri = ''.join((c for c in unicodedata.normalize('NFD', dbpedia_uri)
                                   if unicodedata.category(c) != 'Mn'))
        return dbpedia_uri
