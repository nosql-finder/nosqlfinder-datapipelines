def find_object_in_list_by_name(name, objects):
    name = name.lower()
    for object in objects:
        object_name = object.get('name').lower()
        if name == object_name:
            return object
    return None


def find_object_in_list_by_uri(uri, objects):
    uri = uri.lower()
    for object in objects:
        object_uri = object.get('uri').lower()
        if uri == object_uri:
            return object
    return None


def find_object_in_list_by_dbpedia_uri(dbpedia_uri, objects):
    dbpedia_uri = dbpedia_uri.lower().strip()
    for object in objects:
        object_uri = object.get('dbpedia_uri').lower().strip()
        if dbpedia_uri == object_uri:
            return object
    return None
