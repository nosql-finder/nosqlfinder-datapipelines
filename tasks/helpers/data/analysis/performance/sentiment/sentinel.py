from nltk import word_tokenize
from textblob import TextBlob
from textblob.en.taggers import PatternTagger

from tasks import settings


class Sentinel:

    def __init__(self):
        self.negation_words = self.read_words_from_file(settings.NEGATION_WORDS_PATH)
        self.punctuation_characters = self.read_words_from_file(settings.PUNCTUATION_CHARS_PATH)
        self.strong_intensifiers = self.read_words_from_file(settings.STRONG_INTENSIFIERS_PATH)
        self.weak_intensifiers = self.read_words_from_file(settings.WEAK_INTENSIFIERS_PATH)

    def _preparate_words(self, words):
        tagged_words = []
        for word in words:
            t = (word, )
            tagged_words.append(t)
        return tagged_words

    def _find_negated_word(self, words):
        indexes = []
        for i, cloud_w in enumerate(words):
            word = cloud_w[0]
            if word in self.negation_words:
                indexes.append(i)
        return indexes

    def _mark_negated_scope(self, words, neg_idxs):
        for neg in neg_idxs:
            for i in range(neg + 1, len(words)):
                word = words[i][0]
                if word in self.punctuation_characters:
                    break
                words[i] = words[i] + ('NOT_',)
        return words

    def _find_strong_intensifier(self, words):
        indexes = []
        for i, cloud_w in enumerate(words):
            word = cloud_w[0]
            if word in self.strong_intensifiers:
                indexes.append(i)
        return indexes

    def _find_weak_intensifier(self, words):
        indexes = []
        for i, cloud_w in enumerate(words):
            word = cloud_w[0]
            if word in self.weak_intensifiers:
                indexes.append(i)
        return indexes

    def _mark_intensifier_scope(self, words, str_idxs, tag):
        for i in str_idxs:
            widx = i + 1
            try:
                words[widx] = words[widx] + (tag,)
            except IndexError:
                continue
        return words

    def _find_uncertainty(self, words):
        indexes = []
        for i, cloud_w in enumerate(words):
            word = cloud_w[0]
            if word.lower() == 'be':
                indexes.append(i)
        return indexes

    def _mark_uncertainty_scope(self, words, idxs):
        uncert_tags = ['JJR', 'JJ', 'RBR']
        for uncert in idxs:
            try:
                for i in range(uncert + 1, len(words)):
                    word = words[i][0]
                    tag = TextBlob(word, pos_tagger=PatternTagger()).tags[0][1]
                    if tag in uncert_tags:
                        words[i] += ('UNCERT_',)
                        break
            except:
                continue
        return words

    def _find_althoug(self, words):
        indexes = []
        for i, cloud_w in enumerate(words):
            word = cloud_w[0]
            if word.lower() == 'although':
                indexes.append(i)
        return indexes

    def _mark_althoug_scope(self, words, idxs):
        tag_limiters = ['PRP']
        for idx in idxs:
            for i in range(idx + 1, len(words)):
                word = words[i][0]
                if word in self.punctuation_characters:
                    break
                tag = TextBlob(word, pos_tagger=PatternTagger()).tags
                if tag:
                    tag = tag[0][1]
                else:
                    continue
                if tag in tag_limiters:
                    break
                words[i] += ('ALTH_',)
        return words

    def is_useless(self, word):
        useless = word in self.negation_words or word in self.strong_intensifiers
        return useless

    def _calculate_polarity(self, sentence):
        words = word_tokenize(sentence)
        words = self._preparate_words(words)
        neg_idxs = self._find_negated_word(words)
        str_idxs = self._find_strong_intensifier(words)
        weak_idxs = self._find_weak_intensifier(words)
        unc_idx = self._find_uncertainty(words)
        alth_idx = self._find_althoug(words)
        words = self._mark_negated_scope(words, neg_idxs)
        words = self._mark_intensifier_scope(words, str_idxs, 'STRONG_')
        words = self._mark_intensifier_scope(words, weak_idxs, 'WEAK_')
        words = self._mark_uncertainty_scope(words, unc_idx)
        words = self._mark_althoug_scope(words, alth_idx)
        polarities = []
        for cloud_w in words:
            word = cloud_w[0]
            tags = cloud_w[1:]
            polarity = TextBlob(word).sentiment.polarity
            for tag in tags:
                if 'NOT_' in tag:
                    polarity = polarity * -1
                elif 'STRONG_' in tag:
                    if polarity > 0: polarity += 0.3
                    else: polarity -= 0.3
                elif 'WEAK_' in tag:
                    if polarity > 0 : polarity -= 0.3
                    else: polarity += 0.3
                elif 'UNCERT_' in tag:
                    polarity = 0.0
                elif 'ALTH_' in tag:
                    polarity = 0.0
            if self.is_useless(word): polarity = 0.0
            if polarity > 1: polarity = 1
            if polarity < -1: polarity = -1
            polarities.append(polarity)
        return polarities

    def calculate_sentence_polarity(self, sentence):
        polarities = self._calculate_polarity(sentence)
        count = len([polarity for polarity in polarities if polarity != 0])
        total = sum([p for p in polarities])
        if count == 0:
            sentiment = 0.0
        else:
            sentiment = total/count
        return (sentiment, TextBlob(sentence).sentiment.subjectivity)

    def read_words_from_file(self, filename):
        with open(filename) as f:
            content = f.readlines()
        content = [w.strip() for w in content]
        return content
