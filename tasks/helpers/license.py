from tasks.helpers.clients.sparql.dbpedia_client import DBpediaClient
from tasks.helpers.clients.sparql.wikidata_client import WikidataClient
from tasks.helpers.clients.scrapper.license import LicenseScrapper
from tasks.helpers.inferences.license import MovementInference

from tasks.helpers.data import text_cleaner as tc
from tasks.helpers.data import filters
from tasks.helpers.files import csv

from tasks import settings


class MovementFinder:

    def __init__(self, triples):
        self.triples = triples
        self.memory = []
        self.dbpedia_client = DBpediaClient()
        self.wikidata_client = WikidataClient()
        self.scrapper = LicenseScrapper()
        self.inference = MovementInference()
        self.movements = self._read_movements()

    def _read_movements(self):
        movements = []
        with open(settings.LICENSE_MOVEMENTS, 'r') as file:
            movements = csv.read_csv(file)
        return movements

    def get_fields(self, license):
        dbpedia_uri = license.get('dbpedia_uri')
        new_license = {}
        if dbpedia_uri:
            new_license = self._find_in_movement(dbpedia_uri)
            if new_license is None:
                new_license = self._get_scrapped_movements(dbpedia_uri)
            if new_license is None:
                new_license = self._get_movements_from_rule(dbpedia_uri)
            if new_license is None:
                return {}
        return new_license

    def _find_in_movement(self, dbpedia_uri):
        movement = filters.find_object_in_list_by_dbpedia_uri(dbpedia_uri, self.movements)
        if movement:
            return {'license': '', 'movements': [movement]}
        return None

    def _get_movements_from_rule(self, dbpedia_uri):
        wikidata_uri = self.dbpedia_client.get_wikidata_uri(dbpedia_uri)
        fields = self.wikidata_client.get_fields(wikidata_uri, self.triples)
        if fields:
            movements = self.inference.get_license_movements(fields)
            name = fields.get('name', [])
            name = tc.list_to_str(name)
            license = {'name': name, 'dbpedia_uri': dbpedia_uri, 'wikidata_uri': wikidata_uri}
            license_data = {'license': license, 'movements': movements}
            return license_data
        return None

    def _get_scrapped_movements(self, dbpedia_uri):
        license_data = self.scrapper.find_license(dbpedia_uri)
        return license_data

    def _get_from_memory(self, desired_dbpedia_uri):
        return filters.find_object_in_list_by_dbpedia_uri(desired_dbpedia_uri, self.memory)
