import csv


def read_csv(csv_file):
    return list(csv.DictReader(csv_file))

