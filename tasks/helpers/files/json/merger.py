from jsonmerge import Merger


class NoSQLMerger:

    def __init__(self, json_object):
        self.schema = {
            "mergeStrategy": "objectMerge",
            "properties": {
                "datamodel": {
                    "mergeStrategy": "append"
                },
                "drivers": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "mergeStrategy": "append"
                        }
                    }
                },
                "keywords": {
                    "mergeStrategy": "append"
                },
                "licenses": {
                    "type": "object",
                    "properties": {
                        "resources": {
                            "type": "array",
                            "mergeStrategy": "append"
                        }
                    }
                }
            }
        }
        self.merger = Merger(self.schema)
        self.input = json_object
        self.output = []

    def merge_by_categories(self):
        all_objects = self._join_categories()
        unrepeated_objects = self._merge_duplicated(all_objects)
        return unrepeated_objects

    def merge(self):
        unrepeated_objects = self._merge_duplicated(self.input)
        return unrepeated_objects

    def _join_categories(self):
        all_objects = []
        for category in self.input:
            all_objects += category
        return all_objects

    def _merge_duplicated(self, jsons):
        for object in jsons:
            self._add_object_to_output(object)
        return self.output

    def _add_object_to_output(self, object):
        dbpedia_uri = object.get('dbpedia_uri')
        is_in_output = self._in_output(dbpedia_uri)
        if dbpedia_uri and is_in_output:
            self._add_merged_object(dbpedia_uri, object)
        else:
            self.output.append(object)

    def _in_output(self, desired_uri):
        for object in self.output:
            dbpedia_uri = object.get('dbpedia_uri')
            if dbpedia_uri == desired_uri:
                return True
        return False

    def _add_merged_object(self, dbpedia_uri, object):
        clean_object = self._remove_empty_fields(object)

        duplicated = self._get_value(dbpedia_uri)
        clean_duplicated = self._remove_empty_fields(duplicated)

        merged_object = self.merger.merge(clean_duplicated, clean_object)
        self._set_value(dbpedia_uri, merged_object)

    def _remove_empty_fields(self, json):
        clean_json = {}
        for k, v in json.items():
            if not self._is_empty(v):
                clean_json[k] = v
        return clean_json

    def _get_value(self, desired_uri):
        empty_object = {}
        for object in self.output:
            dbpedia_uri = object.get('dbpedia_uri')
            if dbpedia_uri == desired_uri:
                return object
        return empty_object

    def _set_value(self, desired_uri, new_object):
        for i, object in enumerate(self.output):
            dbpedia_uri = object.get('dbpedia_uri')
            if dbpedia_uri == desired_uri:
                self.output[i] = new_object

    def _is_empty(self, value):
        is_empty_str = type(value) is str and value == ''
        is_empty_list = type(value) is list and len(value) == 0
        return is_empty_list or is_empty_str
