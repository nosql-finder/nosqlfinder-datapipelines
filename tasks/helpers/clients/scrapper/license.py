from lxml import html
import requests

from tasks.helpers.clients.sparql.dbpedia_client import DBpediaClient
from tasks.helpers.data import text_cleaner as tc

OPEN_SOURCE = {'dbpedia_uri': 'Open-source_license', 'name': 'Open Source'}
FREE = {'dbpedia_uri': 'Free_software', 'name': 'Free'}


class LicenseScrapper:

    def __init__(self):
        base_url = 'https://en.wikipedia.org/wiki/Comparison_of_free_and_open-source_software_licenses'
        page = requests.get(base_url)
        self.tree = html.fromstring(page.content)
        self.licenses = self._scrape_licenses()
        self.dbpedia_client = DBpediaClient()

    def _scrape_licenses(self):
        rows = self.tree.xpath('//table[2]//tr[position()>1]')
        licenses = []
        for r in rows:
            dbpedia_uri = r.xpath('./td[1]//a/@href')
            open_source = r.xpath('./td[2]/@class')
            free = r.xpath('./td[4]/@class')
            name = r.xpath('./td[1]//a/text()')
            name = tc.list_to_str(name)
            movements = self._get_movements(open_source, free)
            dbpedia_uri = self._clean_dbpedia_uri(dbpedia_uri)
            license = {'name': name, 'dbpedia_uri': dbpedia_uri}
            license_data = {'license': license, 'movements': movements}
            licenses.append(license_data)
        return licenses

    def _get_movements(self, open_source, free):
        movements = []
        if 'table-yes' in open_source:
            movements.append(OPEN_SOURCE)
        if 'table-yes' in free:
            movements.append(FREE)
        return movements

    def find_license(self, desired_dbpedia_uri):
        desired_dbpedia_uri = self._get_correct_dbpedia_uri(desired_dbpedia_uri)
        for l in self.licenses:
            license = l['license']
            dbpedia_uri = license['dbpedia_uri']
            if desired_dbpedia_uri == dbpedia_uri:
                return l
        return None

    def _clean_dbpedia_uri(self, dbpedia_uri):
        dbpedia_uri = tc.list_to_str(dbpedia_uri)
        dbpedia_uri = dbpedia_uri.replace('/wiki/', '')
        dbpedia_uri = dbpedia_uri.strip()
        return dbpedia_uri

    def _get_correct_dbpedia_uri(self, dbpedia_uri):
        return self.dbpedia_client.get_redirected_uri(dbpedia_uri)
