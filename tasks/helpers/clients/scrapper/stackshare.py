from tasks.helpers.data import text_cleaner as tc

from lxml import html
import requests


class StackshareScrapper:

    def __init__(self, uri):
        self.uri = uri
        base_url = 'https://stackshare.io/{}'.format(uri)
        page = requests.get(base_url)
        self.tree = html.fromstring(page.content)

    def get_stackshare_post(self):
        stack_post = {}
        stack_post['uri'] = self.uri
        stack_post['name'] = self._get_name()
        stack_post['thumbnail'] = self._get_thumbnail()
        stack_post['repository'] = self._get_repository()
        stack_post['summary'] = self._get_summary()
        stack_post['summary'] = self._get_summary()
        stack_post['favorites'] = self._get_favorites()
        stack_post['homepage'] = self._get_homepage()
        stack_post['votes'] = self._get_votes()
        stack_post['fans'] = self._get_fans()
        stack_post['repository_stars'] = self._get_repository_stars()
        stack_post['forks'] = self._get_forks()
        stack_post['comments'] = self._get_comments()
        stack_post['clients'] = self._get_clients()
        stack_post['tools'] = self._get_tools()
        return stack_post

    def _get_name(self):
        xpath = "//a[@itemprop='name']/text()"
        name = self.tree.xpath(xpath)
        name = tc.list_to_str(name)
        return name

    def _get_thumbnail(self):
        xpath = "//img[@itemprop='image']/@src"
        thumbnail = self.tree.xpath(xpath)
        thumbnail = tc.list_to_str(thumbnail)
        return thumbnail

    def _get_repository(self):
        xpath = "//a[@class='source-code-link']/@href"
        repository = self.tree.xpath(xpath)
        repository = tc.list_to_str(repository)
        return repository

    def _get_summary(self):
        xpath = "//span[@itemprop='alternativeHeadline']/text()"
        abstract = self.tree.xpath(xpath)
        abstract = tc.list_to_str(abstract)
        return abstract

    def _get_summary(self):
        xpath = "//span[@itemprop='about']/text()"
        description = self.tree.xpath(xpath)
        description = tc.list_to_str(description)
        return description

    def _get_favorites(self):
        xpath = "//div[contains(@class, 'star-count')]/text()"
        favorites = self.tree.xpath(xpath)
        favorites = tc.list_to_str(favorites)
        favorites = self._convert_value_to_int(favorites)
        return favorites

    def _get_homepage(self):
        xpath = "//a[@id='visit-website']/@href"
        homepage = self.tree.xpath(xpath)
        homepage = tc.list_to_str(homepage)
        return homepage

    def _get_votes(self):
        xpath = "//div[@itemprop='aggregateRating']/../text()"
        votes = self.tree.xpath(xpath)
        votes = tc.list_to_str(votes)
        votes = self._convert_value_to_int(votes)
        return votes

    def _get_fans(self):
        xpath = "//div[contains(text(), 'Fans')]/../text()"
        fans = self.tree.xpath(xpath)
        fans = tc.list_to_str(fans)
        fans = self._convert_value_to_int(fans)
        return fans

    def _get_repository_stars(self):
        xpath = "//div[@data-hint='GitHub Stars']//div[@class='gh-metric']/text()"
        stars = self.tree.xpath(xpath)
        stars = tc.list_to_str(stars)
        stars = self._convert_value_to_int(stars)
        return stars

    def _get_forks(self):
        xpath = "//div[@data-hint='GitHub Forks']//div[@class='gh-metric']/text()"
        forks = self.tree.xpath(xpath)
        forks = tc.list_to_str(forks)
        forks = self._convert_value_to_int(forks)
        return forks

    def _get_comments(self):
        xpath = "//div[@class='reason_item']"
        comments = self.tree.xpath(xpath)

        clean_comments = []
        for c in comments:
            c = self._clean_comment(c)
            clean_comments.append(c)
        return clean_comments

    def _get_clients(self):
        xpath = "//div[@id='stacks']/../../following-sibling::div//div[contains(@class, 'stack-logo')]/a"
        clients = self.tree.xpath(xpath)

        clean_clients = []
        for c in clients:
            c = self._clean_client(c)
            clean_clients.append(c)
        return clean_clients

    def _get_tools(self):
        xpath = "//div[@class='similar-service-logos']//a[not(contains(@href, 'integrations'))]"
        tools = self.tree.xpath(xpath)

        clean_tools = []
        for t in tools:
            t = self._clean_tool(t)
            clean_tools.append(t)
        return clean_tools

    def _clean_comment(self, selector):
        text = selector.xpath(".//div[@id='reason-text']//text()")
        text = tc.list_to_str(text)

        likes = selector.xpath(".//span[@class='reason-count']//text()")
        likes = tc.list_to_str(likes)
        likes = self._convert_value_to_int(likes)
        comment = {'text': text, 'likes': likes}
        return comment

    def _clean_client(self, selector):
        name = selector.xpath('./@data-hint')
        name = tc.list_to_str(name)

        thumbnail = selector.xpath('./img/@src')
        thumbnail = tc.list_to_str(thumbnail)

        uri = selector.xpath('./@href')
        uri = tc.list_to_str(uri)
        uri = tc.remove_text_between_backslashs(uri)
        client = {'name': name, 'thumbnail': thumbnail, 'uri': uri}
        return client

    def _clean_tool(self, selector):
        name = selector.xpath('./@data-hint')
        name = tc.list_to_str(name)

        thumbnail = selector.xpath('./img/@src')
        thumbnail = tc.list_to_str(thumbnail)

        uri = selector.xpath('./@href')
        uri = tc.list_to_str(uri)
        uri = tc.remove_backslash(uri)
        tool = {'name': name, 'thumbnail': thumbnail, 'uri': uri}
        return tool

    def _convert_value_to_int(self, value):
        if 'k' in value.lower():
            value = self._convert_to_thousands(value)
        elif not value:
            value = 0
        else:
            value = int(value)
        return value

    def _convert_to_thousands(self, value):
        value = value.lower()
        value = value.replace('k', '')
        value = float(value)
        value *= 1000
        value = int(value)
        return value
