from tasks.helpers.data import text_cleaner as tc

from lxml import html
from lxml.etree import tostring

import requests


class DBEngineScrapper:

    def __init__(self, uri):
        self.uri = uri
        self.base_url = 'https://db-engines.com/en/system/{}'.format(uri)
        page = requests.get(self.base_url)
        self.tree = html.fromstring(page.content)

    def get_dbengine_post(self):
        dbengine_post = {}
        dbengine_post['uri'] = self.base_url
        dbengine_post['name'] = self._get_name()
        dbengine_post['summary'] = self._get_abstract()
        dbengine_post['score'] = self._get_score()
        dbengine_post['homepage'] = self._get_homepage()
        dbengine_post['documentation'] = self._get_documentation()
        dbengine_post['developer'] = self._get_developer()
        dbengine_post['initial_release'] = self._get_initial_release()
        dbengine_post['latest_release'] = self._get_current_release()
        dbengine_post['licenses'] = self._get_licenses()
        dbengine_post['is_cloud_based'] = self._get_cloud_based_support()
        dbengine_post['developed_languages'] = self._get_developed_languages()
        dbengine_post['operating_systems'] = self._get_operating_systems()
        dbengine_post['data_schemes'] = self._get_data_schemes()
        dbengine_post['datatypes'] = self._get_datatypes()
        dbengine_post['supports_xml'] = self._get_xml_supports()
        dbengine_post['supports_sql'] = self._get_sql_supports()
        dbengine_post['access_methods'] = self._get_access_methods()
        dbengine_post['drivers'] = self._get_drivers()
        dbengine_post['partitioning_methods'] = self._get_partitioning_methods()
        dbengine_post['replication_methods'] = self._get_replication_methods()
        dbengine_post['consistency_methods'] = self._get_consistency_methods()
        dbengine_post['transaction_methods'] = self._get_transaction_methods()
        dbengine_post['supports_concurrency'] = self._get_concurrency_support()
        dbengine_post['supports_durability'] = self._get_durability_support()
        dbengine_post['use_cases'] = self._get_use_cases()
        dbengine_post['apis'] = self._get_apis()
        return dbengine_post

    def _get_name(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Name')]/following-sibling::td//text()"
        name = self.tree.xpath(xpath)
        name = tc.list_to_str(name)
        return name

    def _get_abstract(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Description')]/following-sibling::td//text()"
        abstract = self.tree.xpath(xpath)
        abstract = tc.list_to_str(abstract)
        return abstract

    def _get_score(self):
        xpath = "//th[@class='dbi_mini' and contains(text(), 'Score')]/following-sibling::td/text()"
        score = self.tree.xpath(xpath)
        score = tc.list_to_str(score)
        score = tc.to_float(score)
        return score

    def _get_homepage(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Website')]/following-sibling::td/a/@href"
        homepage = self.tree.xpath(xpath)
        homepage = tc.list_to_str(homepage)
        return homepage

    def _get_documentation(self):
        xpath = "//td[@class='attribute' and contains(text(), 'documentation')]/following-sibling::td/a/@href"
        documentation = self.tree.xpath(xpath)
        documentation = tc.list_to_str(documentation)
        return documentation

    def _get_developer(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Developer')]/following-sibling::td/text()"
        developer = self.tree.xpath(xpath)
        developer = tc.list_to_str(developer)
        return developer

    def _get_initial_release(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Initial release')]/following-sibling::td/text()"
        release = self.tree.xpath(xpath)
        release = tc.list_to_str(release)
        return release

    def _get_current_release(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Current release')]/following-sibling::td/text()"
        release = self.tree.xpath(xpath)
        release = tc.list_to_str(release)
        return release

    def _get_licenses(self):
        xpath = "//td[@class='attribute' and contains(text(), 'License')]/following-sibling::td/text()"
        licenses = self.tree.xpath(xpath)
        return licenses

    def _get_cloud_based_support(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Cloud-based')]/following-sibling::td/text()"
        cloud_based = self.tree.xpath(xpath)
        cloud_based = tc.list_to_str(cloud_based)
        return cloud_based

    def _get_developed_languages(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Implementation language')]/following-sibling::td/text()"
        developed_langs = self.tree.xpath(xpath)
        return developed_langs

    def _get_operating_systems(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Server operating systems')]/following-sibling::td/text()"
        ops = self.tree.xpath(xpath)
        return ops

    def _get_data_schemes(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Data scheme')]/following-sibling::td/text()"
        schemas = self.tree.xpath(xpath)
        return schemas

    def _get_datatypes(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Typing')]/following-sibling::td//text()"
        datatypes = self.tree.xpath(xpath)
        datatypes = self._clean_datatypes(datatypes)
        return datatypes

    def _get_xml_supports(self):
        xpath = "//td[@class='attribute' and contains(text(), 'XML support')]/following-sibling::td/text()"
        xml_supports = self.tree.xpath(xpath)
        xml_supports = tc.list_to_str(xml_supports)
        return xml_supports

    def _get_sql_supports(self):
        xpath = "//td[@class='attribute' and contains(text(), 'SQL')]/following-sibling::td/text()"
        sql_supports = self.tree.xpath(xpath)
        sql_supports = tc.list_to_str(sql_supports)
        return sql_supports

    def _get_access_methods(self):
        xpath = "//td[@class='attribute' and contains(text(), 'access methods')]/following-sibling::td/text()"
        access_methods = self.tree.xpath(xpath)
        access_methods = self._filter_methods(access_methods)
        return access_methods

    def _get_drivers(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Supported programming languages')]/following-sibling::td"
        drivers = self.tree.xpath(xpath)
        drivers = self._clean_drivers(drivers)
        return drivers

    def _get_partitioning_methods(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Partitioning methods')]/following-sibling::td/text()"
        methods = self.tree.xpath(xpath)
        methods = self._filter_methods(methods)
        return methods

    def _get_replication_methods(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Replication methods')]/following-sibling::td/text()"
        methods = self.tree.xpath(xpath)
        methods = self._filter_methods(methods)
        return methods

    def _get_consistency_methods(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Consistency concepts')]/following-sibling::td/text()"
        methods = self.tree.xpath(xpath)
        methods = self._filter_methods(methods)
        return methods

    def _get_transaction_methods(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Transaction concepts')]/following-sibling::td/text()"
        methods = self.tree.xpath(xpath)
        methods = self._filter_methods(methods)
        return methods

    def _get_concurrency_support(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Concurrency')]/following-sibling::td/text()"
        supports_concurrency = self.tree.xpath(xpath)
        supports_concurrency = tc.list_to_str(supports_concurrency)
        return supports_concurrency

    def _get_durability_support(self):
        xpath = "//td[@class='attribute' and contains(text(), 'Durability')]/following-sibling::td/text()"
        supports_durability = self.tree.xpath(xpath)
        supports_durability = tc.list_to_str(supports_durability)
        return supports_durability

    def _get_use_cases(self):
        xpath = "//td[@class='attribute external_att']/*[@id='a34']/../following-sibling::td//ul/li//text()"
        use_cases = self.tree.xpath(xpath)
        use_cases = self._clean_uses(use_cases)
        return use_cases

    def _get_apis(self):
        xpath = "//td[@class='attribute' and contains(text(), 'API')]/following-sibling::td//text()"
        apis = self.tree.xpath(xpath)
        return apis

    def _clean_datatypes(self, datatypes):
        clean_datatypes = self._split_data_type_by_conjunctions(datatypes)
        clean_datatypes = self._add_basic_datatypes(clean_datatypes)
        clean_datatypes = self._remove_no_types(clean_datatypes)
        clean_datatypes = self._filter_datatypes(clean_datatypes)
        return clean_datatypes

    def _clean_drivers(self, drivers):
        clean_languages = []
        if drivers:
            drivers_status = self._get_drivers_with_status(drivers[0])
            clean_languages = self._clean_drivers_and_status(drivers_status)
        return clean_languages

    def _clean_uses(self, uses):
        clean_uses = []
        if uses:
            clean_uses = [tc.remove_text_between_parenthesis(u) for u in uses]
            clean_uses = [u.strip() for u in clean_uses if u.strip()]
        return clean_uses

    def _split_data_type_by_conjunctions(self, datatypes):
        clean_datatypes = []
        for d in datatypes:
            clean_datatypes += self._split_field(d)
        return clean_datatypes

    def _split_field(self, field):
        has_conjunctions = tc.has_conjunctions_or_commas(field)
        if has_conjunctions:
            field = tc.split_list_with_all_conjunctions(field)
        else:
            field = [field]
        return field

    def _get_drivers_with_status(self, html):
        html = tostring(html).decode('utf-8')
        langs_html = html.split('<br/>')
        languages = [tc.get_text_between_tags(l) for l in langs_html]
        return languages

    def _clean_drivers_and_status(self, languages):
        clean_langs = []
        for lang in languages:
            lang_status = lang.split(' ')
            if lang_status:
                lang_data = self._get_status_and_name(lang_status)
                clean_langs.append(lang_data)
        return clean_langs

    def _get_status_and_name(self, language_status):
        name = language_status[0]
        try:
            if language_status[1].startswith('('):
                name += language_status[1]
                status = language_status[2]
            else:
                status = language_status[1]
        except:
            status = 'unknown'
        data = {'name': name, 'status': status}
        return data

    def _add_basic_datatypes(self, datatypes):
        yes_types = tc.is_yes_in_list(datatypes)
        if yes_types and len(datatypes) == 1:
            datatypes = self._get_basic_datatypes()
        return datatypes

    def _remove_no_types(self, datatypes):
        no_types = tc.is_no_in_list(datatypes)
        if no_types and len(datatypes) == 1:
            return []
        return datatypes

    def _filter_datatypes(self, datatypes):
        datatypes = [d for d in datatypes if 'yes' not in d.lower()]
        datatypes = [d for d in datatypes if 'no' not in d.lower()]
        return datatypes

    def _get_basic_datatypes(self):
        return ['Numbers', 'Strings', 'Booleans', 'Null', 'Lists']

    def _is_supported(self, support):
        support = support.lower()
        if 'yes' in support:
            support = True
        elif 'no' in support:
            support = False
        return support

    def _filter_methods(self, methods):
        methods = [m for m in methods if 'no' not in m.lower()]
        return methods
