from tasks.helpers.data import text_cleaner as tc

from lxml import html
import requests


class AlternativetoScrapper:

    def __init__(self, uri):
        self.uri = uri
        base_url = 'https://alternativeto.net/software/{}/'.format(uri)
        page = requests.get(base_url)
        self.tree = html.fromstring(page.content)

    def get_alternativeto_post(self):
        alternativeto_post = {}
        alternativeto_post['uri'] = self.uri
        alternativeto_post['name'] = self._get_name()
        alternativeto_post['thumbnail'] = self._get_thumbnail()
        alternativeto_post['homepage'] = self._get_homepage()
        alternativeto_post['summary'] = self._get_summary()
        alternativeto_post['description'] = self._get_description()
        alternativeto_post['repository'] = self._get_repository()
        alternativeto_post['likes'] = self._get_likes()
        alternativeto_post['labels'] = self._get_labels()
        alternativeto_post['alternatives'] = self._get_alternatives()
        return alternativeto_post

    def _get_name(self):
        xpath = "//h1[@itemprop='name']//text()"
        name = self.tree.xpath(xpath)
        name = tc.list_to_str(name)
        return name

    def _get_thumbnail(self):
        xpath = "//meta[@itemprop='image']/@content"
        thumbnail = self.tree.xpath(xpath)
        thumbnail = tc.list_to_str(thumbnail)
        thumbnail = 'https:{}'.format(thumbnail)
        return thumbnail

    def _get_homepage(self):
        xpath = "//a[@itemprop='url']/@href"
        homepage = self.tree.xpath(xpath)
        homepage = tc.list_to_str(homepage)
        return homepage

    def _get_summary(self):
        xpath = "//section[contains(@class, 'blue app bluebox')]//p[@class='lead']//text()"
        summary = self.tree.xpath(xpath)
        summary = tc.list_to_str(summary)
        return summary

    def _get_description(self):
        xpath = "//span[@itemprop='description']//text()"
        description = self.tree.xpath(xpath)
        description = tc.list_to_str(description)
        return description

    def _get_repository(self):
        xpath = "//a[@class='stackup-gh-count']/@href"
        repository = self.tree.xpath(xpath)
        repository = tc.list_to_str(repository)
        return repository

    def _get_likes(self):
        xpath = "//section[contains(@class, 'blue app bluebox')]//div[@class='like-box']/span/text()"
        likes = self.tree.xpath(xpath)
        likes = tc.list_to_str(likes)
        likes = tc.to_int(likes)
        return likes

    def _get_labels(self):
        xpath = "//ul[@class='labels']//li//text()"
        labels = self.tree.xpath(xpath)

        clean_labels = []
        for label in labels:
            label = label.strip()
            if label:
                clean_labels.append(label.strip())
        return clean_labels

    def _get_alternatives(self):
        xpath = "//ul[@id='alternativeList']//li"
        alternatives = self.tree.xpath(xpath)
        clean_alternatives = []
        for al in alternatives:
            al = self._clean_alternative(al)
            if al.get('uri'):
                clean_alternatives.append(al)
        return clean_alternatives

    def _clean_alternative(self, selector):
        uri = selector.xpath(".//h3/a/@href")
        uri = tc.list_to_str(uri)
        uri = uri.replace('/software/', '')
        uri = tc.remove_backslash(uri)

        name = selector.xpath(".//h3/a/text()")
        name = tc.list_to_str(name)

        thumbnail = selector.xpath(".//img/@data-src")
        thumbnail = tc.list_to_str(thumbnail)
        thumbnail = 'https:{}'.format(thumbnail)

        likes = selector.xpath(".//div[@class='like-box']//span[@class='num']/text()")
        likes = tc.list_to_str(likes)
        likes = tc.to_int(likes)
        alternative = {}
        alternative['uri'] = uri
        alternative['name'] = name
        alternative['thumbnail'] = thumbnail
        alternative['likes'] = likes
        return alternative
