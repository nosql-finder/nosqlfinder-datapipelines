from tasks.helpers.data import text_cleaner as tc

from lxml import html
import requests
import re


class StackoverflowScrapper:

    def __init__(self, tag):
        self.tag = tag
        base_url = 'http://stackoverflow.com/tags/{}/info'.format(tag)
        base_url_unanswered = 'https://stackoverflow.com/questions/tagged/{}?sort=unanswered'.format(tag)
        base_url_answered = 'https://stackoverflow.com/questions/tagged/{}?sort=newest'.format(tag)
        page = requests.get(base_url)
        answered_page = requests.get(base_url_answered)
        unanswered_page = requests.get(base_url_unanswered)
        self.tree = html.fromstring(page.content)
        self.answered_tree = html.fromstring(answered_page.content)
        self.unanswered_tree = html.fromstring(unanswered_page.content)

    def get_tag(self):
        tag = {}
        tag['uri'] = self.tag
        tag['name'] = tc.stack_uri_to_name(tag['uri'])
        tag['summary'] = self._get_abstract()
        tag['links'] = self._get_links()
        tag['related_tags'] = self._get_tags()
        tag['created_at'] = self._get_created()
        tag['views'] = self._get_viewed()
        tag['active'] = self._get_active()
        tag['number_questions'] = self._get_number_questions()
        tag['unanswered_questions'] = self._get_unanswered_questions()
        return tag

    def _get_abstract(self):
        xpath = '//div[@class="post-text"]/p[1]//text()'
        abstract = self.tree.xpath(xpath)
        abstract = self._clean_array(abstract)
        return abstract

    def _get_links(self):
        xpath = '//div[@class="post-text"]//a[not(contains(@rel, "tag"))]/@href'
        return self.tree.xpath(xpath)

    def _get_tags(self):
        xpath = '//div[@class="post-text"]//a[contains(@rel, "tag")]/@href'
        tags = self.tree.xpath(xpath)
        tags = self._clean_tags(tags)
        return tags

    def _get_created(self):
        xpath = '//table[@id="qinfo"]//tr/td/p[@class="label-key" and contains(text(), "created")]/../../td/following-sibling::td//p//text()'
        created = self.tree.xpath(xpath)
        created = self._clean_created(created)
        created = created.strip()
        return created

    def _get_viewed(self):
        xpath = '//table[@id="qinfo"]//tr/td/p[@class="label-key" and contains(text(), "viewed")]/../../td/p/b/text()'
        views = self.tree.xpath(xpath)
        views = self._clean_number(views)
        return views

    def _get_active(self):
        xpath = '//table[@id="qinfo"]//tr/td/p[@class="label-key" and contains(text(), "active")]/../following-sibling::td//b/text()'
        active = self.tree.xpath(xpath)
        active = self._clean_array(active)
        return active

    def _get_number_questions(self):
        xpath = "//div[contains(text(),'questions')]/text()"
        questions = self.answered_tree.xpath(xpath)
        questions = self._clean_number(questions)
        return questions

    def _get_unanswered_questions(self):
        xpath = "//div[contains(text(),'questions')]/text()"
        questions = self.unanswered_tree.xpath(xpath)
        questions = self._clean_number(questions)
        return questions

    def _clean_created(self, created):
        created = ''.join(created)
        created = re.sub(r'[^\x00-\x7f]', r'', created)
        created = created.replace('\r', '')
        created = created.replace('\n', ' ')
        return created

    def _clean_array(self, array):
        value = ''.join(array)
        return value

    def _clean_tags(self, tags):
        tags = [t.replace('/questions/tagged/', '') for t in tags]
        return tags

    def _clean_number(self, number):
        matches = self._extract_numbers(number)
        number = 0
        if matches:
            number = tc.to_int(matches[0])
        return number

    def _extract_numbers(self, number):
        number = ''.join(number)
        number = number.replace(',', '')
        matches = re.findall(r'[0-9]+', number)
        return matches

    def _clean_name(self, uri):
        name = uri.replace('-', ' ').title()
        return name
