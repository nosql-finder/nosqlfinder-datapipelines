import time
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By


from tasks.helpers.data import text_cleaner as tc


class GithubDriverScrapper:

    def __init__(self, uri):
        self.uri = uri
        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(1120, 550)
        self._set_driver_url()

    def _set_driver_url(self):
        url = 'https://github.com/search?o=desc&p=1&q={}+drivers&s=stars&type=Repositories&utf8=%E2%9C%93'
        uri = self._format_uri(self.uri)
        url = url.format(uri)
        self.driver.get(url)

    def _format_uri(self, uri):
        uri = uri.replace('#', '')
        uri = uri.split()
        uri = '+'.join(uri)
        return uri

    def get_repositories_stats(self):
        while 'rate limit' in self.driver.title.lower():
            print("Rate Limit!!: {}".format(self.uri))
            time.sleep(300)
            self._set_driver_url()
        repositories = self._scrape_repositories_stats()
        self.driver.quit()
        return repositories

    def _scrape_repositories_stats(self):
        repositories = {}
        repositories['total'] = self._get_total_repositories()
        repositories['total_by_language'] = self._get_total_by_language()
        repositories['popular_repositories'] = self._get_popular_repositories()
        return repositories

    def _get_total_repositories(self):
        try:
            total = self.driver.find_element_by_xpath('(//span[contains(@class, "Counter ml-2")])[1]').text
            total = tc.to_int(total)
        except NoSuchElementException:
            total = 0
        return total

    def _get_total_by_language(self):
        languages_repos = []
        langs_rows = self.driver.find_elements_by_xpath('//ul[contains(@class, "filter-list")]//li')
        for row in langs_rows:
            row = self._clean_language_row(row)
            languages_repos.append(row)
        return languages_repos

    def _clean_language_row(self, row):
        lang_name = row.find_element_by_xpath('./a').text
        lang_name = self._clean_lang_name(lang_name)
        lang_total = row.find_element_by_xpath('.//span[@class="count"]').text
        lang_total = self._clean_lang_total(lang_total)
        lang_total = tc.to_int(lang_total)
        clean_repo = {'name': lang_name, 'total': lang_total}
        return clean_repo

    def _get_popular_repositories(self):
        repositories = self.driver.find_elements_by_xpath('//div[contains(@class, "repo-list-item")]')
        clean_repos = []
        for repo in repositories:
            repo = self._clean_repository(repo)
            clean_repos.append(repo)
        return clean_repos

    def _clean_lang_total(self, total):
        total = total.replace(',', '')
        return total

    def _clean_lang_name(self, name):
        names = name.split()
        names = [n for n in names if not tc.has_numbers(n)]
        name = tc.list_to_str(names)
        return name

    def _clean_repository(self, repo):
        clean_repo = {}
        name_and_owner = repo.find_element_by_xpath('.//h3/a').text
        name, owner = self._split_name_and_owner(name_and_owner)
        clean_repo['name'] = name
        clean_repo['owner'] = owner
        description = self._get_if_exists(repo, './/p[contains(@class, "d-inline-block")]', '')
        clean_repo['description'] = description

        last_update = self._get_if_exists_date(repo, './/p/relative-time', '')
        clean_repo['last_update'] = last_update

        license = self._get_if_exists(repo, './/p[contains(text(), "license")]', '')
        clean_repo['license'] = license

        language = self._get_if_exists(repo, './/div[contains(@class, "d-table-cell")]', '')
        clean_repo['language'] = language

        clean_repo['stars'] = self._get_stars(repo)
        return clean_repo

    def _get_stars(self, repo):
        stars = self._get_if_exists(repo, './/a[contains(@class, "muted-link")]', 0)
        if stars == 0:
            return stars
        else:
            stars = self._clean_stars(stars)
        return stars

    def _get_if_exists(self, element, xpath, default):
        try:
            value = element.find_element_by_xpath(xpath).text
        except:
            value = default
        return value

    def _get_if_exists_date(self, element, xpath, default):
        try:
            value = element.find_element_by_xpath(xpath).get_attribute('datetime')
        except:
            value = default
        return value

    def _split_name_and_owner(self, name_and_owner):
        name = ''.join(name_and_owner)
        owner, name = name.split('/')
        return name.strip(), owner.strip()

    def _clean_stars(self, stars):
        stars = ''.join(stars).strip()
        if 'k' in stars.lower():
            stars = stars.replace('k', '')
            stars = tc.to_float(stars)
            stars = int(stars * 1000)
        else:
            stars = tc.to_int(stars)
        return stars


class GithubTechnologiesScrapper:

    def __init__(self, uri):
        self.uri = uri
        self.driver = webdriver.PhantomJS()
        self.driver.set_window_size(1120, 550)
        self._set_url()

    def _set_url(self):
        url = 'https://github.com/search?q={}'
        uri = self._format_name(self.uri)
        url = url.format(uri)
        self.driver.get(url)

    def _format_name(self, uri):
        uri = uri.replace('#', '')
        uri = uri.split()
        uri = '+'.join(uri)
        return uri

    def get_technologies(self):
        while 'rate limit' in self.driver.title.lower():
            print("Rate Limit!!: {}".format(self.uri.encode('utf-8')))
            time.sleep(300)
            self._set_url()
        repositories = self._scrape_total_by_technologies()
        self.driver.quit()
        return repositories

    def _scrape_total_by_technologies(self):
        repositories = {}
        repositories['total'] = self._get_total_repositories()
        repositories['total_by_language'] = self._get_total_by_language()
        return repositories

    def _get_total_repositories(self):
        try:
            total = self.driver.find_element(By.XPATH, '//a[contains("Repositories", text())]//span[contains(@class, "Counter")]').text
            total = self._clean_number(total)
        except NoSuchElementException:
            print("Element has not been found")
            total = 0
        return total

    def _get_total_by_language(self):
        languages_repos = []
        langs_rows = self.driver.find_elements(By.XPATH, '//ul[contains(@class, "filter-list")]//li')
        for row in langs_rows:
            row = self._clean_language_row(row)
            languages_repos.append(row)
        return languages_repos

    def _clean_language_row(self, row):
        lang_name = row.find_element(By.XPATH, './a').text
        lang_name = self._clean_lang_name(lang_name)
        lang_total = row.find_element(By.XPATH, './/span[@class="count"]').text
        lang_total = self._clean_number(lang_total)
        clean_repo = {'name': lang_name, 'total': lang_total}
        return clean_repo

    def _clean_lang_name(self, name):
        names = name.split()
        names = [n for n in names if not tc.has_numbers(n)]
        name = tc.list_to_str(names)
        return name

    def _clean_number(self, value):
        value = value.strip()
        value = value.replace(',', '')
        if 'k' in value.lower():
            value = value.lower()
            value = value.replace('k', '')
            value = tc.to_float(value)
            value = int(value * 1000)
        else:
            value = tc.to_int(value)
        return value
