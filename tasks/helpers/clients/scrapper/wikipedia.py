from tasks.helpers.data import text_cleaner as tc

from lxml import html
import requests
import re


class WikipediaScrapper:

    def __init__(self):
        self.base_url = 'https://en.wikipedia.org/wiki/{}'
        self.fields_xpaths = {
            'developer': './/a[contains(@href, "wiki/Software_developer")]/../following-sibling::td',
            'initial_release': './/th[contains(text(), "Initial release")]/following-sibling::td',
            'latest_release': './/a[contains(@href, "wiki/Software_release_life_cycle")]/../following-sibling::td',
            'status': './/th[contains(text(), "Development status")]/following-sibling::td',
            'developed_languages': './/th[contains(text(), "Written in")]/following-sibling::td',
            'operating_systems': './/a[contains(@href, "wiki/Operating_system")]/../following-sibling::td',
            'licenses': './/a[contains(@href, "/wiki/Software_license")]/../following-sibling::td',
            'homepage': './/th[contains(text(), "Website")]/following-sibling::td//a/@href',
            'repository': './/a[contains(@href, "/wiki/Repository_(version_control)")]/../following-sibling::td//a/@href',
            'thumbnail': './/img/@src'
        }

    def get_wikipedia_post(self, uri):
        fields = {}
        table = self._get_table(uri)
        if table is not None:
            fields['developer'] = self._get_developer(table)
            fields['initial_release'] = self._get_initial_release(table)
            fields['latest_release'] = self._get_latest_release(table)
            fields['status'] = self._get_development_status(table)
            fields['developed_languages'] = self._get_developed_languages(table)
            fields['operating_systems'] = self._get_operating_systems(table)
            fields['licenses'] = self._get_licenses(table)
            fields['homepage'] = self._get_homepages(table)
            fields['repository'] = self._get_repository(table)
            fields['thumbnail'] = self._get_thumbnail(table)
            fields['uri'] = 'https://en.wikipedia.org/wiki/{}'.format(uri)
        return fields

    def _get_table(self, uri):
        page = requests.get(self.base_url.format(uri))
        tree = html.fromstring(page.content)
        table = tree.xpath('//table[@class="infobox vevent"]')
        if table:
            table = table[0]
        else:
            table = None
        return table

    def _get_developer(self, table):
        developer_element = self._get_element_from_table(table, 'developer')
        text = self._get_text(developer_element)
        resources = self._get_resources(developer_element)
        developer = {'text': text, 'resources': resources}
        return developer

    def _get_initial_release(self, table):
        release_element = self._get_element_from_table(table, 'initial_release')
        text = self._get_text(release_element)
        text = self._clean_release_dates(text)
        return text

    def _get_latest_release(self, table):
        release_element = self._get_element_from_table(table, 'latest_release')
        text = self._get_text(release_element)
        text = self._clean_release_dates(text)
        return text

    def _get_development_status(self, table):
        status = self._get_element_from_table(table, 'status')
        text = self._get_text(status)
        return text

    def _get_developed_languages(self, table):
        languages_element = self._get_element_from_table(table, 'developed_languages')
        resources = self._get_resources(languages_element)
        languages = {'resources': resources, 'text': ''}
        return languages

    def _get_operating_systems(self, table):
        operating_systems_element = self._get_element_from_table(table, 'operating_systems')
        resources = self._get_resources(operating_systems_element)
        operating_systems = {'resources': resources, 'text': ''}
        return operating_systems

    def _get_licenses(self, table):
        licenses_element = self._get_element_from_table(table, 'licenses')
        resources = self._get_resources(licenses_element)
        text = self._get_text(licenses_element)
        licenses = {'resources': resources, 'text': text}
        return licenses

    def _get_homepages(self, table):
        homepages = self._get_element_from_table(table, 'homepage')
        homepage = tc.list_to_str(homepages)
        if homepage.startswith('//'):
            homepage = 'https:{}'.format(homepage)
        return homepage

    def _get_thumbnail(self, table):
        thumbnail = self._get_element_from_table(table, 'thumbnail')
        thumbnail = tc.list_to_str(thumbnail)
        if thumbnail:
            thumbnail = 'https:{}'.format(thumbnail)
        return thumbnail

    def _get_repository(self, table):
        repository = self._get_element_from_table(table, 'repository')
        for repo in repository:
            if 'github.com' in repo:
                return repo
        return ''

    def _get_element_from_table(self, table, field):
        xpath = self.fields_xpaths[field]
        element = table.xpath(xpath)
        return element

    def _get_text(self, element):
        element = self._get_element_from_list(element)
        if element is not None:
            text = element.xpath('.//text()')
            text = tc.list_to_str(text)
        else:
            text = ''
        return text

    def _get_resources(self, element):
        element = self._get_element_from_list(element)
        clean_resources = []
        if element is not None:
            resources = element.xpath('.//a[not(contains(@href, "#cite_note"))]')
            for r in resources:
                name = r.xpath('./text()')
                name = tc.list_to_str(name)

                dbpedia_uri = r.xpath('./@href')
                dbpedia_uri = tc.list_to_str(dbpedia_uri)
                dbpedia_uri = dbpedia_uri.replace('/wiki/', '')

                if not dbpedia_uri.startswith('#'):
                    clean_resources.append({'name': name, 'dbpedia_uri': dbpedia_uri})
        return clean_resources

    def _get_element_from_list(self, element):
        if isinstance(element, list) and len(element) > 0:
            element = element[0]
        elif isinstance(element, list) and len(element) == 0:
            element = None
        return element

    def _clean_release_dates(self, text):
        text = re.sub(r'[^\x00-\x7f]', r' ', text)
        text = tc.remove_text_between_square_brackets(text)
        return text
