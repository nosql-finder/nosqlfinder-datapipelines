from tasks.helpers.data.filters import find_object_in_list_by_name


class DBpediaTraductor:

    def __init__(self, dictionary):
        self.dictionary = dictionary

    def translate(self, values):
        resources = []
        text = []
        for v in values:
            resource = self.translate_uri(v)
            if resource:
                resources.append(resource)
            else:
                text.append(v)
        translation = {'resources': resources, 'text': text}
        return translation

    def translate_uri(self, value):
        traduction = find_object_in_list_by_name(value, self.dictionary)
        return traduction
