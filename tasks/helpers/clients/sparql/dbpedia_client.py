from SPARQLWrapper import SPARQLWrapper
from .sparql_client import SparqlClient
from urllib.parse import unquote
from tasks.helpers.data import text_cleaner as tc


class DBpediaClient(SparqlClient):

    def __init__(self):
        self.sparql_wrapper = SPARQLWrapper('http://dbpedia.org/sparql')
        self.prefixes = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX owl: <http://www.w3.org/2002/07/owl#>
        PREFIX dbo: <http://dbpedia.org/ontology/>
        PREFIX dbr: <http://dbpedia.org/resource/>
        PREFIX foaf: <http://xmlns.com/foaf/0.1/>
        """
        self.prefix_resource = '<http://dbpedia.org/resource/{uri}>'
        self.redirect_triple = [{'name': 'redirected_uri', 'optional': True,
                                 'predicate': 'dbo:wikiPageRedirects'}]

    def get_wikidata_uri(self, uri):
        uri = self.get_redirected_uri(uri)
        query = self._get_wikidata_query(uri)
        results = self._execute_query(query)
        result = results.get('same', '')
        if result:
            result = result[0]
            result = result.replace('http://www.wikidata.org/entity/', '')
        return result

    def _get_wikidata_query(self, uri):
        query = """
        SELECT DISTINCT ?same
        WHERE {{
             <http://dbpedia.org/resource/{uri}> owl:sameAs ?same
             FILTER (regex(?same, "wikidata.org","i"))
        }}
        """.format(uri=uri)
        return query

    def get_redirected_uri(self, uri):
        uri = unquote(uri)
        redirected_uri = self.get_fields(uri, self.redirect_triple)
        if redirected_uri:
            uri = redirected_uri['redirected_uri']
            uri = tc.list_to_str(uri)
            uri = uri.replace('http://dbpedia.org/resource/', '')
        return uri
