from SPARQLWrapper import SPARQLWrapper, JSON
from urllib.parse import unquote
import urllib

from tasks.helpers.data import text_cleaner as tc


class SparqlClient:

    def set_proxy(self, ip):
        proxy_support = urllib.request.ProxyHandler({'http': ip})
        opener = urllib.request.build_opener(proxy_support)
        urllib.request.install_opener(opener)

    def get_fields(self, uri, fields):
        uri = unquote(uri)
        query = self._get_query(uri, fields)
        results = self._execute_query(query)
        return results

    def _execute_query(self, query):
        self.sparql_wrapper.setQuery(query)
        self.sparql_wrapper.setReturnFormat(JSON)
        results = self.sparql_wrapper.query().convert()
        clean_results = self._clean_results(results)
        return clean_results

    def _get_query(self, uri, fields):
        uri = tc.remove_cites(uri)
        select = self._construct_select(fields)
        where_triples = self._construct_where_triples(uri, fields)
        optional_triples = self._construct_optional_triples(uri, fields)
        filter_triples = self._construct_filter_language_triples(uri, fields)
        # we use four curly braces to scape the double braces in the format function
        query = """
        {prefixes}
        SELECT DISTINCT {select}
        WHERE {{{{
            {where_triples}
            {optional_triples}
            {filter_triples}
        }}}}
        """
        query = query.format(prefixes=self.prefixes, select=select,
                             where_triples=where_triples,
                             optional_triples=optional_triples,
                             filter_triples=filter_triples)
        return query

    def _clean_results(self, results):
        bindings = self._get_bindings(results)
        # create the dict for storing the results help us when some predicate
        # has more than one answer
        clean_results = {}
        if bindings:
            clean_results = self._clean_bindings(bindings)
        return clean_results

    def _clean_bindings(self, bindings):
        clean_results = self._create_result_dict(bindings)
        for b in bindings:
            for k, v in b.items():
                clean_results[k].add(v['value'])

        for k, v in clean_results.items():
            clean_results[k] = list(v)
        return clean_results

    def _get_bindings(self, results):
        bindings = results['results']['bindings']
        return bindings

    def _create_result_dict(self, bindings):
        keys = bindings[0].keys()
        results = {k: set() for k in keys}
        return results

    def _construct_select(self, fields):
        names = [f['name'] for f in fields]
        names_prefixed = ['?{}'.format(name) for name in names]
        select = ' '.join(names_prefixed)
        return select

    def _construct_where_triples(self, uri, fields):
        triples = [self._get_triple(uri, f['predicate'], f['name'])
                   for f in fields if not f['optional']]
        triples = '\n'.join(triples)
        return triples

    def _construct_optional_triples(self, uri, fields):
        triples = [self._get_optional_triple(uri, f['predicate'], f['name'])
                   for f in fields if f['optional']]
        triples = '\n'.join(triples)
        return triples

    def _construct_filter_language_triples(self, uri, fields):
        triples = [self._get_filter_triple(uri, f['name'], f['lang'])
                   for f in fields if f.get('lang')]
        triples = '\n'.join(triples)
        return triples

    def _get_optional_triple(self, uri, predicate, name):
        triple = self._get_triple(uri, predicate, name)
        triple = self._remove_end_continuation_point(triple)
        optional_triple = 'OPTIONAL {{ %s }}' % triple
        return optional_triple

    def _get_filter_triple(self, uri, name, lang):
        filter_triple = "FILTER (lang(?{name}) = '{lang}')"
        filter_triple = filter_triple.format(name=name, lang=lang)
        return filter_triple

    def _get_triple(self, uri, predicate, name):
        resource_prefix = self.prefix_resource.format(uri=uri)
        triple = '{resource_prefix} {predicate} ?{name} .'
        triple = triple.format(resource_prefix=resource_prefix,
                               uri=uri, predicate=predicate, name=name)
        return triple

    def _remove_end_continuation_point(self, triple):
        return triple[0:-2]
