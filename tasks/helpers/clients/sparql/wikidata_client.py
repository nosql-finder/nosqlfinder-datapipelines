from SPARQLWrapper import SPARQLWrapper
from .sparql_client import SparqlClient


class WikidataClient(SparqlClient):

    def __init__(self):
        self.sparql_wrapper = SPARQLWrapper('https://query.wikidata.org/bigdata/namespace/wdq/sparql')
        self.prefixes = """
        PREFIX wd: <http://www.wikidata.org/entity/>
        PREFIX wdt: <http://www.wikidata.org/prop/direct/>
        PREFIX wikibase: <http://wikiba.se/ontology#>
        PREFIX p: <http://www.wikidata.org/prop/>
        PREFIX ps: <http://www.wikidata.org/prop/statement/>
        PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX bd: <http://www.bigdata.com/rdf#>
        prefix schema: <http://schema.org/>
        """
        self.prefix_resource = 'wd:{uri}'

    def get_dbpedia_uri(self, uri):
        query = self._get_dbpedia_query(uri)
        results = self._execute_query(query)
        result = results.get('same', '')
        if result:
            result = result[0]
            result = result.replace('https://en.wikipedia.org/wiki/', '')
        return result

    def _get_dbpedia_query(self, uri):
        query = """
        SELECT DISTINCT ?same
        WHERE {{
            OPTIONAL {{
                ?same schema:about wd:{} .
                ?same schema:inLanguage "en" .
                FILTER (SUBSTR(str(?same), 1, 25) = "https://en.wikipedia.org/")
            }}
        }}
        """.format(uri)
        return query
