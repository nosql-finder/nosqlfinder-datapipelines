from .dbpedia_client import DBpediaClient
from tasks.helpers.data import filters
from tasks.helpers.data import text_cleaner as tc


class CachedDBpediaClient:

    def __init__(self, triples):
        self.triples = triples
        self.memory = []
        self.dbpedia_client = DBpediaClient()

    def get_fields(self, dbpedia_uri):
        rm = self._get_in_memory(dbpedia_uri)
        if rm is None:
            rm = self._call_client(dbpedia_uri)
            self.memory.append(rm)
        return rm

    def _get_in_memory(self, dbpedia_uri):
        return filters.find_object_in_list_by_dbpedia_uri(dbpedia_uri, self.memory)

    def _call_client(self, dbpedia_uri):
        fields = self.dbpedia_client.get_fields(dbpedia_uri, self.triples)
        fields = self._clean_fields(fields)
        fields['dbpedia_uri'] = dbpedia_uri
        return fields

    def _clean_fields(self, fields):
        name = fields.get('name', [])
        name = tc.list_to_str(name)
        fields['name'] = name
        return fields


class ProgrammingLanguageClient:

    def __init__(self, triples):
        self.triples = triples
        self.memory = []
        self.dbpedia_client = DBpediaClient()
        self.is_language_fields = [{"name": "type", "predicate": "rdf:type",
                                    "optional": False}]
        self.base_field = [{"name": "name", "predicate": "rdfs:label", "lang": "en", "optional": False}]

    def get_fields(self, resources):
        fields_with_resources = []
        for r in resources:
            if isinstance(r, dict):
                dbpedia_uri = r.get('dbpedia_uri')
                if dbpedia_uri:
                    rm = self._get_from_memory(dbpedia_uri)
                    if rm is None:
                        if self._is_language(dbpedia_uri):
                            rm = self._get_fields(dbpedia_uri)
                            rm['dbpedia_uri'] = dbpedia_uri
                            self.memory.append(rm)
                    result = r.copy()
                    if rm:
                        result.update(rm)
                    fields_with_resources.append(result)
                else:
                    fields_with_resources.append(r)
            else:
                fields_with_resources.append(r)
        return fields_with_resources

    def _is_language(self, dbpedia_uri):
        r_type = self.dbpedia_client.get_fields(dbpedia_uri, self.is_language_fields)
        r_type = r_type.get('type', [])
        return 'http://dbpedia.org/ontology/ProgrammingLanguage' in r_type

    def _get_fields(self, dbpedia_uri):
        fields = self.dbpedia_client.get_fields(dbpedia_uri, self.triples)
        if not fields:
            fields = self.dbpedia_client.get_fields(dbpedia_uri, self.base_field)
        fields = self._clean_fields(fields)
        return fields

    def _get_from_memory(self, desired_dbpedia_uri):
        return filters.find_object_in_list_by_dbpedia_uri(desired_dbpedia_uri, self.memory)

    def _clean_fields(self, fields):
        if fields:
            influenced_by = fields.get('influenced_by', [])
            influenced_by = self._replace_resources(influenced_by)

            paradigms = fields.get('paradigms', [])
            paradigms = self._replace_resources(paradigms)
            description = fields.get('summary', [])
            description = tc.list_to_str(description)

            fields['influenced_by'] = influenced_by
            fields['paradigms'] = paradigms
            fields['name'] = tc.list_to_str(fields['name'])
            fields['summary'] = description
        return fields

    def _replace_resources(self, fields):
        fields = [f.replace('http://dbpedia.org/resource/', '') for f in fields]
        return fields
