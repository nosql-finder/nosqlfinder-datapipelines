class ApiError(Exception):

    def __init__(self, status):
        self.status = status

    def __str__(self):
        return "Api Error: status={}".format(self.status)
