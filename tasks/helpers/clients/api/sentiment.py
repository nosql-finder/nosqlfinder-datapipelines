from tasks.helpers.data.analysis.performance.sentiment.sentinel import Sentinel


class SentimentClient:

    def __init__(self):
        self.sentinel = Sentinel()

    def get_sentiment_data(self, comments):
        comments = self._get_sentiment(comments)
        avg_sentiment = self._get_average_sentiment(comments)
        rating = self._get_total_rating(comments)
        data_comments = {}
        data_comments['comments'] = comments
        data_comments['total'] = len(comments)
        data_comments['avg_sentiment'] = avg_sentiment
        data_comments['total_rating'] = rating
        return data_comments

    def _get_sentiment(self, comments):
        sentiment_comments = []
        for c in comments:
            c = self._add_sentiment(c)
            sentiment_comments.append(c)
        return sentiment_comments

    def _get_average_sentiment(self, comments):
        total_sentiments = sum([c['sentiment'] for c in comments])
        avg_sentiment = 0
        if comments:
            avg_sentiment = total_sentiments / len(comments)
        return avg_sentiment

    def _add_sentiment(self, comment):
        text = comment['text']
        polarity = self._get_polarity(text)
        stars = self._get_bayesian_ranking(polarity)
        comment['sentiment'] = polarity
        comment['stars'] = stars
        return comment

    def _get_total_rating(self, comments):
        rating = 0
        if comments:
            total_starts = sum([c['stars'] for c in comments])
            C = 5
            m = 2
            rating = (C * m + total_starts) / (C + len(comments))
            rating = round(rating, 2)
        return rating

    def _get_polarity(self, text):
        sentiment = self.sentinel.calculate_sentence_polarity(text)
        sentiment = sentiment[0]
        return round(sentiment, 3)

    def _get_bayesian_ranking(self, polarity):
        # formula: NewValue = (((OldValue - OldMin) * (NewMax - NewMin)) / (OldMax - OldMin)) + NewMin
        old_min = -1
        old_max = 1
        old_range = self._get_range(old_min, old_max)

        new_min = 0
        new_max = 5
        new_range = self._get_range(new_min, new_max)
        star = (((polarity - old_min) * new_range) / old_range) + new_min
        star = round(star, 2)
        return star

    def _get_range(self, min, max):
        return max - min
