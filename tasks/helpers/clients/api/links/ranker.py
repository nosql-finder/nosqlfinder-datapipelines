from tasks.helpers.clients.api.links.mozscape import MozscapeError
from tasks.helpers.clients.api.links.mozscape import Mozscape

from retry import retry


class LinkRanker:

    def __init__(self):
        self.client = Mozscape(
                    'mozscape-156a394681',
                        'f8572c1748959f150d4c2b0b8a0a814d')

    def get_rank_of_links(self, links):
        metrics = self.client.urlMetrics(links, Mozscape.UMCols.domainAuthority)
        return metrics

    @retry(exceptions=(MozscapeError, Exception), tries=-1, delay=10, jitter=1)
    def get_rank_of_link(self, link):
        metrics = self.client.urlMetrics(link, Mozscape.UMCols.domainAuthority)
        domain_authority = metrics['pda']
        return {'url': link, 'score': domain_authority}
