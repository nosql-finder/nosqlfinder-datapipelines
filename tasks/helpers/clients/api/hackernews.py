from ratelimit import *
from urllib.parse import quote
import requests
import re

from tasks.helpers.clients.api.exceptions import ApiError
from tasks.helpers.data import text_cleaner as tc


class HackerNewsClient:

    def __init__(self):
        self.base_url = 'http://hn.algolia.com/api/v1/search?query={}&tags=comment&hitsPerPage=100'

    def get_comments(self, words):
        url = self._get_url(words)
        try:
            data = self._call_api(url)
            comments = data['hits']
            comments = self._process_comments(comments)
        except ApiError:
            comments = []
        return comments

    def _process_comments(self, comments):
        clean_comments = []
        for c in comments:
            if self._is_valid_comment(c):
                c = self._clean_comment(c)
                clean_comments.append(c)
        return clean_comments

    def _is_valid_comment(self, comment):
        text = comment['comment_text']
        is_job_offer = self._is_job_offer(text)
        is_short = self._is_short(text)
        is_valid = is_short and not is_job_offer
        return is_valid

    def _is_job_offer(self, text):
        matched = re.match(r'.*-.*', text)
        job_comment = 'job' in text
        team_comment = 'team' in text
        return matched or job_comment or team_comment

    def _is_short(self, comment):
        return len(comment) < 1000

    def _clean_comment(self, comment):
        text = comment['comment_text']
        text = self._clean_text(text)
        if comment.get('story_url'):
            url = comment['story_url']
        else:
            url = ''
        created_at = comment['created_at']
        if comment.get('points'):
            points = comment['points']
        else:
            points = 0
        clean_comment = {'url': url, 'text': text, 'created_at': created_at, 'points': points}
        return clean_comment

    def _clean_text(self, text):
        text = tc.clean_apostrophe(text)
        text = tc.clean_html_tags(text)
        text = tc.clean_hex_characters(text)
        return text

    def _get_url(self, words):
        words = [quote(w.lower()) for w in words]
        words = '+'.join(words)
        url = self.base_url.format(words)
        return url

    @rate_limited(1)
    def _call_api(self, url):
        response = requests.get(url)
        if response.status_code != 200:
            raise ApiError('Cannot call API: {}'.format(response.status_code))
        return response.json()
