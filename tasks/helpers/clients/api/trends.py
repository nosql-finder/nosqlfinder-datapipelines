from py2neo import Graph, Node, Relationship
from pytrends.request import TrendReq
from retry import retry
from tasks import settings


class InterestOverTimeWriter:

    def __init__(self):
        self.google_username = settings.GOOGLE_USERNAME
        self.google_password = settings.GOOGLE_PASSWORD
        self.graph = Graph(settings.PRODUCTION_DATABASE)

    def write_interest_nodes(self):
        data = self._get_interest_over_time()
        for key, values in data.items():
            nosql_node = self.graph.find_one('NoSQL', 'name', key)
            interest_node = Node('InterestOverTime', query=key)
            for year, count in values.items():
                year_node = Node('Year', year=year)
                year_node['num_queries'] = int(count)
                self.graph.merge(year_node)
                self.graph.merge(Relationship(interest_node, 'HAS', year_node))
            self.graph.merge(Relationship(nosql_node, 'HAS', interest_node))

    @retry(exceptions=(KeyError), tries=-1, delay=30)
    def _get_interest_over_time(self):
        names = self._get_products_names()
        pytrend = TrendReq()
        interest_dbs = {}
        for db in names:
            pytrend.build_payload(kw_list=db)
            interest_over_time_db = pytrend.interest_over_time()
            data = interest_over_time_db.to_dict()
            for key, value in data.items():
                if 'isPartial' not in key:
                    interest = self._get_interest(value, key)
                    interest_dbs[key] = interest
        return interest_dbs

    def _get_products_names(self):
        dbs = self.graph.data("MATCH (n:NoSQL) RETURN n.name")
        dbs_names = [db['n.name'] for db in dbs if db.get('n.name')]
        dbs_names_interval = [dbs_names[i:i+5] for i in range(0, len(dbs_names), 5)]
        return dbs_names_interval

    def _get_interest(self, data, key):
        years = self._initialize_years(data)
        for date, count in data.items():
            year = self._get_year(date)
            years[year].append(count)
        result = self._get_total_years(years)
        return result

    def _initialize_years(self, data):
        years = {}
        for date, count in data.items():
            year = self._get_year(date)
            years[year] = []
        return years

    def _get_total_years(self, years):
        for year, count in years.items():
            years[year] = sum(count)
        return years

    def _get_year(self, date):
        year = date.strftime('%Y-%m-%d')
        year = year[:4]
        return year


if __name__ == '__main__':
    interest = InterestOverTimeWriter()
    interest.write_interest_nodes()
