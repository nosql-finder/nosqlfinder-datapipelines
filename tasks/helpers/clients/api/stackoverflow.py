import requests
from urllib.parse import quote
from ratelimit import *

from tasks.helpers.clients.api.exceptions import ApiError
from tasks.helpers.data import text_cleaner as tc


class StackoverflowRelatedTagClient:

    def __init__(self):
        self._base_url = 'https://api.stackexchange.com/2.2/tags'
        self._key = 'hIPBODAfMfCoM50)yINAeA(('

    def add_related_tags(self, tag):
        uri = tag['uri']
        num_questions = tag['number_questions']
        url = self._endpoint(uri)
        response = self._call_api(url)
        items = response.get('items', [])
        clean_tags = self._clean_tags(items, num_questions)
        clean_tags = self._remove_this_tag_from_related_tags(clean_tags, uri)
        tag['related_tags'] = clean_tags
        return tag

    def _clean_tags(self, items, num_questions):
        related_tags = []
        for item in items:
            weight = self._get_weight(item, num_questions)
            tag = {}
            tag['uri'] = item['name']
            tag['name'] = tc.stack_uri_to_name(tag['uri'])
            tag['weight'] = weight
            related_tags.append(tag)
        return related_tags

    def _get_weight(self, item, num_questions):
        weight = 0
        if num_questions > 0:
            weight = round(item['count'] / num_questions, 2)
        return weight

    def _remove_this_tag_from_related_tags(self, tags, uri):
        clean_tags = [t for t in tags if t['uri'] != uri]
        return clean_tags

    def _endpoint(self, uri):
        url = '{base_url}/{uri}/related?pagesize=50&site=stackoverflow&key={key}'
        return url.format(base_url=self._base_url, uri=uri, key=self._key)

    @rate_limited(20)
    def _call_api(self, url):
        response = requests.get(url)

        if response.status_code != 200:
            raise ApiError('Cannot call API: {} with url: {}'.format(
                response.status_code, url))
        return response.json()
