from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks import settings


SIMPLE_ATTRIBUTES = ['total_rating', 'total', 'avg_sentiment']


class HackernewsWriter(Writer):

    def __init__(self, graph):
        self.graph = graph

    def write_review(self, comments_attr):
        node = self._write_simple_attributes(comments_attr)
        self._bind_comments(comments_attr, node)
        return node

    def _write_simple_attributes(self, comments_attr):
        node = Node(settings.REVIEW_LABEL)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = comments_attr[attr]
        self.graph.merge(node)
        return node

    def _bind_comments(self, comments_attr, base_node):
        comments = comments_attr.get('comments', [])
        for c in comments:
            node = Node(settings.COMMENT_LABEL, text=c['text'])
            node['sentiment'] = c['sentiment']
            node['stars'] = c['stars']
            node['points'] = c['points']
            node['created_at'] = c['created_at']
            node['url'] = self._get_url(c)
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _get_url(self, comment):
        try:
            url = comment.get('url', '')
        except NameError:
            url = ''
        return url
