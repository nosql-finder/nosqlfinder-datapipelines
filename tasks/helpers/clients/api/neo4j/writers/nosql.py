from py2neo import Graph, Node, Relationship
from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter, LanguageWriter
from tasks.helpers.clients.api.neo4j.writers.stackshare import StackshareWriter
from tasks.helpers.clients.api.neo4j.writers.wikipedia import WikipediaWriter
from tasks.helpers.clients.api.neo4j.writers.dbengine import DBEngineWriter
from tasks.helpers.clients.api.neo4j.writers.dbpedia import DBpediaWriter
from tasks.helpers.clients.api.neo4j.writers.wikidata import WikidataWriter
from tasks.helpers.clients.api.neo4j.writers.stackoverflow import StackoverflowWriter
from tasks.helpers.clients.api.neo4j.writers.repository import RepositoryWriter, DriverWriter
from tasks.helpers.clients.api.neo4j.writers.hackernews import HackernewsWriter
from tasks.helpers.clients.api.neo4j.writers.alternativeto import AlternativetoWriter
from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks.helpers.data import text_cleaner as tc
from tasks import settings


SIMPLE_ATTRIBUTES = [
 'uri', 'dbpedia_uri', 'name', 'summary', 'REST_API', 'latest_release',
 'supports_sql', 'homepage', 'thumbnail']


class NoSQLWriter(Writer):

    def __init__(self):
        self.graph = Graph(settings.TEST_DATABASE)
        self.license_writer = LicenseWriter(self.graph)
        self.language_writer = LanguageWriter(self.graph)
        self.stackshare_writer = StackshareWriter(self.graph)
        self.wikipedia_writer = WikipediaWriter(self.graph)
        self.dbengine_writer = DBEngineWriter(self.graph)
        self.dbpedia_writer = DBpediaWriter(self.graph)
        self.wikidata_writer = WikidataWriter(self.graph)
        self.stackoverflow_writer = StackoverflowWriter(self.graph)
        self.driver_writer = DriverWriter(self.graph)
        self.repository_writer = RepositoryWriter(self.graph)
        self.hackernews_writer = HackernewsWriter(self.graph)
        self.alternativeto_writer = AlternativetoWriter(self.graph)
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.NOSQL_LABEL, 'uri')
        self._create_uniqueness_constraint(settings.NOSQL_LABEL, 'name')

    def write_nosql(self, nosql):
        node = self._write_simple_attributes(nosql)
        self._write_datamodel(nosql, node)
        self._write_developers(nosql, node)
        self._write_licenses(nosql, node)
        self._write_drivers(nosql, node)
        self._write_developed_languages(nosql, node)
        self._write_keywords(nosql, node)
        self._write_transactions(nosql, node)
        self._write_apis(nosql, node)
        self._write_datatypes(nosql, node)
        self._write_stackshare_post(nosql, node)
        self._write_wikipedia_post(nosql, node)
        self._write_dbengine_post(nosql, node)
        self._write_dbpedia_post(nosql, node)
        self._write_wikidata_post(nosql, node)
        self._write_stackoverflow_tag(nosql, node)
        self._write_drivers_repositories(nosql, node)
        self._write_comments(nosql, node)
        self._write_technologies_stats(nosql, node)
        self._write_alternativeto_post(nosql, node)
        self._write_links_stats(nosql, node)
        self._write_repository(nosql, node)

    def _write_alternativeto_post(self, nosql, node):
        alternativeto_node = self.alternativeto_writer.write_alternativeto(nosql)
        if alternativeto_node:
            self.graph.merge(Relationship(node, 'SAME_AS', alternativeto_node))

    def _write_stackshare_post(self, nosql, node):
        stackshare_post = nosql.get('stackshare_post')
        if stackshare_post:
            stackshare_node = self.stackshare_writer.create_stackshare_node(stackshare_post)
            self.graph.merge(Relationship(node, 'SAME_AS', stackshare_node))

    def _write_wikipedia_post(self, nosql, node):
        wikipedia_post = nosql.get('wikipedia_post')
        if wikipedia_post:
            wikipedia_node = self.wikipedia_writer.write_wikipedia_post(wikipedia_post)
            self.graph.merge(Relationship(node, 'SAME_AS', wikipedia_node))

    def _write_dbengine_post(self, nosql, node):
        dbengine_post = nosql.get('dbengine_post')
        if dbengine_post:
            dbengine_node = self.dbengine_writer.write_dbengine_post(dbengine_post)
            self.graph.merge(Relationship(node, 'SAME_AS', dbengine_node))

    def _write_dbpedia_post(self, nosql, node):
        dbpedia_post = nosql.get('dbpedia_post')
        if dbpedia_post:
            dbpedia_node = self.dbpedia_writer.write_dbpedia_post(dbpedia_post, nosql.get('dbpedia_uri', ''))
            self.graph.merge(Relationship(node, 'SAME_AS', dbpedia_node))

    def _write_wikidata_post(self, nosql, node):
        wikidata_post = nosql.get('wikidata_post')
        if wikidata_post:
            wikidata_node = self.wikidata_writer.write_wikidata_post(wikidata_post)
            self.graph.merge(Relationship(node, 'SAME_AS', wikidata_node))

    def _write_stackoverflow_tag(self, nosql, node):
        tag = nosql.get('stackoverflow_tag')
        if tag:
            tag_node = self.stackoverflow_writer.write_stackoverflow_tag(tag)
            self.graph.merge(Relationship(node, 'SAME_AS', tag_node))

    def _write_repository(self, nosql, node):
        repository = nosql.get('repository')
        if repository:
            repository_node = self.repository_writer.write_repository(repository)
            self.graph.merge(Relationship(node, 'HAS', repository_node))

    def _write_drivers_repositories(self, nosql, node):
        drivers = nosql.get('drivers_repositories')
        if drivers:
            self.driver_writer.write_driver_repository(drivers, node)

    def _write_technologies_stats(self, nosql, node):
        drivers = nosql.get('technologies_stats', {})
        cluster_node = Node(settings.CLUSTER_TECHNOLOGIES_LABEL, total=drivers['total'], uri=nosql['uri'])
        languages = drivers.get('total_by_language', [])
        for lang in languages:
            lang_node = self.find_programming_language(lang['name'])
            if not lang_node:
                lang_node = Node('Technology', name=lang['name'])
            self.graph.merge(Relationship(cluster_node, 'HAS', lang_node, total=lang['total']))
        self.graph.merge(Relationship(node, 'HAS', cluster_node))

    def _write_simple_attributes(self, nosql):
        node = Node(settings.NOSQL_LABEL, uri=nosql['uri'])
        self.graph.merge(node)
        for attr in SIMPLE_ATTRIBUTES:
            if nosql.get(attr, ''):
                node[attr] = nosql.get(attr, "")
        self.graph.push(node)
        return node

    def _write_datamodel(self, nosql, base_node):
        datamodels = nosql.get('datamodel', [])
        for d in datamodels:
            node = self._write_resource_node(settings.DATAMODEL_LABEL, d)
            self.graph.merge(Relationship(base_node, 'TYPE', node))

    def _write_developers(self, nosql, base_node):
        developers = nosql.get('developers', {})
        resources = developers.get('resources', [])
        text = developers.get('text', '')
        if resources:
            self._write_developers_resources(resources, base_node)
        elif text:
            self._write_developers_text(text, base_node)

    def _write_licenses(self, nosql, base_node):
        licenses = nosql.get('licenses')
        if isinstance(licenses, list):
            self.license_writer._write_license_resources(licenses, base_node)
        elif isinstance(licenses, dict):
            self.license_writer.write_licenses_attribute(licenses, base_node)

    def _write_drivers(self, nosql, base_node):
        drivers = nosql.get('drivers', {})
        if drivers:
            self.language_writer.write_languages_attribute(drivers, base_node, 'SUPPORTS')

    def _write_developed_languages(self, nosql, base_node):
        developed_languages = nosql.get('developed_languages', {})
        if developed_languages:
            self.language_writer.write_languages_attribute(developed_languages, base_node, 'WRITTEN_IN')

    def _write_keywords(self, nosql, base_node):
        keywords = nosql.get('keywords', [])
        for k in keywords:
            node = self._write_resource_node(settings.KEYWORD_LABEL, k)
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _write_comments(self, nosql, base_node):
        comments = nosql.get('comments', [])
        node = self.hackernews_writer.write_review(comments)
        if node:
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _write_developers_resources(self, resources, base_node):
        for d in resources:
            node = self._write_resource_node(settings.DEVELOPER_LABEL, d)
            self.graph.merge(Relationship(base_node, 'CREATED_BY', node))

    def _write_developers_text(self, text, base_node):
        node = self.find_developer(text)
        if not node:
            node = self._write_node_by_name(settings.DEVELOPER_LABEL, text)
        self.graph.merge(Relationship(base_node, 'CREATED_BY', node))

    def _write_transactions(self, nosql, base_node):
        transaction_attr = nosql.get('transactions', {})
        resources = transaction_attr.get('resources', [])
        text = transaction_attr.get('text', '')
        if resources:
            self._write_transaction_resources(resources, base_node)
        elif text:
            self._write_transaction_text(text, base_node)

    def _write_apis(self, nosql, base_node):
        api = nosql.get('API', {})
        resources = api.get('resources', [])
        text = api.get('text', '')
        if resources:
            self._write_apis_resources(resources, base_node)
        elif text:
            self._write_apis_text(text, base_node)

    def _write_links_stats(self, nosql, base_node):
        links = nosql.get('links_stats', [])
        for link in links:
            node = self.find_link(link['url'])
            if not node:
                node = Node(settings.LINK_LABEL, url=link['url'])
                self.graph.merge(node)
            node['score'] = link['score']
            self.graph.push(node)
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _write_apis_resources(self, resources, base_node):
        for r in resources:
            node = self._write_resource_node(settings.API_LABEL, r)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_apis_text(self, text, base_node):
        names = tc.split_list_with_all_conjunctions(text)
        for name in names:
            node = self._write_node_by_name(settings.API_LABEL, name)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_transaction_resources(self, resources, base_node):
        for tr in resources:
            node = self._write_resource_node(settings.TRANSACTION_METHOD_LABEL, tr)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_transaction_text(self, text, base_node):
        names = tc.split_list_with_all_conjunctions(text)
        for name in names:
            node = self._write_node_by_name(settings.TRANSACTION_METHOD_LABEL, name)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_datatypes(self, nosql, base_node):
        datatypes = nosql.get('datatypes', [])
        for d in datatypes:
            node = self._write_node_by_name(settings.DATATYPE_LABEL, d)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))