from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter
from tasks.helpers.clients.api.neo4j.writers.base import Writer

from tasks import settings


SIMPLE_ATTRIBUTES = ['frequently_updated', 'latest_release', 'latest_release_date',
                     'homepage', 'name', 'summary', 'status', 'thumbnail']


class DBpediaWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_uniqueness_constraints()
        self.language_writer = LanguageWriter(self.graph)
        self.license_writer = LicenseWriter(self.graph)

    def _create_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.DBPEDIA_LABEL, 'uri')
        self._create_uniqueness_constraint(settings.LINK_LABEL, 'url')

    def write_dbpedia_post(self, dbpedia_post, uri):
        node = self._write_dbedia_node(dbpedia_post, uri)
        return node

    def _write_dbedia_node(self, dbpedia_post, uri):
        node = self._write_simple_attrs(dbpedia_post, uri)
        self._write_operating_systems_nodes(dbpedia_post, node)
        self._write_developers_nodes(dbpedia_post, node)
        self._write_developed_languages_nodes(dbpedia_post, node)
        self._write_links_nodes(dbpedia_post, node)
        self._write_license_node(dbpedia_post, node)
        return node

    def _write_simple_attrs(self, dbpedia_post, uri):
        node = Node(settings.DBPEDIA_LABEL, uri=uri)
        self.graph.merge(node)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = dbpedia_post.get(attr, '')
        self.graph.push(node)
        return node

    def _write_operating_systems_nodes(self, dbpedia_post, base_node):
        operating_systems = dbpedia_post.get('operating_systems', [])
        for op in operating_systems:
            node = self._write_resource_node(settings.OPERATING_SYSTEM_LABEL, op)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_developers_nodes(self, dbpedia_post, base_node):
        developers = dbpedia_post.get('developers', [])
        for d in developers:
            node = self._write_resource_node(settings.DEVELOPER_LABEL, d)
            self.graph.merge(Relationship(base_node, 'CREATED_BY', node))

    def _write_developed_languages_nodes(self, dbpedia_post, base_node):
        developed_languages = dbpedia_post.get('developed_languages', [])
        for lang in developed_languages:
            node = self.language_writer.write_language(lang)
            self.graph.merge(Relationship(base_node, 'WRITTEN_IN', node))

    def _write_license_node(self, dbpedia_post, base_node):
        licenses = dbpedia_post.get('licenses', [])
        self.license_writer._write_license_resources(licenses, base_node)

    def _write_links_nodes(self, dbpedia_post, base_node):
        links = dbpedia_post.get('external_links', [])
        for link in links:
            node = Node(settings.LINK_LABEL, url=link)
            self.graph.merge(Relationship(base_node, 'HAS', node))
