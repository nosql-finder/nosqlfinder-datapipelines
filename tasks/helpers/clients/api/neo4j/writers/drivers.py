from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks.helpers.clients.api.neo4j.writers.base import Writer

REPOSITORY_ATTRS = ['created_at', 'description', 'has_downloads', 'has_wiki', 'homepage',
                   'language', 'name', 'owner', 'stars', 'updated_at', 'url']


class DriverRepositoryWritter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.language_writter = LanguageWriter(self.graph)
        self._create_uniqueness_constraints()

    def _create_uniqueness_constraints(self):
        self._create_uniqueness_constraint('Repository', 'uri')

    def write_drivers_repositories(self, drivers, nosql_node):
        for dr in drivers:
            node = self._write_repo_node(dr, nosql_node)
            self.graph.merge(Relationship(nosql_node, 'HAS', node, type='driver'))

    def _write_repo_node(self, driver, nosql_node):
        cluster_node = self._find_or_write_driver_cluster_node(driver, nosql_node)
        self._write_repositories_nodes(driver, cluster_node)
        return cluster_node

    def _find_or_write_driver_cluster_node(self, driver, nosql_node):
        results = self._find_cluster_query(driver, nosql_node)
        if results:
            cluster_node = self._create_cluster_node_from_results(results, driver)
        else:
            cluster_node = self._create_cluster_node(driver)
        return cluster_node

    def _find_cluster_query(self, driver, nosql_node):
        lang_dbpedia_uri = driver['language']['dbpedia_uri']
        query = """MATCH (n:NoSQL)-[:HAS]-(cl:DriversCluster)-[:FOR]-(p:ProgrammingLanguage)
        WHERE n.uri = '{nosql_uri}' AND p.dbpedia_uri = '{p_uri}' RETURN cl"""
        query = query.format(nosql_uri=nosql_node['uri'], p_uri=lang_dbpedia_uri)
        results = self.graph.data(query)
        return results

    def _create_cluster_node_from_results(self, results, driver):
        cluster_node = results[0]['cl']
        cluster_node['total'] = driver['total']
        self.graph.push(cluster_node)
        return cluster_node

    def _create_cluster_node(self, driver):
        lang_dbpedia_uri = driver['language']['dbpedia_uri']
        cluster_node = Node('DriversCluster', total=driver['total'])
        self.graph.create(cluster_node)
        language_node = Node('ProgrammingLanguage', dbpedia_uri=lang_dbpedia_uri)
        self.graph.merge(language_node)
        self.graph.merge(Relationship(cluster_node, 'FOR', language_node))
        return cluster_node

    def _write_repositories_nodes(self, driver, cluster_node):
        repositories = driver.get('popular_repositories', [])
        for r in repositories:
            repo_node = self._write_repository_node(r)
            self.graph.merge(Relationship(cluster_node, 'HAS', repo_node))

    def _write_repository_node(self, repository):
        node = Node('Repository', name=repository['name'])
        self.graph.merge(node)
        for key, value in repository.items():
            node[key] = value
        self.graph.push(node)
        return node
