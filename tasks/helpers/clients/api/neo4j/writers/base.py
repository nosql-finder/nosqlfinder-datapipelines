from py2neo import Graph, Node, Relationship
from tasks.helpers.data import text_cleaner as tc
from tasks import settings


class Writer:


    def _write_resource_node(self, label, resource):
        node = Node(label, dbpedia_uri=resource['dbpedia_uri'])
        self.graph.merge(node)
        node['name'] = resource['name'].lower()
        self.graph.push(node)
        return node

    def _write_node_by_name(self, label, name):
        name = name.lower()
        name = tc.remove_text_between_square_brackets(name)
        node = Node(label, name=name)
        self.graph.merge(node)
        return node

    def _create_uniqueness_constraint(self, label, property):
        query = "CREATE CONSTRAINT ON (n:{label}) ASSERT n.{property} IS UNIQUE"
        query = query.format(label=label, property=property)
        self.graph.run(query)

    def find_programming_language(self, name):
        name = name.lower()
        name = tc.remove_text_between_square_brackets(name).strip()
        query = """MATCH (p:{}) WHERE LOWER(p.name) = '{}'
        RETURN p""".format(settings.PROGRAMMING_LANGUAGE_LABEL, name)
        result = self.graph.run(query).evaluate()
        return result

    def find_operating_system(self, name):
        query = """MATCH (op:OperatingSystem) WHERE LOWER(op.name) = LOWER('{}')
        RETURN op""".format(name)
        result = self.graph.run(query).evaluate()
        return result

    def find_movement(self, name):
        query = """MATCH (m:Movement) WHERE LOWER(m.name)=LOWER('{}')
        RETURN m""".format(name)
        result = self.graph.run(query).evaluate()
        return result

    def find_license(self, name):
        name = tc.remove_version(name)
        name = name.replace('license', '').strip()
        query = """MATCH (lic:License) WHERE toLower(lic.name) CONTAINS '{}'
        RETURN lic""".format(name)
        result = self.graph.run(query).evaluate()
        return result

    def find_developer(self, name):
        query = """MATCH (d:Developer) WHERE LOWER(d.name)=LOWER('{}')
        RETURN d""".format(name)
        result = self.graph.run(query).evaluate()
        return result

    def find_stackshare_comment(self, text, likes):
        text = text.lower()
        query = """MATCH (st:{})-[r]->(c:{}) WHERE LOWER(c.text)='{}'
        AND c.likes={} RETURN c""".format(settings.STACKSHARE_LABEL, settings.COMMENT_LABEL,
                                          text, likes)
        result = self.graph.run(query).evaluate()
        return result

    def find_alternativeto_post(self, uri):
        query = """MATCH (al:{}) WHERE al.uri='{}' RETURN al""".format(settings.ALTERNATIVETO_LABEL,
                                                                       uri)
        result = self.graph.run(query).evaluate()
        return result

    def find_link(self, url):
        query = """MATCH (link: {}) WHERE link.url='{}' RETURN link""".format(settings.LINK_LABEL, url)
        result = self.graph.run(query).evaluate()
        return result

    def find_apis(self, name):
        query = """MATCH (api:{}) WHERE api.name='{}' RETURN api""".format(settings.API_LABEL, name.lower())
        result = self.graph.run(query).evaluate()
        return result
