from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter
from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks import settings


SIMPLE_ATTRIBUTES = ['uri', 'name', 'initial_release', 'latest_release', 'documentation',
                     'is_cloud_based', 'score', 'homepage', 'summary',
                     'supports_xml', 'supports_sql', 'supports_concurrency',
                     'supports_durability']


class DBEngineWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.license_writer = LicenseWriter(self.graph)
        self.language_writer = LanguageWriter(self.graph)
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.DBENGINE_LABEL, 'uri')
        self._create_uniqueness_constraint('License', 'dbpedia_uri')
        self._create_uniqueness_constraint('Movement', 'dbpedia_uri')

    def write_dbengine_post(self, dbengine_post):
        node = self._write_dbengine_node(dbengine_post)
        return node

    def _write_dbengine_node(self, dbengine_post):
        dbengine_node = self._write_simple_attrs(dbengine_post)

        self._write_use_cases_nodes(dbengine_post, dbengine_node)
        self._write_schemas_nodes(dbengine_post, dbengine_node)
        self._write_access_methods_nodes(dbengine_post, dbengine_node)
        self._write_transactions_methods_nodes(dbengine_post, dbengine_node)
        self._write_datatypes_nodes(dbengine_post, dbengine_node)
        self._write_developer_node(dbengine_post, dbengine_node)
        self._write_consistency_methods_nodes(dbengine_post, dbengine_node)
        self._write_replication_methods_nodes(dbengine_post, dbengine_node)
        self._write_operating_systems_nodes(dbengine_post, dbengine_node)
        self._write_partitioning_methods_nodes(dbengine_post, dbengine_node)
        self._write_apis_nodes(dbengine_post, dbengine_node)

        self._write_license_node(dbengine_post, dbengine_node)
        self._write_developed_languages_nodes(dbengine_post, dbengine_node)
        self._write_drivers_nodes(dbengine_post, dbengine_node)
        return dbengine_node

    def _write_simple_attrs(self, dbengine_post):
        node = Node(settings.DBENGINE_LABEL, uri=dbengine_post['uri'])
        self.graph.merge(node)
        for attr in SIMPLE_ATTRIBUTES:
            if dbengine_post.get(attr):
                node[attr] = dbengine_post[attr]
        self.graph.push(node)
        return node

    def _write_use_cases_nodes(self, dbengine_post, dbengine_node):
        use_cases = dbengine_post.get('use_cases', [])
        for use in use_cases:
            node = self._write_node_by_name(settings.USE_CASE_LABEL, use.lower())
            self.graph.merge(Relationship(dbengine_node, 'USED_FOR', node))

    def _write_schemas_nodes(self, dbengine_post, dbengine_node):
        schemas = dbengine_post.get('data_schemes', [])
        for s in schemas:
            node = self._write_node_by_name(settings.SCHEMA_LABEL, s.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_access_methods_nodes(self, dbengine_post, dbengine_node):
        methods = dbengine_post.get('access_methods', [])
        for m in methods:
            node = self._write_node_by_name(settings.ACCESS_METHOD_LABEL, m.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_transactions_methods_nodes(self, dbengine_post, dbengine_node):
        methods = dbengine_post.get('transaction_methods', [])
        for m in methods:
            node = self._write_node_by_name(settings.TRANSACTION_METHOD_LABEL, m.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_datatypes_nodes(self, dbengine_post, dbengine_node):
        datatypes = dbengine_post.get('datatypes', [])
        for d in datatypes:
            node = self._write_node_by_name(settings.DATATYPE_LABEL, d.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_developer_node(self, dbengine_post, dbengine_node):
        developer = dbengine_post.get('developer')
        if developer:
            node = self._write_node_by_name(settings.DEVELOPER_LABEL, developer.lower())
            self.graph.merge(Relationship(dbengine_node, 'CREATED_BY', node))

    def _write_license_node(self, dbengine_post, dbengine_node):
        licenses_attr = dbengine_post.get('licenses', {})
        self.license_writer.write_licenses_attribute(licenses_attr, dbengine_node)

    def _write_license_resources_nodes(self, resources, base_node):
        for r in resources:
            license = r.get('license', {})
            if license:
                node = self._write_resource_node('License', license)
                self._write_movements(r.get('movements', []), node)
                self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))
            else:
                self._write_movements(r.get('movements', []), base_node)

    def _write_movements(self, movements, base_node):
        for r in movements:
            node = Node('Movement', dbpedia_uri=r)
            self.graph.merge(node)
            self.graph.merge(Relationship(base_node, 'APPROVED_BY', node))

    def _write_license_name(self, text, base_node):
        node = Node('License', name=text.lower())
        self.graph.merge(node)
        self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))

    def _write_developed_languages_nodes(self, dbengine_post, dbengine_node):
        developed_langs_attr = dbengine_post.get('developed_languages', {})
        resources = developed_langs_attr.get('resources', [])
        for lang in resources:
            node = self.language_writer.write_language(lang)
            self.graph.merge(Relationship(dbengine_node, 'WRITTEN_IN', node))

    def _write_consistency_methods_nodes(self, dbengine_post, dbengine_node):
        methods = dbengine_post.get('consistency_methods', [])
        for m in methods:
            node = self._write_node_by_name(settings.CONSISTENCY_METHOD_LABEL, m.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_drivers_nodes(self, dbengine_post, dbengine_node):
        drivers_attr = dbengine_post.get('drivers', {})
        resources = drivers_attr.get('resources', [])
        for dr in resources:
            status = dr.pop('status')
            lang_node = self.language_writer.write_language(dr)
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', lang_node, status=status))

    def _write_replication_methods_nodes(self, dbengine_post, dbengine_node):
        methods = dbengine_post.get('replication_methods', [])
        for m in methods:
            node = self._write_node_by_name(settings.REPLICATION_METHOD_LABEL, m.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_apis_nodes(self, dbengine_post, dbengine_node):
        apis = dbengine_post.get('apis', [])
        for api in apis:
            node = self.find_apis(api)
            if not node:
                node = Node(settings.API_LABEL, name=api.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))    

    def _write_operating_systems_nodes(self, dbengine_post, dbengine_node):
        operating_systems = dbengine_post.get('operating_systems', {})
        resources = operating_systems.get('resources', [])
        text = operating_systems.get('text', '')
        self._write_operating_systems_resources_nodes(resources, dbengine_node)
        self._write_operating_systems_text_nodes(text, dbengine_node)

    def _write_operating_systems_resources_nodes(self, resources, dbengine_node):
        for r in resources:
            node = self._write_resource_node(settings.OPERATING_SYSTEM_LABEL, r)
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_operating_systems_text_nodes(self, text, dbengine_node):
        if isinstance(text, list):
            for name in text:
                node = self._write_node_by_name(settings.OPERATING_SYSTEM_LABEL, name.lower())
        if text and isinstance(text, str):
            node = self._write_node_by_name(settings.OPERATING_SYSTEM_LABEL, text.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))

    def _write_partitioning_methods_nodes(self, dbengine_post, dbengine_node):
        methods = dbengine_post.get('partitioning_methods', [])
        for m in methods:
            node = self._write_node_by_name(settings.PARTITIONING_METHOD_LABEL, m.lower())
            self.graph.merge(Relationship(dbengine_node, 'SUPPORTS', node))
