from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter

from tasks import settings


SIMPLE_ATTRIBUTES = ['summary', 'created_at', 'views', 'number_questions',
                     'name', 'unanswered_questions', 'active']


class StackoverflowWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_all_uniqueness_constraints()
        self.language_writter = LanguageWriter(self.graph)

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.STACKOVERFLOW_LABEL, 'uri')

    def write_stackoverflow_tag(self, tag):
        node = self._write_stackoverflow_node(tag)
        return node

    def _write_stackoverflow_node(self, tag):
        node = self._write_simple_attrs(tag)
        self._write_related_tags(tag, node)
        return node

    def _write_simple_attrs(self, tag):
        node = Node(settings.STACKOVERFLOW_LABEL, uri=tag['uri'])
        self.graph.merge(node)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = tag[attr]
        self.graph.push(node)
        return node

    def _write_related_tags(self, tag, base_node):
        related_tags = tag.get('related_tags', [])
        for tag in related_tags:
            weight = tag.pop('weight')
            node = self._write_related_tag_node(tag)
            self.graph.merge(Relationship(base_node, 'RELATED_TO', node, weight=weight))

    def _write_related_tag_node(self, tag):
        node = self._write_base_related_tag(tag)
        dbpedia_uri = tag.get('dbpedia_uri')
        if dbpedia_uri:
            self._write_language_node(tag, node)
        return node

    def _write_language_node(self, tag, base_node):
        lang_node = self.language_writter.write_language(tag)
        self.graph.merge(Relationship(lang_node, 'SAME_AS', base_node))

    def _write_base_related_tag(self, tag):
        node = Node(settings.STACKOVERFLOW_LABEL, uri=tag['uri'], name=tag['name'])
        self.graph.merge(node)
        return node
