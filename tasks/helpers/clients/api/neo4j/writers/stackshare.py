from py2neo import Graph, Node, Relationship
from py2neo.database.status import ConstraintError

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks import settings


SIMPLE_ATTRIBUTES = ['uri', 'name', 'thumbnail', 'forks', 
                    'votes', 'repository_stars', 'summary',
                    'favorites', 'homepage', 'fans']


class StackshareWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.STACKSHARE_LABEL, 'uri')
        self._create_uniqueness_constraint(settings.CLIENT_LABEL, 'uri')
        self._create_uniqueness_constraint(settings.TOOL_LABEL, 'uri')

    def create_stackshare_node(self, stackshare_post):
        stackshare_node = self._write_simple_attributes(stackshare_post)
        self._bind_comments(stackshare_post, stackshare_node)
        self._bind_clients(stackshare_post, stackshare_node)
        self._bind_tools(stackshare_post, stackshare_node)
        return stackshare_node

    def _write_simple_attributes(self, stackshare_post):
        node = Node(settings.STACKSHARE_LABEL)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = stackshare_post[attr]
        self.graph.merge(node)
        return node

    def _bind_comments(self, stackshare_post, base_node):
        comments = stackshare_post.get('comments', [])
        for c in comments:
            try:
                node = Node(settings.COMMENT_LABEL, text=c['text'].lower(), likes=c['likes'])
                self.graph.merge(Relationship(base_node, 'HAS', node))
            except ConstraintError:
                continue


    def _bind_clients(self, stackshare_post, base_node):
        clients = stackshare_post.get('clients', [])
        for c in clients:
            node = Node(settings.CLIENT_LABEL)
            node['uri'] = c['uri']
            node['name'] = c['name']
            node['thumbnail'] = c['thumbnail']
            self.graph.merge(Relationship(base_node, 'USED_BY', node))

    def _bind_tools(self, stackshare_post, base_node):
        tools = stackshare_post.get('tools', [])
        for t in tools:
            node = Node(settings.TOOL_LABEL)
            node['uri'] = t['uri']
            node['name'] = t['name']
            node['thumbnail'] = t['thumbnail']
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))
