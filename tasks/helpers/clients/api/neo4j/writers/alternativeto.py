from py2neo import Graph, Node, Relationship, NodeSelector

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks import settings


SIMPLE_ATTRIBUTES = ['uri', 'name', 'thumbnail', 'repository',
                     'homepage', 'likes', 'summary', 'description']


class AlternativetoWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.selector = NodeSelector(graph)
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.ALTERNATIVETO_LABEL, 'uri')

    def write_alternativeto(self, nosql):
        alternativeto_post = nosql.get('alternativeto_post')
        if alternativeto_post:
            node = self._write_simple_attributes(alternativeto_post)
            self._bind_alternatives(alternativeto_post, node)
            self._bind_labels_nodes(alternativeto_post, node)
            return node
        return None

    def _write_simple_attributes(self, alternativeto_post):
        node = self.find_alternativeto_post(alternativeto_post['uri'])
        if not node:
            node = Node(settings.ALTERNATIVETO_LABEL)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = alternativeto_post[attr]
        self.graph.merge(node)
        return node

    def _bind_alternatives(self, alternativeto_post, base_node):
        alternatives = alternativeto_post.get('alternatives', [])
        for al in alternatives:
            node = self.find_alternativeto_post(al['uri'])
            if not node:
                node = Node(settings.ALTERNATIVETO_LABEL, uri=al['uri'])
                node['name'] = al['name']
                node['thumbnail'] = al['thumbnail']
            node['likes'] = al['likes']
            self.graph.merge(Relationship(base_node, 'RELATED_TO', node))

    def _bind_labels_nodes(self, alternativeto_post, base_node):
        labels = alternativeto_post.get('labels', [])
        for label in labels:
            if label.get('dbpedia_uri'):
                self._bind_based_on_node_type(label, base_node)

    def _bind_based_on_node_type(self, label, base_node):
        dbpedia_uri = label['dbpedia_uri']
        node = self._find_types(dbpedia_uri)
        if node:
            self._bind_resources(node, base_node)
        else:
            node = Node(settings.LABEL_FROM_ALTERNATIVETO, name=label['name'],
                        dbpedia_uri=label['dbpedia_uri'])
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _bind_resources(self, node, base_node):
        if settings.MOVEMENT_LABEL in node.labels():
            self.graph.merge(Relationship(base_node, 'APPROVED_BY', node))
        elif settings.OPERATING_SYSTEM_LABEL in node.labels():
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _find_types(self, dbpedia_uri):
        node = self.find_by_label(settings.MOVEMENT_LABEL, dbpedia_uri)
        if not node:
            node = self.find_by_label(settings.OPERATING_SYSTEM_LABEL, dbpedia_uri)
        return node

    def find_by_label(self, label, dbpedia_uri):
        selected = self.selector.select(label, dbpedia_uri=dbpedia_uri)
        node = selected.first()
        return node
