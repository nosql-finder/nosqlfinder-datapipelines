from py2neo import Graph, Node, Relationship, NodeSelector

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks import settings

SIMPLE_ATTRIBUTES = ['url', 'name', 'has_downloads', 'has_wiki',
                     'stars', 'updated_at', 'created_at', 'homepage',
                     'summary', 'owner']


class DriverWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.selector = NodeSelector(graph)
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.REPOSITORY_LABEL, 'uri')

    def write_driver_repository(self, drivers_repositories, base_node):
        for dr in drivers_repositories:
            node = self._write_driver_cluster_node(dr, base_node)
            node['uri'] = base_node['uri']
            self._bind_popular_repositories(dr, node)
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _write_driver_cluster_node(self, repository, base_node):
        language = repository.get('language')
        node = Node(settings.DRIVER_REPOS_CLUSTER_LABEL, count=repository['total'], uri=base_node['uri'])
        lang_node = self._get_language_node(language)
        if lang_node:
            self.graph.merge(Relationship(node, 'FOR', lang_node))
        return node

    def _get_language_node(self, language):
        dbpedia_uri = language.get('dbpedia_uri')
        node = None
        if dbpedia_uri:
            node = self.find_programming_language(dbpedia_uri)
            if not node:
                node = self._write_resource_node(settings.PROGRAMMING_LANGUAGE_LABEL,
                                                 language)
        return node

    def _bind_popular_repositories(self, repository, base_node):
        repositories = repository.get('popular_repositories', [])
        for repo in repositories:
            node = self._write_repository_node(repo)
            self.graph.merge(Relationship(base_node, 'HAS', node))

    def _write_repository_node(self, repository):
        node = Node(settings.REPOSITORY_LABEL)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = repository[attr]
        return node

    def find_programming_language(self, dbpedia_uri):
        selected = self.selector.select(settings.PROGRAMMING_LANGUAGE_LABEL,
                                        dbpedia_uri=dbpedia_uri)
        node = selected.first()
        return node


REPOSITORY_SIMPLE_ATTRIBUTES = [
    'uri', 'user', 'name', 'homepage', 'forks', 'stars'
]


class RepositoryWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.REPOSITORY_LABEL, 'uri')

    def write_repository(self, repository):
        node = self._write_simple_attributes(repository)
        lang_node = self.find_programming_language(repository['language'])
        if lang_node:
            self.graph.merge(Relationship(node, 'WRITTEN_IN', lang_node))
        return node

    def _write_simple_attributes(self, repository):
        node = Node(settings.REPOSITORY_LABEL)
        for attr in REPOSITORY_SIMPLE_ATTRIBUTES:
            node[attr] = repository[attr]
        return node
