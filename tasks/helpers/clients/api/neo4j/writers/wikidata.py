from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter
from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks import settings


SIMPLE_ATTRIBUTES = ['repository', 'stack_exchange_tag',
                     'twitter_username', 'facebook_username',
                     'quora_topic', 'thumbnail', 'latest_release', 'homepage']


class WikidataWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.language_writer = LanguageWriter(self.graph)
        self.license_writer = LicenseWriter(self.graph)
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.WIKIDATA_LABEL, 'uri')

    def write_wikidata_post(self, wikidata_post):
        node = self._write_simple_attributes(wikidata_post)
        self._write_developed_languages(wikidata_post, node)
        self._write_licenses(wikidata_post, node)
        self._write_developer(wikidata_post, node)
        self._write_operating_systems(wikidata_post, node)
        return node

    def _write_simple_attributes(self, wikidata_post):
        node = Node(settings.WIKIDATA_LABEL)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = wikidata_post[attr]
        self.graph.merge(node)
        return node

    def _write_developed_languages(self, wikidata_post, wikidata_node):
        developed_languages = wikidata_post.get('developed_languages', [])
        for lang in developed_languages:
            node = self.language_writer.write_language(lang)
            self.graph.merge(Relationship(wikidata_node, 'WRITTEN_IN', node))

    def _write_licenses(self, wikidata_post, wikidata_node):
        licenses = wikidata_post.get('licenses', [])
        format_data = {'resources': licenses}
        self.license_writer.write_licenses_attribute(format_data, wikidata_node)

    def _write_developer(self, wikidata_post, wikidata_node):
        developers = wikidata_post.get('developers', [])
        for d in developers:
            node = self._write_resource_node(settings.DEVELOPER_LABEL, d)
            self.graph.merge(Relationship(wikidata_node, 'CREATED_BY', node))

    def _write_operating_systems(self, wikidata_post, wikidata_node):
        operating_systems = wikidata_post.get('operating_systems', [])
        for op in operating_systems:
            node = self._write_resource_node(settings.OPERATING_SYSTEM_LABEL, op)
            self.graph.merge(Relationship(wikidata_node, 'SUPPORTS', node))
