from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.resources import LicenseWriter
from tasks.helpers.clients.api.neo4j.writers.resources import LanguageWriter
from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks.helpers.data import text_cleaner as tc
from tasks import settings


SIMPLE_ATTRIBUTES = ['initial_release', 'latest_release', 'homepage',
                     'status', 'thumbnail', 'repository']


class WikipediaWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self.license_writer = LicenseWriter(self.graph)
        self.language_writer = LanguageWriter(self.graph)
        self._create_uniqueness_constraints()

    def _create_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.WIKIPEDIA_LABEL, 'uri')
        self._create_uniqueness_constraint(settings.DEVELOPER_LABEL, 'dbpedia_uri')
        self._create_uniqueness_constraint(settings.DEVELOPER_LABEL, 'name')
        self._create_uniqueness_constraint(settings.OPERATING_SYSTEM_LABEL, 'dbpedia_uri')
        self._create_uniqueness_constraint(settings.OPERATING_SYSTEM_LABEL, 'name')

    def write_wikipedia_post(self, wikipedia_post):
        node = self._write_wikipedia_node(wikipedia_post)
        return node

    def _write_wikipedia_node(self, wikipedia_post):
        node = self._write_simple_attrs(wikipedia_post)
        self._write_developed_languages_nodes(wikipedia_post, node)
        self._write_developer_node(wikipedia_post, node)
        self._write_operating_systems_nodes(wikipedia_post, node)
        self._write_licenses_nodes(wikipedia_post, node)
        return node

    def _write_simple_attrs(self, wikipedia_post):
        node = Node(settings.WIKIPEDIA_LABEL, uri=wikipedia_post['uri'])
        self.graph.merge(node)
        for attr in SIMPLE_ATTRIBUTES:
            node[attr] = wikipedia_post.get(attr)
        self.graph.push(node)
        return node

    def _write_developed_languages_nodes(self, wikipedia_post, base_node):
        languages_attr = wikipedia_post.get('developed_languages', {})
        self.language_writer.write_languages_attribute(languages_attr, base_node, 'WRITTEN_IN')

    def _write_developer_node(self, wikipedia_post, base_node):
        developer = wikipedia_post.get('developer', {})
        resources = developer.get('resources', [])
        if resources:
            self._write_developer_resources_nodes(resources, base_node)
        else:
            text = developer.get('text', '')
            self._write_developer_name_node(text, base_node)

    def _write_licenses_nodes(self, wikipedia_post, base_node):
        licenses_attr = wikipedia_post.get('licenses', {})
        self.license_writer.write_licenses_attribute(licenses_attr, base_node)

    def _write_license_resources_nodes(self, resources, base_node):
        for r in resources:
            license = r.get('license', {})
            if license:
                node = self._write_resource_node(settings.LICENSE_LABEL, license)
                self._write_movements(r.get('movements', []), node)
                self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))
            else:
                self._write_movements(r.get('movements', []), base_node)

    def _write_license_name(self, text, base_node):
        node = Node('License', name=text.lower())
        self.graph.merge(node)
        self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))

    def _write_movements(self, movements, base_node):
        for r in movements:
            node = Node(settings.MOVEMENT_LABEL, dbpedia_uri=r)
            self.graph.merge(node)
            self.graph.merge(Relationship(base_node, 'APPROVED_BY', node))

    def _write_developer_resources_nodes(self, resources, base_node):
        for r in resources:
            node = self._write_resource_node(settings.DEVELOPER_LABEL, r)
            self.graph.merge(Relationship(base_node, 'CREATED_BY', node))

    def _write_developer_name_node(self, text, base_node):
        node = Node('Developer', name=text.lower())
        self.graph.merge(node)
        self.graph.merge(Relationship(base_node, 'CREATED_BY', node))

    def _write_operating_systems_nodes(self, wikipedia_post, base_node):
        operating_systems = wikipedia_post.get('operating_systems', {})
        resources = operating_systems.get('resources', [])
        text = operating_systems.get('text', '')
        self._write_operating_system_resource_node(resources, base_node)
        if text:
            self._write_operating_system_name_node(text, base_node)

    def _write_operating_system_resource_node(self, resources, base_node):
        for r in resources:
            node = self._write_resource_node(settings.OPERATING_SYSTEM_LABEL, r)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))

    def _write_operating_system_name_node(self, text, base_node):
        names = tc.split_list_with_all_conjunctions(text)
        for name in names:
            node = self.find_operating_system(text)
            if not node:
                node = Node(settings.OPERATING_SYSTEM_LABEL, name=name.lower())
                self.graph.merge(node)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))
