from py2neo import Graph, Node, Relationship

from tasks.helpers.clients.api.neo4j.writers.base import Writer
from tasks.helpers.data import text_cleaner as tc
from tasks import settings


class LicenseWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.LICENSE_LABEL, 'dbpedia_uri')
        self._create_uniqueness_constraint(settings.MOVEMENT_LABEL, 'dbpedia_uri')

    def write_licenses_attribute(self, attr, base_node):
        resources = attr.get('resources', [])
        text = attr.get('text', '')
        if resources:
            self._write_license_resources(resources, base_node)
        if text and not resources:
            self._write_licenses_text(text, base_node)

    def _write_license_resources(self, resources, base_node):
        for r in resources:
            self._write_license_with_movements(r, base_node)

    def _write_licenses_text(self, text, base_node):
        names = tc.split_list_with_all_conjunctions(text)
        for name in names:
            name = name.lower()
            node = self._write_if_license(name, base_node)
            if not node:
                node = self._write_if_movement(name, base_node)
            if not node:
                node = self._write_node_by_name(settings.LICENSE_LABEL, name)
                self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))

    def _write_if_license(self, name, base_node):
        node = self.find_license(name)
        if node:
            self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', node))
        return node

    def _write_if_movement(self, name, base_node):
        node = self.find_movement(name)
        if node:
            self.graph.merge(Relationship(base_node, 'APPROVED_BY', node))
        return node

    def _write_license_with_movements(self, resource, base_node):
        license = resource.get('license')
        movements = resource.get('movements', [])
        if license:
            license_node = self._write_resource_node('License', license)
            self._write_movements_nodes(movements, license_node)
            self.graph.merge(Relationship(base_node, 'LICENSED_UNDER', license_node))
        else:
            self._write_movements_nodes(movements, base_node)

    def _write_movements_nodes(self, movements, base_node):
        for m in movements:
            node = Node(settings.MOVEMENT_LABEL,
                        dbpedia_uri=m['dbpedia_uri'])
            self.graph.merge(node)
            if m.get('name', ''):
                node['name'] = m['name']
                self.graph.push(node)
            self.graph.merge(Relationship(base_node, 'APPROVED_BY', node))


class LanguageWriter(Writer):

    def __init__(self, graph):
        self.graph = graph
        self._create_all_uniqueness_constraints()

    def _create_all_uniqueness_constraints(self):
        self._create_uniqueness_constraint(settings.PROGRAMMING_LANGUAGE_LABEL, 'dbpedia_uri')
        self._create_uniqueness_constraint(settings.PROGRAMMING_PARADIGM_LABEL, 'dbpedia_uri')

    def write_languages_attribute(self, attr, base_node, relation):
        resources = attr.get('resources', [])
        text = attr.get('text', '')
        self._write_language_resources(resources, base_node, relation)
        if text:
            self._write_languages_text(text, base_node, relation)

    def _write_languages_text(self, text, base_node, relation):
        names = tc.split_list_with_all_conjunctions(text)
        for name in names:
            node = self.find_programming_language(name)
            if not node:
                node = self._write_node_by_name(settings.PROGRAMMING_LANGUAGE_LABEL, name)
            self.graph.merge(Relationship(base_node, relation, node))

    def _write_language_resources(self, resources, base_node, relation):
        for r in resources:
            node = self.write_language(r)
            self.graph.merge(Relationship(base_node, relation, node))

    def write_language(self, resource):
        node = Node(settings.PROGRAMMING_LANGUAGE_LABEL, dbpedia_uri=resource['dbpedia_uri'])
        self.graph.merge(node)
        node['name'] = resource['name'].lower()
        if not node.get('summary'):
            node['summary'] = resource.get('summary', '')
        self.graph.push(node)
        self._write_influenced_languages_nodes(resource, node)
        self._write_paradigms_nodes(resource, node)
        return node

    def _write_influenced_languages_nodes(self, resource, base_node):
        influenced_langs = resource.get('influenced_by', [])
        for lang in influenced_langs:
            node = Node(settings.PROGRAMMING_LANGUAGE_LABEL, dbpedia_uri=lang)
            self.graph.merge(node)
            self.graph.merge(Relationship(base_node, 'INFLUENCED_BY', node))

    def _write_paradigms_nodes(self, resource, base_node):
        paradigms = resource.get('paradigms', [])
        for p in paradigms:
            node = Node(settings.PROGRAMMING_PARADIGM_LABEL, dbpedia_uri=p)
            self.graph.merge(node)
            self.graph.merge(Relationship(base_node, 'SUPPORTS', node))
