from github import Github
from ratelimit import *
from retry import retry
import requests
from tasks import settings
import time

from tasks.helpers.clients.api.exceptions import ApiError
from github.GithubException import RateLimitExceededException


class GihubDriversClient:

    def __init__(self, languages):
        self.languages = languages
        self.client = Github(settings.GITHUB_ACCESS_TOKEN, per_page=10)

    def get_repos(self, name):
        repos = []
        for lang in self.languages:
            lang_repos = self._get_popular_repositories(name, lang)
            repos.append(lang_repos)
        return repos

    @retry(exceptions=(RateLimitExceededException, Exception), tries=-1, delay=30)
    def _get_popular_repositories(self, name, language):
        query = self._get_query(name)
        language_name = language['name']
        repos = self.client.search_repositories(query, sort="stars", order="desc", language=language_name)
        popular_repos = repos.get_page(1)
        popular_repos = self._clean_repos(popular_repos)
        clean_repos = {'popular_repositories': popular_repos,
                       'total': repos.totalCount,
                       'language': language}
        return clean_repos

    def _clean_repos(self, repos):
        clean_repos = []
        for r in repos:
            r = self._format_repo(r)
            clean_repos.append(r)
        return clean_repos

    def _format_repo(self, repo):
        clean_repo = {}
        clean_repo['name'] = repo.name
        clean_repo['summary'] = repo.description
        clean_repo['url'] = repo.url
        clean_repo['created_at'] = repo.created_at.isoformat()
        clean_repo['updated_at'] = repo.updated_at.isoformat()
        clean_repo['owner'] = repo.owner.login
        clean_repo['language'] = repo.language
        clean_repo['homepage'] = repo.homepage
        clean_repo['has_wiki'] = repo.has_wiki
        clean_repo['has_downloads'] = repo.has_downloads
        clean_repo['stars'] = repo.stargazers_count
        return clean_repo

    def _get_query(self, name):
        return "{} drivers".format(name)





class GithubRepositoryClient:

    def __init__(self):
        self.base_url = 'https://api.github.com/repos/{owner}/{repo_name}?access_token=b66432481c152607eb89642c9735646e2ebde250'

    def get_repo_stats(self, repository):
        repo_data = {}
        if repository and self._is_github_repo(repository):
            owner, name = self._get_owner_and_name_from_repo_url(repository)
            repo_data = self._get_repo_data(owner, name)
        return repo_data

    def _is_github_repo(self, name):
        return 'github.com' in name

    def _get_owner_and_name_from_repo_url(self, repo):
        repo = repo.replace('.git', '')
        repo = repo.split('/')
        owner = repo[-2]
        repo_name = repo[-1]
        return owner, repo_name

    def _get_repo_data(self, owner, repo_name):
        data = self._call_api(owner, repo_name)
        if(data):
            return self._clean_repo(data)
        else:
            return {}

    @rate_limited(70)
    def _call_api(self, owner, repo_name):
        url = self.base_url.format(owner=owner, repo_name=repo_name)
        response = requests.get(url)
        if response.status_code == 404:
            return {}
        elif response.status_code != 200:
            raise ApiError('Cannot call API: {}'.format(response.status_code))
        return response.json()

    def _clean_repo(self, repository):
        clean_repo = {}
        clean_repo['user'] = repository['owner']['login']
        clean_repo['stars'] = repository['stargazers_count']
        clean_repo['forks'] = repository['forks']
        clean_repo['language'] = repository['language']
        clean_repo['homepage'] = repository['homepage']
        clean_repo['uri'] = repository['html_url']
        clean_repo['name'] = repository['name']
        return clean_repo
