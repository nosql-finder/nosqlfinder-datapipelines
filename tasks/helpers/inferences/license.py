import json

from tasks import settings


OPEN_SOURCE = {'dbpedia_uri': 'Open-source_license', 'name': 'open source'}
FREE = {'dbpedia_uri': 'Free_software', 'name': 'free'}
COMMERCIAL = {'dbpedia_uri': 'Commercial_software', 'name': 'commercial'}
PROPIETARY = {'dbpedia_uri': 'Proprietary_software', 'name': 'proprietary'}
FREEWARE = {'dbpedia_uri': 'Freeware', 'name': 'freeware'}


class MovementInference:

    def __init__(self):
        self.entities = self._read_entities()

    def _read_entities(self):
        entities = []
        with open(settings.LICENSES_ENTITIES, 'r') as file:
            entities = json.load(file)
        return entities

    def get_license_movements(self, license):
        movements = []
        license = self._clean_license(license)
        movements += self._get_movements_from_uri(license)
        movements += self._get_movements_from_instances(license)
        movements += self._get_movements_from_subclasses(license)
        movements += self._get_movements_from_approved_by(license)
        movements += self._get_movements_from_topics(license)
        movements += self._get_movements_from_opposites(license)
        movements += self._get_movements_from_movements(license)
        return movements

    def _clean_license(self, license):
        license = self._clean_wikidata_uri(license, 'movements')
        license = self._clean_wikidata_uri(license, 'publishers')
        license = self._clean_wikidata_uri(license, 'approved_by')
        license = self._clean_wikidata_uri(license, 'instances')
        license = self._clean_wikidata_uri(license, 'topics')
        license = self._clean_wikidata_uri(license, 'subclasses')
        license = self._clean_wikidata_uri(license, 'opposites')
        return license

    def _clean_wikidata_uri(self, license, attr):
        resources = license.get(attr, [])
        resources = [r.replace('http://www.wikidata.org/entity/', '')
                     for r in resources]
        license[attr] = resources
        return license

    def _get_movements_from_uri(self, license):
        movements = self._get_movement(license)
        return movements

    def _get_movements_from_movements(self, license):
        movements = license.get('movements', [])
        movements = self._get_movements(movements)
        return movements

    def _get_movements_from_instances(self, license):
        instances = license.get('instances', [])
        movements = self._get_movements(instances)
        return movements

    def _get_movements_from_approved_by(self, license):
        approved = license.get('approved_by', [])
        movements = self._get_movements(approved)
        return movements

    def _get_movements_from_subclasses(self, license):
        subclasses = license.get('subclasses', [])
        movements = self._get_movements(subclasses)
        return movements

    def _get_movements_from_topics(self, license):
        topics = license.get('topics', [])
        movements = self._get_movements(topics)
        return movements

    def _get_movements_from_opposites(self, license):
        opposites = license.get('opposites', [])
        movements = self._get_opposite_movements(opposites)
        return movements

    def _get_movements(self, licenses):
        movements = []
        for license in licenses:
            movements += self._get_movement(license)
        return movements

    def _get_movement(self, license):
        movements = []
        if self._is_free(license):
            movements.append(FREE)
        if self._is_open_source(license):
            movements.append(OPEN_SOURCE)
        if self._is_propietary(license):
            movements.append(PROPIETARY)
        if self._is_commercial(license):
            movements.append(COMMERCIAL)
        if self._is_freeware(license):
            movements.append(FREEWARE)
        return movements

    def _get_opposite_movements(self, licenses):
        movements = []
        for license in licenses:
            if self._is_freeware(license):
                movements.append(COMMERCIAL)
            if self._is_commercial(license):
                movements.append(FREEWARE)
            if self._is_free(license):
                movements.append(PROPIETARY)
            if self._is_open_source(license):
                movements.append(PROPIETARY)
            if self._is_propietary(license):
                movements.append(OPEN_SOURCE)
        return movements

    def _is_free(self, license):
        return license == self.entities['free_software'] \
                or license == self.entities['free_software_license'] \
                or license == self.entities['free_license'] \
                or license == self.entities['free_software_movement'] \
                or license == self.entities['free_software_foundation'] \
                or license == self.entities['permissive_free_software_license'] \
                or license == self.entities['free_and_open_software']

    def _is_open_source(self, license):
        return license == self.entities['open_source_software'] \
                or license == self.entities['open_source_license'] \
                or license == self.entities['open_source_movement'] \
                or license == self.entities['free_and_open_software']

    def _is_propietary(self, license):
        return license == self.entities['non_free_license']\
                or license == self.entities['propietary_software'] \
                or license == self.entities['shareware'] \
                or license == self.entities['freeware']

    def _is_freeware(self, license):
        return license == self.entities['freeware']

    def _is_commercial(self, license):
        return license == self.entities['commercial_software']
