import json
import luigi
from luigi.format import UTF8
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.scrapper.stackshare import StackshareScrapper

from tasks.helpers.data import filters
from tasks.helpers.files import csv
from tasks import settings


class StackshareMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.STACKSHARE_MAPPER_FILE, format=UTF8)


class FindStacksharePostTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                'stackshares': StackshareMapperFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.STACKSHARE_OUTPUT_PATH,
                                   'technologies_stackshare_posts_mapper_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['stackshares'].open('r') as file:
            self.stackshares = csv.read_csv(file)

        new_techs = []
        for t in technologies:
            t = self._add_stackshare_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_stackshare_post(self, technology):
        uri = technology.get('uri')
        stackshare_map = filters.find_object_in_list_by_uri(uri, self.stackshares)
        if stackshare_map:
            technology['stackshare_post'] = stackshare_map['stackshare_post']
        return technology


class AddStackshareFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return FindStacksharePostTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.STACKSHARE_OUTPUT_PATH,
                                   'technologies_stackshare_posts_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        new_techs = []
        for t in technologies:
            t = self._add_stackshare_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_stackshare_post(self, technology):
        stackshare_name = technology.get('stackshare_post')
        post = {}
        if stackshare_name:
            scrapper = StackshareScrapper(stackshare_name)
            post = scrapper.get_stackshare_post()
        technology['stackshare_post'] = post
        return technology
