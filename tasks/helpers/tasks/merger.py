import json
import luigi
import os.path

from tasks.helpers.tasks.alternativeto import AddAlternativeToFieldsTask
from tasks.helpers.tasks.wikipedia import AddWikipediaFieldsTask
from tasks.helpers.tasks.dbengine import AddDBEnginePostFieldsTask
from tasks.helpers.tasks.dbpedia import AddDBpediaFieldsTask
from tasks.helpers.tasks.stackoverflow import AddStackoverflowFieldsTask
from tasks.helpers.tasks.wikidata import TranslateFieldsToDBpedia

from tasks.helpers.data import filters
from tasks import settings


class MergeFieldsWithDBpediaURITask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        task = {'alternativetos': AddAlternativeToFieldsTask(self.date_interval),
                'wikipedias': AddWikipediaFieldsTask(self.date_interval),
                'dbengines': AddDBEnginePostFieldsTask(self.date_interval),
                'dbpedias': AddDBpediaFieldsTask(self.date_interval),
                'stackoverflows': AddStackoverflowFieldsTask(self.date_interval),
                'wikidatas': TranslateFieldsToDBpedia(self.date_interval)}
        return task

    def output(self):
        output_path = os.path.join(settings.DBPEDIA_MERGED_OUTPUT_PATH,
                                   'dbpedia_merged_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['alternativetos'].open('r') as input:
            self.alternativetos = json.load(input)

        with self.input()['wikipedias'].open('r') as input:
            self.wikipedias = json.load(input)

        with self.input()['dbengines'].open('r') as input:
            self.dbengines = json.load(input)

        with self.input()['dbpedias'].open('r') as input:
            dbpedias = json.load(input)

        with self.input()['stackoverflows'].open('r') as input:
            self.stackoverflows = json.load(input)

        with self.input()['wikidatas'].open('r') as input:
            self.wikidatas = json.load(input)

        merged_technologies = []
        for t in dbpedias:
            t = self._merge(t)
            merged_technologies.append(t)

        with self.output().open('w') as output:
            json.dump(merged_technologies, output)

    def _merge(self, technology):
        technology = self._add_alternativeto_post(technology)
        technology = self._add_wikipedia_post(technology)
        technology = self._add_dbengine_post(technology)
        technology = self._add_stackoverflow_tag(technology)
        technology = self._add_wikidata_post(technology)
        return technology

    def _add_alternativeto_post(self, technology):
        uri = technology['uri']
        alternativeto_post = filters.find_object_in_list_by_uri(uri, self.alternativetos)
        alternativeto_post = alternativeto_post.get('alternativeto_post', {})
        technology['alternativeto_post'] = alternativeto_post
        return technology

    def _add_wikipedia_post(self, technology):
        uri = technology['uri']
        wikipedia_post = filters.find_object_in_list_by_uri(uri, self.wikipedias)
        wikipedia_post = wikipedia_post.get('wikipedia_post', {})
        technology['wikipedia_post'] = wikipedia_post
        return technology

    def _add_dbengine_post(self, technology):
        uri = technology['uri']
        dbengine_post = filters.find_object_in_list_by_uri(uri, self.dbengines)
        dbengine_post = dbengine_post.get('dbengine_post', {})
        technology['dbengine_post'] = dbengine_post
        return technology

    def _add_stackoverflow_tag(self, technology):
        uri = technology['uri']
        stackoverflow_tag = filters.find_object_in_list_by_uri(uri, self.stackoverflows)
        stackoverflow_tag = stackoverflow_tag.get('stackoverflow_tag', {})
        technology['stackoverflow_tag'] = stackoverflow_tag
        return technology

    def _add_wikidata_post(self, technology):
        uri = technology['uri']
        wikidata_post = filters.find_object_in_list_by_uri(uri, self.wikidatas)
        wikidata_post = wikidata_post.get('wikidata_post', {})
        technology['wikidata_post'] = wikidata_post
        return technology
