import json
import luigi
from luigi.format import UTF8
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.scrapper.alternativeto import AlternativetoScrapper
from tasks.helpers.clients.traductor.dbpedia import DBpediaTraductor

from tasks.helpers.data import filters
from tasks.helpers.files import csv
from tasks import settings


class AlternativeMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.ALTERNATIVETO_MAPPER_FILE, format=UTF8)


class FindAlternativetoPostTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                 'alternativetos': AlternativeMapperFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.ALTERNATIVETO_OUTPUT_PATH,
                                   'technologies_alternativeto_posts_mapper_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['alternativetos'].open('r') as file:
            self.alternativetos = csv.read_csv(file)

        new_techs = []
        for t in technologies:
            t = self._add_alternativeto_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_alternativeto_post(self, technology):
        uri = technology.get('uri')
        alternativeto_map = filters.find_object_in_list_by_uri(uri, self.alternativetos)
        if alternativeto_map:
            technology['alternativeto_post'] = alternativeto_map['alternativeto_post']
        return technology


class ScrapeAlternativetoTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return FindAlternativetoPostTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.ALTERNATIVETO_OUTPUT_PATH,
                                   'technologies_alternativeto_posts_scrapped_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        new_techs = []
        for t in technologies:
            t = self._add_alternativeto_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_alternativeto_post(self, technology):
        alternativeto_name = technology.get('alternativeto_post')
        post = {}
        if alternativeto_name:
            scrapper = AlternativetoScrapper(alternativeto_name)
            post = scrapper.get_alternativeto_post()
        technology['alternativeto_post'] = post
        return technology


class AlternativeDBpediaDictionaryFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.ALTERNATIVETO_DICTIONARY_PATH, format=UTF8)


class AddAlternativeToFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': ScrapeAlternativetoTask(self.date_interval),
                 'dictionary': AlternativeDBpediaDictionaryFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.ALTERNATIVETO_OUTPUT_PATH,
                                   'technologies_alternativeto_posts_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as input:
            technologies = json.load(input)

        with self.input()['dictionary'].open('r') as input:
            dictionary = csv.read_csv(input)
            self.traductor = DBpediaTraductor(dictionary)

        new_techs = []
        for t in technologies:
            t = self._add_dbpedia_uris(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbpedia_uris(self, technology):
        post = technology.get('alternativeto_post')
        if post:
            labels = post.get('labels')
            new_labels = self._translate_labels(labels)
            post['labels'] = new_labels
            technology['alternativeto_post'] = post
        return technology

    def _translate_labels(self, labels):
        clean_labels = []
        for label in labels:
            dbpedia_uri = self.traductor.translate_uri(label)
            new_label = {}
            if dbpedia_uri:
            	new_label['dbpedia_uri'] = dbpedia_uri['dbpedia_uri']
            else:
                new_label['dbpedia_uri'] = ''
            new_label['name'] = label
            clean_labels.append(new_label)
        return clean_labels
