import os.path
import subprocess
import json
import luigi

from tasks.helpers.files.json.merger import NoSQLMerger
from tasks import settings


class XpathFiles(luigi.ExternalTask):
    xpath_file = luigi.Parameter()

    def output(self):
        return luigi.LocalTarget(os.path.join(settings.ROOT_XPATH_PATH,
                                              '{}.json'.format(self.xpath_file)))


class LaunchCrawlerTask(luigi.Task):
    xpath_file = luigi.Parameter()
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return XpathFiles(self.xpath_file)

    def output(self):
        path = os.path.join(settings.CRAWLING_TASK_OUTPUT,
                            '{file}_{date}.json'.format(file=self.xpath_file,
                                                        date=self.date_interval))
        return luigi.LocalTarget(path)

    # We start the spider of Scrapy using the same commands when starting it from the console
    def run(self):
        output_path = self.output().path
        input_path = self.input().path
        subprocess.check_output(['scrapy', 'crawl', 'technologyTestSpider', '-a',
                                 'source_file={}'.format(input_path), '-o', output_path])


class CrawlTechnologyTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return [LaunchCrawlerTask(f, self.date_interval) for f in
                settings.XPATH_FILES]

    def output(self):
        output_path = os.path.join(
            settings.CRAWLING_TASK_OUTPUT,
            'crawled_technologies_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        crawled_technologies = []
        for xpath_file in self.input():
            with xpath_file.open('r') as file:
                crawled_technologies.append(json.load(file))

        # the technologies could be repeated in different websites so we merge it's attributes
        merger = NoSQLMerger(crawled_technologies)
        crawled_technologies = merger.merge_by_categories()

        with self.output().open('w') as output:
            json.dump(crawled_technologies, output)
