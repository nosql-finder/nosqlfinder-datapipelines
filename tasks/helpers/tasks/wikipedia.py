import json
import luigi
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.scrapper.wikipedia import WikipediaScrapper
from tasks import settings


class AddWikipediaFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddURITask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.WIKIPEDIA_OUTPUT_PATH,
                                   'technologies_wikipedia_post_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        self.scrapper = WikipediaScrapper()

        new_techs = []
        for t in technologies:
            t = self._add_wikipedia_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_wikipedia_post(self, technology):
        dbpedia_uri = technology.get('dbpedia_uri')
        wikipedia_post = {}
        if dbpedia_uri:
            wikipedia_post = self.scrapper.get_wikipedia_post(dbpedia_uri)
        technology['wikipedia_post'] = wikipedia_post
        return technology
