import json
import luigi
import os.path

from tasks.helpers.tasks.merger import MergeFieldsWithDBpediaURITask
from tasks.helpers.clients.sparql.cache_clients import ProgrammingLanguageClient

from tasks import settings


class LanguagesTriples(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.LANGUAGES_TRIPLES)


class AddLanguagesFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'languages_triples': LanguagesTriples(),
                 'technologies': MergeFieldsWithDBpediaURITask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.DBPEDIA_MERGED_OUTPUT_PATH,
                                   'languages_dbpedia_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['languages_triples'].open('r') as file:
            triples = json.load(file)

        self.lang_client = ProgrammingLanguageClient(triples)

        new_techs = []
        for t in technologies:
            t = self._add_language_fields(t)
            new_techs.append(t)

        with self.output().open('w') as file:
            json.dump(new_techs, file)

    def _add_language_fields(self, technology):
        technology = self._add_wikipedias_languages(technology)
        technology = self._add_dbengine_post(technology)
        technology = self._add_dbpedia_post(technology)
        technology = self._add_stackoverflow_tag(technology)
        technology = self._add_native(technology)
        technology = self._add_wikidata_post(technology)
        return technology

    def _add_wikipedias_languages(self, technology):
        wikipedia_post = technology.get('wikipedia_post', {})
        if wikipedia_post:
            dev_languages = wikipedia_post.get('developed_languages', {})
            if dev_languages:
                dev_languages = self.lang_client.get_fields(dev_languages['resources'])
                wikipedia_post['developed_languages']['resources'] = dev_languages
        technology['wikipedia_post'] = wikipedia_post
        return technology

    def _add_dbengine_post(self, technology):
        dbengine_post = technology.get('dbengine_post', {})
        if dbengine_post:
            drivers = dbengine_post.get('drivers', {})
            if drivers:
                if isinstance(drivers, dict):
                    drivers = drivers.get('resources', [])

                drivers = self.lang_client.get_fields(drivers)
                dbengine_post['drivers']['resources'] = drivers

            dev_langs = dbengine_post.get('developed_languages', {})
            if dev_langs:
                if isinstance(dev_langs, dict):
                    dev_langs = dev_langs.get('resources', [])

                dev_langs = self.lang_client.get_fields(dev_langs)
                dbengine_post['developed_languages']['resources'] = dev_langs
        technology['dbengine_post'] = dbengine_post
        return technology

    def _add_dbpedia_post(self, technology):
        dbpedia_post = technology.get('dbpedia_post', {})
        if dbpedia_post:
            dev_langs = dbpedia_post.get('developed_languages', [])
            dev_langs = self.lang_client.get_fields(dev_langs)
            dbpedia_post['developed_languages'] = dev_langs
        technology['dbpedia_post'] = dbpedia_post
        return technology

    def _add_wikidata_post(self, technology):
        wikidata_post = technology.get('wikidata_post', {})
        if wikidata_post:
            dev_langs = wikidata_post.get('developed_languages', [])
            dev_langs = self.lang_client.get_fields(dev_langs)
            wikidata_post['developed_languages'] = dev_langs
        technology['wikidata_post'] = wikidata_post
        return technology

    def _add_stackoverflow_tag(self, technology):
        stackoverflow_tag = technology.get('stackoverflow_tag', {})
        if stackoverflow_tag:
            rel_tags = stackoverflow_tag.get('related_tags', [])
            rel_tags = self.lang_client.get_fields(rel_tags)
            stackoverflow_tag['related_tags'] = rel_tags
            technology['stackoverflow_tag'] = stackoverflow_tag
        return technology

    def _add_native(self, technology):
        drivers = technology.get('drivers', {})
        if drivers:
            if isinstance(drivers, dict):
                drivers = drivers.get('resources', [])
            drivers = self.lang_client.get_fields(drivers)
            technology['drivers']['resources'] = drivers

        dev_langs = technology.get('developed_languages', {})
        if dev_langs:
            if isinstance(dev_langs, dict):
                dev_langs = dev_langs.get('resources', [])
            dev_langs = self.lang_client.get_fields(dev_langs)
            technology['developed_languages']['resources'] = dev_langs
        return technology

    def _merge_fields(self, dictionary1, dictionary2):
        result = dictionary1.copy()
        result.update(dictionary2)
        return result
