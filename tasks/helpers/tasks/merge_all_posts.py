import json
import luigi
import os.path

from tasks.helpers.tasks.stackshare import AddStackshareFieldsTask
from tasks.helpers.tasks.github import AddRepositoriesStatsTask
from tasks.helpers.tasks.github import AddGithubDriversRepositoriesTask
from tasks.helpers.clients.scrapper.github import GithubTechnologiesScrapper

from tasks.helpers.tasks.links import AddStatsToLinks
from tasks.helpers.tasks.hackernews import AddSentimentCommentsTask

from tasks.helpers.tasks.merged_license_language import MergeLicenseAndLanguageFieldsTask
from tasks.helpers.files import csv
from tasks.helpers.data import filters
from tasks import settings


class MergeAllFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'stackshares': AddStackshareFieldsTask(self.date_interval),
                 'repositories_stats': AddRepositoriesStatsTask(self.date_interval),
                 'drivers_repositories': AddGithubDriversRepositoriesTask(self.date_interval),
                 'links': AddStatsToLinks(self.date_interval),
                 'comments': AddSentimentCommentsTask(self.date_interval),
                 'merged_licenses_languages': MergeLicenseAndLanguageFieldsTask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                                   'all_technologies_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['stackshares'].open('r') as file:
            self.stackshares = json.load(file)

        with self.input()['repositories_stats'].open('r') as file:
            self.repositories_stats = json.load(file)

        with self.input()['drivers_repositories'].open('r') as file:
            self.drivers_repositories = json.load(file)

        with self.input()['links'].open('r') as file:
            self.links = json.load(file)

        with self.input()['comments'].open('r') as file:
            self.comments = json.load(file)

        with self.input()['merged_licenses_languages'].open('r') as file:
            merged_licenses_languages = json.load(file)

        new_techs = []
        for t in merged_licenses_languages:
            t = self._merge_posts(t)
            new_techs.append(t)

        with self.output().open('w') as file:
            json.dump(new_techs, file)

    def _merge_posts(self, technology):
        technology = self._add_stackshare_post(technology)
        technology = self._add_repository_stats(technology)
        technology = self._add_drivers_repositories(technology)
        technology = self._add_links_stats(technology)
        technology = self._add_comments(technology)
        return technology

    def _add_stackshare_post(self, technology):
        uri = technology['uri']
        tech_with_stackshare_post = filters.find_object_in_list_by_uri(uri, self.stackshares)
        stackshare_post = tech_with_stackshare_post.get('stackshare_post', {})
        technology['stackshare_post'] = stackshare_post
        return technology

    def _add_comments(self, technology):
        uri = technology['uri']
        tech_with_comments = filters.find_object_in_list_by_uri(uri, self.comments)
        comments = tech_with_comments.get('comments', [])
        technology['comments'] = comments
        return technology

    def _add_repository_stats(self, technology):
        uri = technology['uri']
        tech_with_repository = filters.find_object_in_list_by_uri(uri, self.repositories_stats)
        repository = tech_with_repository.get('repository', {})
        technology['repository'] = repository
        return technology

    def _add_drivers_repositories(self, technology):
        uri = technology['uri']
        tech_with_drivers = filters.find_object_in_list_by_uri(uri, self.drivers_repositories)
        drivers_repositories = tech_with_drivers.get('drivers_repositories', {})
        technology['drivers_repositories'] = drivers_repositories
        return technology

    def _add_links_stats(self, technology):
        uri = technology['uri']
        tech_with_links = filters.find_object_in_list_by_uri(uri, self.links)
        links = tech_with_links.get('links_stats', [])
        technology['links_stats'] = links
        return technology


class DropNoSQLs(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return MergeAllFieldsTask(self.date_interval)

    def output(self):
        path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                            'technologies_with_sufficient_data_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        clean_techs = []
        for t in technologies:
            if self._has_sufficient_data(t):
                clean_techs.append(t)

        with self.output().open('w') as file:
            json.dump(clean_techs, file)

    def _has_sufficient_data(self, technology):
        has_wikipedia = self._has_post(technology, 'wikidata_post')
        has_dbpedia = self._has_post(technology, 'dbpedia_post')
        return has_dbpedia and has_wikipedia

    def _has_post(self, technology, post):
        stackshare_post = technology.get(post)
        return bool(stackshare_post)


class AddGithubTechnologies(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return DropNoSQLs(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                                   'technologies_github_techs_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        clean_techs = []
        for t in technologies:
            t = self._add_github_techs(t)
            clean_techs.append(t)

        with self.output().open('w') as file:
            json.dump(clean_techs, file)

    def _add_github_techs(self, technology):
        name = technology['name']
        github_client = GithubTechnologiesScrapper(name)
        technologies_stats = github_client.get_technologies()
        technology['technologies_stats'] = technologies_stats
        return technology


class ThumbnailMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.THUMBNAIL_MAPPER_FILE)


class AddThumbnail(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {
            'technologies': AddGithubTechnologies(self.date_interval),
            'thumbnails': ThumbnailMapperFile() 
        }
        return tasks

    def output(self):
        output_path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
            'technologies_with_thumbnails_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['thumbnails'].open('r') as file:
            self.thumbnails = csv.read_csv(file)

        new_techs = []
        for t in technologies:
            t = self._add_thumbnail(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_thumbnail(self, technology):
        uri = technology.get('uri')
        thumbnail_map = filters.find_object_in_list_by_uri(uri, self.thumbnails)
        if thumbnail_map:
            technology['thumbnail'] = thumbnail_map['thumbnail']
        return technology