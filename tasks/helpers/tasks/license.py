import json
import luigi
import os.path

from tasks.helpers.tasks.merger import MergeFieldsWithDBpediaURITask
from tasks.helpers.license import MovementFinder

from tasks import settings


class LicensesTriples(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.LICENSES_TRIPLES)


class AddLicenseFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'licenses_triples': LicensesTriples(),
                 'technologies': MergeFieldsWithDBpediaURITask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.DBPEDIA_MERGED_OUTPUT_PATH,
                                   'licenses_dbpedia_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['licenses_triples'].open('r') as file:
            triples = json.load(file)

        self.license_client = MovementFinder(triples)

        new_techs = []
        for t in technologies:
            t = self._add_license_fields(t)
            new_techs.append(t)

        with self.output().open('w') as file:
            json.dump(new_techs, file)

    def _add_license_fields(self, technology):
        technology = self._add_wikipedias_licenses(technology)
        technology = self._add_dbengine_post(technology)
        technology = self._add_dbpedia_post(technology)
        technology = self._add_native_licenses(technology)
        technology = self._add_wikidata_post(technology)
        return technology

    def _add_wikipedias_licenses(self, technology):
        wikipedia_post = technology.get('wikipedia_post', {})
        if wikipedia_post:
            licenses = wikipedia_post.get('licenses', {})
            if licenses:
                new_licenses = self._get_list_licenses(licenses)
                wikipedia_post['licenses']['resources'] = new_licenses
        technology['wikipedia_post'] = wikipedia_post
        return technology

    def _add_dbengine_post(self, technology):
        dbengine_post = technology.get('dbengine_post', {})
        if dbengine_post:
            licenses = dbengine_post.get('licenses', {})
            if licenses:
                new_licenses = self._get_list_licenses(licenses)
                dbengine_post['licenses']['resources'] = new_licenses
        technology['dbengine_post'] = dbengine_post
        return technology

    def _add_dbpedia_post(self, technology):
        dbpedia_post = technology.get('dbpedia_post', {})
        if dbpedia_post:
            licenses = dbpedia_post.get('licenses')
            new_licenses = self._get_list_licenses(licenses)
            dbpedia_post['licenses'] = new_licenses
        technology['dbpedia_post'] = dbpedia_post
        return technology

    def _add_wikidata_post(self, technology):
        wikidata_post = technology.get('wikidata_post', {})
        if wikidata_post:
            licenses = wikidata_post.get('licenses')
            new_licenses = self._get_list_licenses(licenses)
            wikidata_post['licenses'] = new_licenses
        technology['wikidata_post'] = wikidata_post
        return technology

    def _add_native_licenses(self, technology):
        licenses = technology.get('licenses')
        if licenses:
            new_licenses = self._get_list_licenses(licenses)
            technology['licenses'] = new_licenses
        return technology

    def _get_list_licenses(self, licenses):
        new_licenses = []
        if isinstance(licenses, dict):
            licenses = licenses.get('resources', [])
        for lic in licenses:
            new_lic = self.license_client.get_fields(lic)
            if new_lic:
                new_licenses.append(new_lic)
        return new_licenses

    def _get_list_licenses_for_dbpedia(self, licenses):
        new_licenses = []
        for lic in licenses:
            new_license = self.license_client.get_fields(lic)
            if new_license:
                new_licenses.append(new_license)
        return new_licenses
