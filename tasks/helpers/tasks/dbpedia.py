import json
import luigi
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.sparql.dbpedia_client import DBpediaClient
from tasks.helpers.clients.sparql.cache_clients import CachedDBpediaClient
from tasks.helpers.data import text_cleaner as tc

from tasks import settings


class DBpediaTriplesFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.DBPEDIA_TRIPLES_PATH)


class AddDBpediaTriplesTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                 'triples': DBpediaTriplesFile()}
        return tasks

    def output(self):
        output_path = os.path.join(
            settings.DBPEDIA_OUTPUT_PATH,
            'technologies_dbpedia_triples_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['triples'].open('r') as file:
            self.triples = json.load(file)

        self.dbpedia_client = DBpediaClient()

        new_techs = []
        for t in technologies:
            t = self._add_dbpedia_triples(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbpedia_triples(self, technology):
        dbpedia_uri = technology.get('dbpedia_uri')
        dbpedia_post = {}
        wikidata_uri = ''
        if dbpedia_uri:
            dbpedia_post = self._get_dbpedia_post(dbpedia_uri)
            wikidata_uri = self.dbpedia_client.get_wikidata_uri(dbpedia_uri)
        technology['dbpedia_post'] = dbpedia_post
        technology['wikidata_uri'] = wikidata_uri
        return technology

    def _get_dbpedia_post(self, dbpedia_uri):
        try:
            dbpedia_post = self.dbpedia_client.get_fields(dbpedia_uri, self.triples)
            dbpedia_post = self._clean_dbpedia_fields(dbpedia_post)
        except:
            dbpedia_post = {}
        return dbpedia_post

    def _clean_dbpedia_fields(self, dbpedia_post):
        dbpedia_post = self._add_cleaned_field('developed_languages', dbpedia_post)
        dbpedia_post = self._add_cleaned_field('licenses', dbpedia_post)
        dbpedia_post = self._add_cleaned_field('operating_systems', dbpedia_post)
        dbpedia_post = self._add_cleaned_field('developers', dbpedia_post)
        return dbpedia_post

    def _add_cleaned_field(self, field_name, dbpedia_post):
        resources = dbpedia_post.get(field_name, [])
        resources = self._replace_dbpedia_uri(resources)
        dbpedia_post[field_name] = resources
        return dbpedia_post

    def _replace_dbpedia_uri(self, resources):
        return [{'dbpedia_uri': tc.remove_dbpedia_uri_prefix(r)}
                for r in resources]


class BasicTriples(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.BASIC_TRIPLES_PATH)


class AddDBpediaFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddDBpediaTriplesTask(self.date_interval),
                 'triples': BasicTriples()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.DBPEDIA_OUTPUT_PATH,
                                   'technologies_dbpedia_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['triples'].open('r') as file:
            self.triples = json.load(file)

        self.dbpedia_client = CachedDBpediaClient(self.triples)

        new_techs = []
        for t in technologies:
            t = self._add_dbpedia_fields(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbpedia_fields(self, technology):
        dbpedia_post = technology.get('dbpedia_post')
        if dbpedia_post:
            dbpedia_post = self._add_simple_fields(dbpedia_post)
            dbpedia_post = self._add_dbpedia_field('developers', dbpedia_post)
            dbpedia_post = self._add_dbpedia_field('developed_languages', dbpedia_post)
            dbpedia_post = self._add_dbpedia_field('operating_systems', dbpedia_post)
        technology['dbpedia_post'] = dbpedia_post
        return technology

    def _add_simple_fields(self, dbpedia_post):
        dbpedia_post = self._add_simple_field(dbpedia_post, 'name')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'summary')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'homepage')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'latest_release')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'status')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'thumbnail')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'frequently_updated')
        dbpedia_post = self._add_simple_field(dbpedia_post, 'latest_release_date')
        return dbpedia_post

    def _add_simple_field(self, dbpedia_post, field):
        value = dbpedia_post.get(field, [])
        value = tc.list_to_str(value)
        dbpedia_post[field] = value
        return dbpedia_post

    def _add_dbpedia_field(self, field_name, dbpedia_post):
        attrs = dbpedia_post.get(field_name, [])
        clean_attrs = []
        for attr in attrs:
            dbpedia_uri = attr.get('dbpedia_uri')
            if dbpedia_uri:
                fields = self.dbpedia_client.get_fields(dbpedia_uri)
                fields['dbpedia_uri'] = dbpedia_uri
                clean_attrs.append(fields)
        dbpedia_post[field_name] = clean_attrs
        return dbpedia_post

    def _clean_dbpedia_fields(self, fields):
        name = fields.get('name', [])
        name = tc.list_to_str(name)
        fields['name'] = name
        return fields
