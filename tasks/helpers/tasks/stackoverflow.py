import json
import luigi
from luigi.format import UTF8
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.scrapper.stackoverflow import StackoverflowScrapper
from tasks.helpers.clients.api.stackoverflow import StackoverflowRelatedTagClient
from tasks.helpers.clients.traductor.dbpedia import DBpediaTraductor

from tasks.helpers.data import filters
from tasks.helpers.files import csv
from tasks import settings


class StackoverflowMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.STACKOVERFLOW_MAPPER_FILE, format=UTF8)


class FindStackoverflowPostTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                 'stackoverflow_tags': StackoverflowMapperFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.STACKOVERFLOW_OUTPUT_PATH,
                                   'technologies_stackoverflow_posts_mapper_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as input:
            technologies = json.load(input)

        with self.input()['stackoverflow_tags'].open('r') as input:
            self.stackoverflow_tags = csv.read_csv(input)

        new_techs = []
        for t in technologies:
            t = self._add_stackoverflow_tag(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_stackoverflow_tag(self, technology):
        uri = technology.get('uri')
        stackoverflow_map = filters.find_object_in_list_by_uri(uri, self.stackoverflow_tags)
        if stackoverflow_map:
            technology['stackoverflow_tag'] = stackoverflow_map['tag']
        return technology


class ScrapeStackoverflowTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return FindStackoverflowPostTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.STACKOVERFLOW_OUTPUT_PATH,
                                   'technologies_stackoverflow_posts_scrapped_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        new_techs = []
        for t in technologies:
            t = self._add_stackoverflow_tag(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_stackoverflow_tag(self, technology):
        tag_name = technology.get('stackoverflow_tag')
        tag = {}
        if tag_name:
            scrapper = StackoverflowScrapper(tag_name)
            tag = scrapper.get_tag()
        technology['stackoverflow_tag'] = tag
        return technology


class FindStackoverflowRelatedTags(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return ScrapeStackoverflowTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.STACKOVERFLOW_OUTPUT_PATH,
                                   'technologies_stackoverflow_related_tags_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        self.stackoverflow_client = StackoverflowRelatedTagClient()

        new_techs = []
        for t in technologies:
            t = self._add_related_tags(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_related_tags(self, technology):
        tag = technology.get('stackoverflow_tag')
        if tag:
            tag = self.stackoverflow_client.add_related_tags(tag)
        else:
            tag = {}
        technology['stackoverflow_tag'] = tag
        return technology


class StackoverflowDBpediaDictionaryFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.STACKOVERFLOW_DICTIONARY_PATH, format=UTF8)


class AddStackoverflowFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': FindStackoverflowRelatedTags(self.date_interval),
                 'dictionary': StackoverflowDBpediaDictionaryFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.STACKOVERFLOW_OUTPUT_PATH,
                                   'technologies_stackoverflow_tags_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as input:
            technologies = json.load(input)

        with self.input()['dictionary'].open('r') as input:
            dictionary = csv.read_csv(input)
            self.traductor = DBpediaTraductor(dictionary)

        new_techs = []
        for t in technologies:
            t = self._add_dbpedia_uris(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbpedia_uris(self, technology):
        tag = technology.get('stackoverflow_tag')
        if tag:
            related_tags = tag.get('related_tags')
            new_related_tags = self._translate_related_tags(related_tags)
            tag['related_tags'] = new_related_tags
            technology['stackoverflow_tag'] = tag
        return technology

    def _translate_related_tags(self, tags):
        clean_related_tags = []
        for t in tags:
            uri = t['uri']
            dbpedia_uri = self.traductor.translate_uri(uri)
            if dbpedia_uri:
                t['dbpedia_uri'] = dbpedia_uri['dbpedia_uri']
            clean_related_tags.append(t)
        return clean_related_tags
