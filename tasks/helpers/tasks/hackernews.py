import json
import luigi
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.api.hackernews import HackerNewsClient
from tasks.helpers.clients.api.sentiment import SentimentClient

from tasks import settings


class AddPerformanceCommentsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddURITask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.HACKERNEWS_OUTPUT_PATH,
                                   'technologies_with_performance_comments_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        self.hacker_news_client = HackerNewsClient()

        new_techs = []
        for t in technologies:
            t = self._add_comments(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_comments(self, technology):
        name = technology.get('name')
        comments = self.hacker_news_client.get_comments([name, 'performance'])
        technology['comments'] = comments
        return technology


class AddSentimentCommentsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddPerformanceCommentsTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.HACKERNEWS_OUTPUT_PATH,
                                   'technologies_sentiment_in_comments_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        self.sentiment_client = SentimentClient()

        new_techs = []
        for t in technologies:
            t = self._add_sentiment_to_comments(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_sentiment_to_comments(self, technology):
        comments = technology.get('comments')
        comments_sentiments = self.sentiment_client.get_sentiment_data(comments)
        technology['comments'] = comments_sentiments
        return technology
