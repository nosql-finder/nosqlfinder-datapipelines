import json
import luigi
import os.path

from tasks.helpers.tasks.license import AddLicenseFieldsTask
from tasks.helpers.tasks.language import AddLanguagesFieldsTask

from tasks.helpers.data import filters
from tasks import settings


class MergeLicenseAndLanguageFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'licenses': AddLicenseFieldsTask(self.date_interval),
                 'languages': AddLanguagesFieldsTask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                                   'merged_licenses_languages_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['licenses'].open('r') as file:
            self.licenses = json.load(file)

        with self.input()['languages'].open('r') as file:
            languages = json.load(file)

        new_techs = []
        for t in languages:
            t = self._add_license_fields(t)
            new_techs.append(t)

        with self.output().open('w') as file:
            json.dump(new_techs, file)

    def _add_license_fields(self, technology):
        uri = technology['uri']
        tech_with_license = filters.find_object_in_list_by_uri(uri, self.licenses)
        technology = self._add_wikipedia_license(technology, tech_with_license)
        technology = self._add_dbengine_license(technology, tech_with_license)
        technology = self._add_dbpedia_license(technology, tech_with_license)
        technology = self._add_native_license(technology, tech_with_license)
        technology = self._add_wikidata_license(technology, tech_with_license)
        return technology

    def _add_wikipedia_license(self, technology, tech_with_license):
        wikipedia_post = tech_with_license.get('wikipedia_post', {})
        if wikipedia_post:
            licenses = wikipedia_post.get('licenses')
            technology['wikipedia_post']['licenses'] = licenses
        return technology

    def _add_dbengine_license(self, technology, tech_with_license):
        dbengine_post = tech_with_license.get('dbengine_post', {})
        if dbengine_post:
            licenses = dbengine_post.get('licenses', [])
            technology['dbengine_post']['licenses'] = licenses
        return technology

    def _add_dbpedia_license(self, technology, tech_with_license):
        dbpedia_post = tech_with_license.get('dbpedia_post', {})
        if dbpedia_post:
            licenses = dbpedia_post.get('licenses', [])
            technology['dbpedia_post']['licenses'] = licenses
        return technology

    def _add_wikidata_license(self, technology, tech_with_license):
        wikidata_post = tech_with_license.get('wikidata_post', {})
        if wikidata_post:
            licenses = wikidata_post.get('licenses', [])
            technology['wikidata_post']['licenses'] = licenses
        return technology

    def _add_native_license(self, technology, tech_with_license):
        licenses = tech_with_license.get('licenses')
        if licenses:
            technology['licenses'] = licenses
        return technology
