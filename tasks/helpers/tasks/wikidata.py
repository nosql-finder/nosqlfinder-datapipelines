import json
import luigi
import os.path
import traceback

from tasks.helpers.tasks.dbpedia import AddDBpediaFieldsTask
from tasks.helpers.clients.sparql.wikidata_client import WikidataClient
from tasks.helpers.clients.sparql.cache_clients import CachedDBpediaClient

from tasks.helpers.data import text_cleaner as tc

from tasks import settings


class WikidataTriplesFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.WIKIDATA_TRIPLES_PATH)


class AddWikidataFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddDBpediaFieldsTask(self.date_interval),
                'triples': WikidataTriplesFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.WIKIDATA_OUTPUT_PATH,
                                   'technologies_wikidata_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['triples'].open('r') as file:
            self.fields = json.load(file)

        self.wikidata_client = WikidataClient()

        new_techs = []
        for t in technologies:
            t = self._add_wikidata_fields(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_wikidata_fields(self, technology):
        wikidata_uri = technology.get('wikidata_uri')
        wikidata_post = {}
        if wikidata_uri:
            try:
                wikidata_post = self.wikidata_client.get_fields(wikidata_uri, self.fields)
                wikidata_post = self._clean_wikidata_post(wikidata_post)
            except:
                traceback.print_exc()
        technology['wikidata_post'] = wikidata_post
        return technology

    def _clean_wikidata_post(self, wikidata_post):
        wikidata_post = self._add_cleaned_field('developed_languages', wikidata_post)
        wikidata_post = self._add_cleaned_field('licenses', wikidata_post)
        wikidata_post = self._add_cleaned_field('operating_systems', wikidata_post)
        wikidata_post = self._add_cleaned_field('developers', wikidata_post)
        return wikidata_post

    def _add_cleaned_field(self, field_name, wikidata_post):
        resources = wikidata_post.get(field_name, [])
        resources = self._replace_wikidata_uri(resources)
        wikidata_post[field_name] = resources
        return wikidata_post

    def _replace_wikidata_uri(self, resources):
        return [r.replace('http://www.wikidata.org/entity/', '')
                           for r in resources]


class BasicTriplesFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.BASIC_TRIPLES_PATH)


class TranslateFieldsToDBpedia(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddWikidataFieldsTask(self.date_interval),
                 'triples': BasicTriplesFile()}
        return tasks

    def output(self):
        path = os.path.join(settings.WIKIDATA_OUTPUT_PATH,
                            'technologies_translated_dbpedia_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['triples'].open('r') as file:
            triples = json.load(file)

        self.dbpedia_client = CachedDBpediaClient(triples)
        self.wikidata_client = WikidataClient()

        clean_techs = []
        for t in technologies:
            t = self._add_dbpedia_fields(t)
            clean_techs.append(t)

        with self.output().open('w') as file:
            json.dump(clean_techs, file)

    def _add_dbpedia_fields(self, technology):
        wikidata_post = technology.get('wikidata_post', {})
        wikidata_post = self._translate_to_dbpedia('developers', wikidata_post)
        wikidata_post = self._translate_to_dbpedia('licenses', wikidata_post)
        wikidata_post = self._translate_to_dbpedia('developed_languages', wikidata_post)
        wikidata_post = self._translate_to_dbpedia('operating_systems', wikidata_post)
        technology['wikidata_post'] = wikidata_post
        return technology

    def _translate_to_dbpedia(self, field_name, wikidata_post):
        values = wikidata_post.get(field_name, [])
        clean_values = []
        for v in values:
            dbpedia_uri = self.wikidata_client.get_dbpedia_uri(v)
            if dbpedia_uri:
                fields = self.dbpedia_client.get_fields(dbpedia_uri)
                fields['dbpedia_uri'] = dbpedia_uri
                clean_values.append(fields)
        wikidata_post[field_name] = clean_values
        wikidata_post = self._clean_simple_fields(wikidata_post)
        return wikidata_post

    def _clean_simple_fields(self, wikidata_post):
        wikidata_post = self._clean_simple_field('thumbnail', wikidata_post)
        wikidata_post = self._clean_simple_field('quora_topic', wikidata_post)
        wikidata_post = self._clean_simple_field('homepage', wikidata_post)
        wikidata_post = self._clean_simple_field('latest_release', wikidata_post)
        wikidata_post = self._clean_simple_field('twitter_username', wikidata_post)
        wikidata_post = self._clean_simple_field('repository', wikidata_post)
        wikidata_post = self._clean_simple_field('stack_exchange_tag', wikidata_post)
        wikidata_post = self._clean_simple_field('facebook_username', wikidata_post)
        return wikidata_post

    def _clean_simple_field(self, field_name, wikidata_post):
        field = wikidata_post.get(field_name, [])
        if isinstance(field, list):
            if len(field) > 1:
                field = field[0]
            else:
                field = tc.list_to_str(field)
        wikidata_post[field_name] = field
        return wikidata_post
