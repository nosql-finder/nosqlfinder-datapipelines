import json
import luigi
from luigi.format import UTF8
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.clients.scrapper.dbengine import DBEngineScrapper
from tasks.helpers.clients.traductor.dbpedia import DBpediaTraductor

from tasks.helpers.data import filters
from tasks.helpers.files import csv
from tasks import settings


class DBEngineMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.DBENGINE_MAPPER_FILE, format=UTF8)


class FindDBEnginePostTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                'dbengines': DBEngineMapperFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.DBENGINE_OUTPUT_PATH,
                                   'technologies_dbengine_posts_mapper_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['dbengines'].open('r') as file:
            self.dbengines = csv.read_csv(file)

        new_techs = []
        for t in technologies:
            t = self._add_dbengine_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbengine_post(self, technology):
        uri = technology.get('uri')
        dbengine_map = filters.find_object_in_list_by_uri(uri, self.dbengines)
        if dbengine_map:
            technology['dbengine_post'] = dbengine_map['dbengine_post']
        return technology


class ScrapeDBEngineTask(luigi.ExternalTask):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return FindDBEnginePostTask(self.date_interval)

    def output(self):
        output_path = os.path.join(settings.DBENGINE_OUTPUT_PATH,
                                   'technologies_dbengine_posts_scrapped_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        new_techs = []
        for t in technologies:
            t = self._add_dbengine_post(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbengine_post(self, technology):
        dbengine_name = technology.get('dbengine_post')
        post = {}
        if dbengine_name:
            scrapper = DBEngineScrapper(dbengine_name)
            post = scrapper.get_dbengine_post()
        technology['dbengine_post'] = post
        return technology


class DBEngineDBpediaDictionaryFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.DBENGINE_DICTIONARY_PATH, format=UTF8)


class AddDBEnginePostFieldsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': ScrapeDBEngineTask(self.date_interval),
                 'dictionary': DBEngineDBpediaDictionaryFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.DBENGINE_OUTPUT_PATH,
                                   'technologies_dbengine_posts_fields_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['dictionary'].open('r') as file:
            dictionary = csv.read_csv(file)
            self.traductor = DBpediaTraductor(dictionary)

        technologies_with_dbpedia_uris = []
        for t in technologies:
            t = self._add_dbpedia_uris(t)
            technologies_with_dbpedia_uris.append(t)

        with self.output().open('w') as output:
            json.dump(technologies_with_dbpedia_uris, output)

    def _add_dbpedia_uris(self, technology):
        dbengine_post = technology.get('dbengine_post')
        if dbengine_post:
            dbengine_post = self._add_dbpedia_uri_to_drivers(dbengine_post)
            dbengine_post = self._add_dbpedia_uri_to_licenses(dbengine_post)
            dbengine_post = self._add_dbpedia_uri_to_operating_systems(dbengine_post)
            dbengine_post = self._add_dbpedia_uri_to_developed_languages(dbengine_post)
            technology['dbengine_post'] = dbengine_post
        return technology

    def _add_dbpedia_uri_to_drivers(self, post):
        drivers = post.get('drivers')
        post['drivers'] = self._translate_drivers(drivers)
        return post

    def _translate_drivers(self, drivers):
        resources = []
        text = []
        for dr in drivers:
            name = dr.get('name')
            dbpedia_uri = self.traductor.translate_uri(name)
            if dbpedia_uri:
                dr['dbpedia_uri'] = dbpedia_uri['dbpedia_uri']
                resources.append(dr)
            else:
                text.append(dr)
        clean_drivers = {'resources': resources, 'text': text}
        return clean_drivers

    def _add_dbpedia_uri_to_developed_languages(self, post):
        developed_languages = post.get('developed_languages')
        post['developed_languages'] = self._translate_developed_languages(developed_languages)
        return post

    def _translate_developed_languages(self, developed_languages):
        resources = []
        text = []
        for dl in developed_languages:
            dbpedia_uri = self.traductor.translate_uri(dl)
            if dbpedia_uri:
                name = dl
                dl = {'dbpedia_uri': dbpedia_uri['dbpedia_uri'], 'name': name}
                resources.append(dl)
            else:
                text.append(dl)
        clean_developed_languages = {'resources': resources, 'text': text}
        return clean_developed_languages

    def _add_dbpedia_uri_to_licenses(self, post):
        licenses = post.get('licenses')
        resources = []
        text = []
        for lic in licenses:
            dbpedia_uri = self.traductor.translate_uri(lic.strip())
            if dbpedia_uri:
                name = lic
                lic = {'dbpedia_uri': dbpedia_uri['dbpedia_uri'], 'name': name}
                resources.append(lic)
            else:
                text.append(lic)
        clean_licenses = {'resources': resources, 'text': text}
        post['licenses'] = clean_licenses
        return post

    def _add_dbpedia_uri_to_operating_systems(self, post):
        operating_systems = post.get('operating_systems')
        operating_systems = self.traductor.translate(operating_systems)
        post['operating_systems'] = operating_systems
        return post

    def _get_driver_status(self, drivers, driver_name):
        for dr in drivers:
            name = dr.get('name')
            if name == driver_name:
                return dr.get('status')
        return 'unknown'
