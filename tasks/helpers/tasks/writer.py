import json
import luigi
import os.path

from tasks.helpers.tasks.merge_all_posts import AddThumbnail
from tasks.helpers.clients.api.neo4j.writers.nosql import NoSQLWriter
from tasks.helpers.clients.api.trends import InterestOverTimeWriter
from tasks import settings


class WriteNeo4j(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddThumbnail(self.date_interval)

    def output(self):
        path = os.path.join(settings.ALL_TECHNOLOGIES_MERGED,
                            'writed_technologies_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(path)

    def run(self):
        with self.input().open('r') as file:
            technologies = json.load(file)

        writer = NoSQLWriter()

        for t in technologies:
            writer.write_nosql(t)

        interestWriter = InterestOverTimeWriter()
        interestWriter.write_interest_nodes()

        with self.output().open('w') as file:
            json.dump(technologies, file)


