import json
import luigi
import os.path

from tasks.helpers.tasks.uris import AddURITask
from tasks.helpers.tasks.stackshare import AddStackshareFieldsTask
from tasks.helpers.tasks.wikidata import AddWikidataFieldsTask
from tasks.helpers.tasks.wikipedia import AddWikipediaFieldsTask

from tasks.helpers.clients.api.github import GithubRepositoryClient
from tasks.helpers.clients.api.github import GihubDriversClient

from tasks.helpers.data import filters
from tasks.helpers.files import csv

from tasks import settings


class GithubMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.GITHUB_MAPPER_FILE)


class FindGithubRepositoryTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                'repositories': GithubMapperFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.GITHUB_OUTPUT_PATH,
                                   'technologies_github_repositories_mapper_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['repositories'].open('r') as file:
            self.repositories = csv.read_csv(file)

        new_techs = []
        for t in technologies:
            t = self._add_github_repository(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_github_repository(self, technology):
        uri = technology.get('uri')
        repository_map = filters.find_object_in_list_by_uri(uri, self.repositories)
        if repository_map:
            technology['repository'] = repository_map['repository']
        return technology


class AddRepositoriesStatsTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies_wikidatas': AddWikidataFieldsTask(self.date_interval),
                 'technologies_wikipedias': AddWikipediaFieldsTask(self.date_interval),
                 'technologies_stackshares': AddStackshareFieldsTask(self.date_interval),
                 'technologies': FindGithubRepositoryTask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.GITHUB_OUTPUT_PATH,
                                   'technologies_repositories_stats_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['technologies_wikidatas'].open('r') as file:
            self.technologies_with_wikidatas = json.load(file)

        with self.input()['technologies_wikipedias'].open('r') as file:
            self.technologies_with_wikipedias = json.load(file)

        with self.input()['technologies_stackshares'].open('r') as file:
            self.technologies_with_stackshares = json.load(file)

        self.github_client = GithubRepositoryClient()

        technologies_with_repos = []
        for t in technologies:
            t = self._add_repo_stats(t)
            technologies_with_repos.append(t)

        with self.output().open('w') as output:
            json.dump(technologies_with_repos, output)

    def _add_repo_stats(self, technology):
        repository = self._get_repository(technology)
        repo_data = {}
        if repository is not None:
            repo_data = self.github_client.get_repo_stats(repository)
        technology['repository'] = repo_data
        return technology

    def _get_repository(self, technology):
        repo = technology.get('repository')
        wikidata_repo = self._get_wikidata_repo(technology)
        stackshare_repo = self._get_stackshare_repo(technology)
        wikipedia_repo = self._get_wikipedia_repo(technology)
        if repo:
            return repo
        if wikipedia_repo:
            return wikipedia_repo
        if wikidata_repo:
            return wikidata_repo
        if stackshare_repo:
            return stackshare_repo
        return None

    def _get_wikidata_repo(self, technology):
        uri = technology['uri']
        tech_with_wikidata_repos = filters.find_object_in_list_by_uri(uri, self.technologies_with_wikidatas)
        wikidata_post = tech_with_wikidata_repos.get('wikidata_post')
        github_repo = ''
        if wikidata_post:
            repo = wikidata_post.get('source_code_repository', [])
            github_repo = self._get_github_repo(repo)
        return github_repo

    def _get_stackshare_repo(self, technology):
        uri = technology['uri']
        tech_with_stackshare_post = filters.find_object_in_list_by_uri(uri, self.technologies_with_stackshares)
        stackshare_post = tech_with_stackshare_post.get('stackshare_post')
        github_repo = ''
        if stackshare_post:
            repo = stackshare_post.get('repository', [])
            github_repo = self._get_github_repo(repo)
        return github_repo

    def _get_wikipedia_repo(self, technology):
        uri = technology['uri']
        tech_with_wikipedia_post = filters.find_object_in_list_by_uri(uri, self.technologies_with_wikipedias)
        wikipedia_post = tech_with_wikipedia_post.get('wikipedia_post')
        github_repo = ''
        if wikipedia_post:
            repo = wikipedia_post.get('repository', [])
            github_repo = self._get_github_repo(repo)
        return github_repo

    def _get_github_repo(self, repo):
        if 'github' in repo:
            if repo.endswith('.git'):
                repo = repo.replace('.git', '')
            return repo
        return None


class MainProgrammingLanguagesFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.MAIN_PROGRAMMING_LANGUAGES)


class AddGithubDriversRepositoriesTask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'technologies': AddURITask(self.date_interval),
                 'languages': MainProgrammingLanguagesFile()}
        return tasks

    def output(self):
        output_path = os.path.join(settings.GITHUB_OUTPUT_PATH,
                                   'technologies_github_drivers_repositories2_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as file:
            technologies = json.load(file)

        with self.input()['languages'].open('r') as file:
            self.languages = csv.read_csv(file)

        self.github_client = GihubDriversClient(self.languages)

        new_techs = []
        for t in technologies:
            t = self._add_drivers_repositories(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_drivers_repositories(self, technology):
        name = technology.get('name')
        drivers_repos = self.github_client.get_repos(name)
        technology['drivers_repositories'] = drivers_repos
        return technology
