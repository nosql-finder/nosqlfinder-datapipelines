import json
import luigi
import os.path

from tasks.helpers.tasks.dbpedia import AddDBpediaFieldsTask
from tasks.helpers.tasks.stackoverflow import AddStackoverflowFieldsTask
from tasks.helpers.clients.api.links.ranker import LinkRanker

from tasks import settings
from tasks.helpers.data import filters


class AddStatsToLinks(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'dbpedias': AddDBpediaFieldsTask(self.date_interval),
                 'stackoverflows': AddStackoverflowFieldsTask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(settings.LINKS_OUTPUT_PATH,
                                   'technologies_links_stats_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['dbpedias'].open('r') as input:
            technologies = json.load(input)

        with self.input()['stackoverflows'].open('r') as input:
            self.stackoverflows = json.load(input)

        self.link_client = LinkRanker()

        new_techs = []
        for t in technologies:
            t = self._add_stats_to_links(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_stats_to_links(self, technology):
        dbpedia_links = self._get_dbpedia_links(technology)
        
        stackoverflow_links = self._get_stackoverflow_links(technology)
        all_links = set(dbpedia_links + stackoverflow_links)
        all_links_stats = []
        for link in all_links:
            if self._is_valid_link(link):
                link_stats = self.link_client.get_rank_of_link(link)
                all_links_stats.append(link_stats)
        technology['links_stats'] = all_links_stats
        return technology

    def _is_valid_link(self, link):
        is_link = 'http' in link
        is_html = link.endswith('html')
        return is_link and not is_html

    def _get_dbpedia_links(self, technology):
        dbpedia_post = technology.get('dbpedia_post')
        links = []
        if dbpedia_post:
            links = dbpedia_post.get('external_links', [])
        return links

    def _get_stackoverflow_links(self, technology):
        technology_with_tag = filters.find_object_in_list_by_uri(technology['uri'], self.stackoverflows)
        stackoverflow_tag = technology_with_tag.get('stackoverflow_tag')
        links = []
        if stackoverflow_tag:
            links = stackoverflow_tag.get('links', [])
        return links
