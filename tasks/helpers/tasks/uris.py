import os.path
import json
import luigi

from tasks.helpers.tasks.crawling import CrawlTechnologyTask
from tasks.helpers.files import csv
from tasks.helpers.data import filters
from tasks.helpers.data.text_cleaner import URICleaner

from tasks import settings


class DBpediaMapperFile(luigi.ExternalTask):

    def output(self):
        return luigi.LocalTarget(settings.DBPEDIA_MAPPER_FILE)


class AddDBpediaURITask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        tasks = {'dbpedia_uris': DBpediaMapperFile(),
                'technologies': CrawlTechnologyTask(self.date_interval)}
        return tasks

    def output(self):
        output_path = os.path.join(
            settings.URIS_TASK_OUTPUT,
            'technologies_dbpedia_uris_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input()['technologies'].open('r') as input:
            technologies = json.load(input)

        with self.input()['dbpedia_uris'].open('r') as input:
            self.dbpedia_uris = csv.read_csv(input)

        new_techs = []
        for t in technologies:
            t = self._add_dbpedia_uri(t)
            new_techs.append(t)

        with self.output().open('w') as output:
            json.dump(new_techs, output)

    def _add_dbpedia_uri(self, technology):
        name = technology.get('name')
        dbpedia_map = filters.find_object_in_list_by_name(name, self.dbpedia_uris)
        if dbpedia_map:
            technology['dbpedia_uri'] = dbpedia_map['dbpedia_uri']
        return technology


class AddURITask(luigi.Task):
    date_interval = luigi.DateIntervalParameter()

    def requires(self):
        return AddDBpediaURITask(self.date_interval)

    def output(self):
        output_path = os.path.join(
            settings.URIS_TASK_OUTPUT,
            'technologies_uris_{}.json'.format(self.date_interval))
        return luigi.LocalTarget(output_path)

    def run(self):
        with self.input().open('r') as input:
            technologies = json.load(input)

        self.uri_cleaner = URICleaner()

        technologies_with_uri = []
        for t in technologies:
            t = self._add_uri(t)
            technologies_with_uri.append(t)

        with self.output().open('w') as output:
            json.dump(technologies_with_uri, output)

    def _add_uri(self, technology):
        uri = ''
        dbpedia_uri = technology.get('dbpedia_uri')
        name = technology.get('name')
        if dbpedia_uri:
            uri = self.uri_cleaner.clean_dbpedia_uri(dbpedia_uri)
        else:
            uri = self.uri_cleaner.clean_name(name)
        technology['uri'] = uri.lower()
        return technology
