# This are the paths for the output of every task

import os

# PRODUCTION_DATABASE = ''
# TEST_DATABASE = ''
# GOOGLE_USERNAME = ''
# GOOGLE_PASSWORD = ''



ROOT_OUTPUT_PATH = 'tasks/output'
ROOT_MAPPERS_PATH = 'tasks/helpers/mappers_files'
ROOT_TRIPLES_PATH = 'tasks/helpers/sparql_triples'
ROOT_DICTIONARIES_PATH = 'tasks/helpers/dictionaries'
ROOT_SENTINEL = 'tasks/helpers/data/analysis/performance/sentiment'
ROOT_INFERENCE_ENTITIES = 'tasks/helpers/inferences/entities'
ROOT_ENTITIES = 'tasks/helpers/entities'

# Crawling Tasks Path files in /helpers/task/crawling_tasks_output
CRAWLING_TASK_OUTPUT = os.path.join(ROOT_OUTPUT_PATH, 'crawling_tasks_output')
ROOT_XPATH_PATH = 'tasks/helpers/xpath_files'
XPATH_FILES = ['column_nosql', 'document_nosql', 'graph_nosql', 'graph_nosql2',
               'key_value_nosql', 'multimodel_nosql', 'multivalue_nosql',
               'object_nosql', 'triplestore_nosql', 'triplestore_nosql2']


# Add URI's Task Path files in /helpers/task/crawling_tasks_output
URIS_TASK_OUTPUT = os.path.join(ROOT_OUTPUT_PATH, 'uris_tasks_output')
DBPEDIA_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'DBpediaURIsMapper.csv')

# DBpedia Tasks Path files
DBPEDIA_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'dbpedia_tasks_output')
DBPEDIA_TRIPLES_PATH = os.path.join(ROOT_TRIPLES_PATH, 'DBpediaTriples.json')
BASIC_TRIPLES_PATH = os.path.join(ROOT_TRIPLES_PATH, 'BasicTriples.json')

# Wikidata Tasks Path file
WIKIDATA_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'wikidata_tasks_output')
WIKIDATA_TRIPLES_PATH = os.path.join(ROOT_TRIPLES_PATH, 'WikidataTriples.json')

# AlternativeTo Tasks Path files
ALTERNATIVETO_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'alternativeto_tasks_output')
ALTERNATIVETO_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'AlternativeToMapper.csv')
ALTERNATIVETO_DICTIONARY_PATH = os.path.join(ROOT_DICTIONARIES_PATH, 'AlternativeToDictionary.csv')

# DBengine Tasks Path files
DBENGINE_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'dbengine_tasks_output')
DBENGINE_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'DBEngineMapper.csv')
DBENGINE_DICTIONARY_PATH = os.path.join(ROOT_DICTIONARIES_PATH, 'DBEngineDictionary.csv')

# Stackshare Tasks Path files
STACKSHARE_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'stackshare_tasks_output')
STACKSHARE_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'StackshareMapper.csv')

# Stackoverflow Tasks Path files
STACKOVERFLOW_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'stackoverflow_tasks_output')
STACKOVERFLOW_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'StackoverflowMapper.csv')
STACKOVERFLOW_DICTIONARY_PATH = os.path.join(ROOT_DICTIONARIES_PATH, 'StackoverflowDictionary.csv')

# Wikipedia Tasks Path files
WIKIPEDIA_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'wikipedia_tasks_output')

# HackerNews Tasks Path files
HACKERNEWS_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'hackernews_tasks_output')
NEGATION_WORDS_PATH = os.path.join(ROOT_SENTINEL, 'negation_words.txt')
PUNCTUATION_CHARS_PATH = os.path.join(ROOT_SENTINEL, 'punctuation_characters.txt')
STRONG_INTENSIFIERS_PATH = os.path.join(ROOT_SENTINEL, 'strong_intensifiers.txt')
WEAK_INTENSIFIERS_PATH = os.path.join(ROOT_SENTINEL, 'weak_intensifiers.txt')

# DBpediaMerger Tasks Path files with all the dependency tasks that have
# a dbpedia resource
DBPEDIA_MERGED_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'dbpedia_merged_fields')
LICENSES_TRIPLES = os.path.join(ROOT_TRIPLES_PATH, 'LicenseTriples.json')
LICENSES_ENTITIES = os.path.join(ROOT_INFERENCE_ENTITIES, 'licenses.json')
LICENSE_MOVEMENTS = os.path.join(ROOT_ENTITIES, 'LicensesMovements.csv')

LANGUAGES_TRIPLES = os.path.join(ROOT_TRIPLES_PATH, 'LanguagesTriples.json')

# Github Tasks Path files
GITHUB_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'github_tasks_output')
GITHUB_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'GithubMapper.csv')
MAIN_PROGRAMMING_LANGUAGES = os.path.join(ROOT_ENTITIES, 'MainProgrammingLanguages.csv')

# Links Task Path files
LINKS_OUTPUT_PATH = os.path.join(ROOT_OUTPUT_PATH, 'links_tasks_output')

GITHUB_ACCESS_TOKEN = '27dbd82595f8243d1dc8ac13d9c8b777c88bcdca'

# Merged Licenses and Languages
ALL_TECHNOLOGIES_MERGED = os.path.join(ROOT_OUTPUT_PATH, 'merged_tasks_output')

# Thumbnails
THUMBNAIL_MAPPER_FILE = os.path.join(ROOT_MAPPERS_PATH, 'ThumbnailMapper.csv')

# Nodes Labels
PROGRAMMING_LANGUAGE_LABEL = 'ProgrammingLanguage'
PROGRAMMING_PARADIGM_LABEL = 'ProgrammingParadigm'

LICENSE_LABEL = 'License'
MOVEMENT_LABEL = 'Movement'

DEVELOPER_LABEL = 'Developer'
OPERATING_SYSTEM_LABEL = 'OperatingSystem'

WIKIPEDIA_LABEL = 'WikipediaPost'

DBENGINE_LABEL = 'DBEnginePost'
USE_CASE_LABEL = 'Use'
SCHEMA_LABEL = 'Schema'
ACCESS_METHOD_LABEL = 'AccessMethod'
TRANSACTION_METHOD_LABEL = 'TransactionMethod'
CONSISTENCY_METHOD_LABEL = 'ConsistencyMethod'
REPLICATION_METHOD_LABEL = 'ReplicationMethod'
PARTITIONING_METHOD_LABEL = 'PartitioningMethod'
DATATYPE_LABEL = 'DataType'

STACKOVERFLOW_LABEL = 'StackoverflowTag'
DBPEDIA_LABEL = 'DBpediaPost'
STACKSHARE_LABEL = 'StacksharePost'
ALTERNATIVETO_LABEL = 'AlternativetoPost'

COMMENT_LABEL = 'Comment'
CLIENT_LABEL = 'Client'
TOOL_LABEL = 'Tool'

REPOSITORY_LABEL = 'Repository'
DRIVER_REPOS_CLUSTER_LABEL = 'DriverReposCluster'

WIKIDATA_LABEL = 'WikidataPost'
NOSQL_LABEL = 'NoSQL'

DATAMODEL_LABEL = 'DataModel'
KEYWORD_LABEL = 'Keyword'
API_LABEL = 'Api'
LINK_LABEL = 'Link'
REVIEW_LABEL = 'Review'
CLUSTER_TECHNOLOGIES_LABEL = 'ClusterTechsRepositories'
LABEL_FROM_ALTERNATIVETO = 'Label'
